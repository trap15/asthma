/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011        Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef PROC_HPP_
#define PROC_HPP_

#include <vector>

typedef uintmax_t ea_t;

//------------------------------------------------------------------------------
#define MAX_SYMNAME_LEN (128)
struct symbol_t
{
  symbol_t() : global(false), ext(false), valid(false), val_valid(false), val(0)
  {
    name[0] = 0;
  }
  symbol_t(char *nm) : global(false), ext(false), valid(true), val_valid(false), val(0)
  {
    strncpy(name, nm, MAX_SYMNAME_LEN); name[MAX_SYMNAME_LEN-1] = 0;
  }
  symbol_t(char *nm, uint32_t v) : global(false), ext(false), valid(true), val_valid(true), val(v)
  {
    strncpy(name, nm, MAX_SYMNAME_LEN); name[MAX_SYMNAME_LEN-1] = 0;
  }
  void set_val(intmax_t v)
  {
    valid = true;
    val_valid = true;
    val = v;
  }
  void make_global()
  {
    global = true;
    ext = false;
  }
  void make_extern()
  {
    global = false;
    ext = true;
  }
  bool global, ext;
  bool valid;
  char name[MAX_SYMNAME_LEN];
  bool val_valid;
  intmax_t val;
};

//------------------------------------------------------------------------------
enum op_type_t
{
  OPT_NONE = 0,
  OPT_UNK,
  OPT_REG, // Rx
  OPT_DISP, // 5(Rx)
  OPT_PHRASE, // 5(Rx,Ry)
  OPT_PREDEC, // -(Rx)
  OPT_POSTINC, // (Rx)+
  OPT_PREINC, // +(Rx)
  OPT_POSTDEC, // (Rx)-
  OPT_NUM, // #5
  OPT_ADDR, // (5)
  OPT_STR, // "blah"
};

#define MAX_ARG_LEN     (128)
#define MAX_STRING_LEN  (MAX_ARG_LEN-2)
struct op_t
{
  op_t() : type(OPT_NONE), reg(0), disp(0), value(0), sym(NULL)
  {
    str[0] = 0;
    argstr[0] = 0;
  }
  uint_fast8_t  type;
  uint_fast16_t reg;
  uint_fast16_t reg2;
  int_fast32_t  disp;
  int_fast32_t  value;
  char          str[MAX_STRING_LEN];
  char          argstr[MAX_ARG_LEN];
  symbol_t     *sym;
  bool is_writable() const
  {
    switch ( type )
    {
      case OPT_REG:
      case OPT_DISP:
      case OPT_PHRASE:
      case OPT_PREDEC:
      case OPT_POSTINC:
      case OPT_PREINC:
      case OPT_POSTDEC:
        return true;
      case OPT_NONE:
      case OPT_UNK:
      case OPT_NUM:
      case OPT_ADDR:
      case OPT_STR:
      default:
        return false;
    }
  }
};

#define MAX_MNEM_LEN    (32)
struct insn_t
{
  insn_t()
  {
    ea = 0;
    len = 0;
    code.resize(32); // Back-compatibility/simplicity/speed
    op.resize(8); // Back-compatibility/simplicity/speed
  }
  ea_t ea;
  std::vector<uint8_t> code;
  uint_fast8_t len;
  char mnem[MAX_MNEM_LEN];
  std::vector<op_t> op;
  uint_fast8_t opcnt;
};

#define MAX_REGNAME_LEN (32)
struct reg_t
{
  reg_t(char *nm, uint_fast16_t reg) : regno(reg)
  {
    strncpy(name, nm, MAX_REGNAME_LEN-1); name[MAX_REGNAME_LEN-1] = 0;
  }
  char name[MAX_REGNAME_LEN];
  uint_fast16_t regno;
};

//------------------------------------------------------------------------------
enum deref_styles
{
  PROC_DEREF_NUM_PARENS_REG = 0,
  PROC_DEREF_NUM_BRACKS_REG,
  PROC_DEREF_BRACK_NUM_COM_REG_BRACK,
  PROC_DEREF_BRACK_REG_COM_NUM_BRACK,
};

enum addr_styles
{
  PROC_ADDR_NONE = 0,
  PROC_ADDR_PARENS,
};

enum err_code
{
  PROC_ASM_ERR_UNK = -1,
  PROC_ASM_ERR_OK = 0,
  PROC_ASM_ERR_MISC,
  PROC_ASM_ERR_UNIMP,
  PROC_ASM_ERR_FILE_NOT_FOUND,
  PROC_ASM_ERR_BAD_MNEM,
  PROC_ASM_ERR_TOO_FEW_OPS,
  PROC_ASM_ERR_TOO_MANY_OPS,
  PROC_ASM_ERR_OUT_OF_RANGE,
  PROC_ASM_ERR_EA_INVALID,
  PROC_ASM_ERR_BAD_OP_TYPE,
  PROC_ASM_ERR_BAD_OP0_TYPE,
  PROC_ASM_ERR_BAD_OP1_TYPE,
  PROC_ASM_ERR_BAD_OP2_TYPE,
  PROC_ASM_ERR_BAD_OP3_TYPE,
  PROC_ASM_ERR_BAD_OP4_TYPE,
  PROC_ASM_ERR_BAD_OP5_TYPE,
  PROC_ASM_ERR_BAD_OP6_TYPE,
  PROC_ASM_ERR_BAD_OP7_TYPE,
  PROC_ASM_ERR_BAD_OP8_TYPE,
  PROC_ASM_ERR_BAD_OP9_TYPE,
  PROC_ASM_ERR_BAD_OP10_TYPE,
  PROC_ASM_ERR_BAD_OP11_TYPE,
  PROC_ASM_ERR_BAD_OP12_TYPE,
  PROC_ASM_ERR_BAD_OP13_TYPE,
  PROC_ASM_ERR_BAD_OP14_TYPE,
  PROC_ASM_ERR_BAD_OP15_TYPE,
  PROC_ASM_ERR_BAD_OP_VAL,
  PROC_ASM_ERR_BAD_OP0_VAL,
  PROC_ASM_ERR_BAD_OP1_VAL,
  PROC_ASM_ERR_BAD_OP2_VAL,
  PROC_ASM_ERR_BAD_OP3_VAL,
  PROC_ASM_ERR_BAD_OP4_VAL,
  PROC_ASM_ERR_BAD_OP5_VAL,
  PROC_ASM_ERR_BAD_OP6_VAL,
  PROC_ASM_ERR_BAD_OP7_VAL,
  PROC_ASM_ERR_BAD_OP8_VAL,
  PROC_ASM_ERR_BAD_OP9_VAL,
  PROC_ASM_ERR_BAD_OP10_VAL,
  PROC_ASM_ERR_BAD_OP11_VAL,
  PROC_ASM_ERR_BAD_OP12_VAL,
  PROC_ASM_ERR_BAD_OP13_VAL,
  PROC_ASM_ERR_BAD_OP14_VAL,
  PROC_ASM_ERR_BAD_OP15_VAL,
};

enum proc_endian
{
  PROC_ENDIAN_BE = 0,
  PROC_ENDIAN_LE,
  PROC_ENDIAN_PDP,
};

#define MAX_PROCNAME_LEN  (32)
#define MAX_COMMENT_LEN   (32)
struct proc_t
{
  proc_t();
  virtual ~proc_t();
//--- Variables
  int word_size; // in bytes
  int endian;
  uint8_t allowed_endians; // bitmask, (1 << proc_endian)
  uintmax_t bot_addr;
  uintmax_t top_addr;
  bool case_sense;
  char name[MAX_PROCNAME_LEN];
  char cmts[MAX_COMMENT_LEN];
  std::vector<reg_t> regs;
  int deref_style;
  int addr_style;
  char num_pfx; // prefix for integer values
//--- Functions
  virtual bool assume_reg(char *reg, uintmax_t value);
  virtual int process_insn(insn_t &cmd);
  uint_fast16_t get_reg(const char *reg);
};

#define proc_init_func(proc) proc_gen_##proc
#define proc_init(proc) proc_init_func(proc)()
#define proc_init_def(proc) proc_t *proc_init(proc)

#define mnem_check(cmd, mnm, type) do { \
  if ( strncmp(cmd.mnem, mnm, MAX_MNEM_LEN) == 0 ) \
    itype = type; \
} while ( 0 )

#endif
