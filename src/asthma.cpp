/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "proc.hpp"
#include "parser.hpp"
#include "warn_err.hpp"
#include "binary.hpp"
#include "proclist.hpp"
#include "fmtlist.hpp"
#include "getopt.hpp"

static char *app;

void print_options();

void usage(char *message)
{
  fprintf(stderr,
          "%sUsage:\n"
          "\t%s [options] in.s\n",
          message, app);
  fprintf(stderr,
          "\nOptions:\n");
  print_options();
}

namespace config
{
  const char *infile = NULL;
  char *outfile = NULL;
  const char *fmt = "binary";
  const char *cpu = "proc";
  bool set_format(const char *_fmt)
  {
    config::fmt = _fmt;
    delete asthma::outmeth;
    fmt_def_list_entry *ptr;
    for ( ptr=fmt_list; ptr->fmtinit != NULL; ptr++ )
    {
      if ( strcmp(ptr->name, config::fmt) == 0 )
      {
        asthma::outmeth = ptr->fmtinit();
        break;
      }
    }
    if ( ptr->fmtinit == NULL )
    {
      usage("Invalid output format. ");
      return false;
    }
    return true;
  }
  bool set_cpu(const char *_cpu)
  {
    config::cpu = _cpu;
    delete asthma::proc;
    proc_def_list_entry *ptr;
    for ( ptr=proc_list; ptr->procinit != NULL; ptr++ )
    {
      if ( strncmp(ptr->name, config::cpu, MAX_PROCNAME_LEN) == 0 )
      {
        asthma::proc = ptr->procinit();
        break;
      }
    }
    if ( ptr->procinit == NULL )
    {
      usage("Invalid CPU. ");
      return false;
    }
    return true;
  }
};

void print_options()
{
  printopt("--format=fmt",  "-f fmt",  "Set the output file format. Valid types are:");
  printopt_extra(                          "binary srec aout");
  printopt("--output=file", "-o file", "Set the output file name. Defaults to infile.bin");
  printopt("--cpu=cpu",     "-c cpu",  "Set the initial CPU type to assemble for.");
  asthma::outmeth->print_options();
}

bool opt_handler(const char *opt, bool small, const char *param, bool &care_param)
{
  care_param = true;
  if ( opt == NULL )
  {
    if ( config::infile != NULL )
      return false;
    config::infile = param;
    return true;
  }
  if ( (strcmp(opt, "h") == 0 && small) || (strcmp(opt, "help") == 0 && !small) )
  {
    care_param = false;
    usage("");
    exit(EXIT_SUCCESS);
  }
  else if ( (strcmp(opt, "f") == 0 && small) || (strcmp(opt, "format") == 0 && !small) )
  {
    return config::set_format(param);
  }
  else if ( (strcmp(opt, "o") == 0 && small) || (strcmp(opt, "output") == 0 && !small) )
  {
    config::outfile = (char*)param;
  }
  else if ( (strcmp(opt, "c") == 0 && small) || (strcmp(opt, "cpu") == 0 && !small) )
  {
    return config::set_cpu(param);
  }
  else
  {
    if ( !asthma::outmeth->set_option(opt, small, param, care_param) )
    {
      return false;
    }
  }
  return true;
}

int main(int argc, char *argv[])
{
  int ret = EXIT_SUCCESS;
  app = argv[0];
  printf("Asthma (c)2011~2012 trap15\n");

  asthma::syms.clear();

  asthma::outmeth = fmt_init(binary);
  asthma::proc = new proc_t(); // basic "processor" that kickstarts everything

  if ( !agetopt(argc, argv, opt_handler) )
  {
    usage("Invalid arguments. ");
    return EXIT_FAILURE;
  }

  if ( config::infile == NULL )
  {
    usage("No input file. ");
    return EXIT_FAILURE;
  }

  bool outfree = false;
  if ( config::outfile == NULL )
  {
    outfree = true;
    config::outfile = (char*)malloc(strlen(config::infile)+5);
    sprintf(config::outfile, "%s.bin", (char*)config::infile);
  }

  printf("File          : %s\n", config::infile);
  printf("Output File   : %s\n", config::outfile);
  printf("Output Format : %s\n", config::fmt);

  std::map<ea_t,uint8_t> outbin;

  if ( asthma::parse((char*)config::infile, outbin, ea_t(0)) )
  {
    asthma::error((char*)config::infile, -1, NULL, "Assembling failed.");
    ret = EXIT_FAILURE;
  }
  printf("writing file...\n");
  fflush(stdout);
  std::vector<uint8_t> binary;
  asthma::outmeth->generate_output(outbin, asthma::syms, binary);
  FILE *ofp = fopen(config::outfile, "wb");
  for ( std::vector<uint8_t>::iterator p=binary.begin(); p != binary.end(); p++ )
  {
    uint8_t tmp = *p;
    fwrite(&tmp, 1, 1, ofp);
  }
  fclose(ofp);

  if ( outfree )
    free(config::outfile);

  for ( std::vector<symbol_t>::iterator p=asthma::syms.begin(); p != asthma::syms.end(); p++ )
  {
    bool dispinfo = false;
    if ( !(*p).valid || (*p).ext )
      dispinfo = true;
    if ( dispinfo )
    {
      printf("Invalid/external symbol: %s\n"
             "  Value  : %08llX\n"
             "  Global : %s\n"
             "  Extern : %s\n\n",
             (*p).name, (long long unsigned)(*p).val,
             (*p).global ? "YES" : "NO",
             (*p).ext ? "YES" : "NO");
    }
  }

  delete asthma::outmeth;
  delete asthma::proc;

  return ret;
}
