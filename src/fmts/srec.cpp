/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "proc.hpp"
#include "parser.hpp"
#include "binary.hpp"
#include "warn_err.hpp"
#include "getopt.hpp"

struct srec_bin_t : binary_t
{
  srec_bin_t();
  virtual ~srec_bin_t();
//--- Variables
  char s0_comment[0x80];
  ea_t entrypoint;
//--- Functions
  virtual void init();
  virtual void generate_output(std::map<ea_t,uint8_t> &inbin,
                               std::vector<symbol_t> &insyms,
                               std::vector<uint8_t> &outbin);
  virtual void print_options();
  virtual bool set_option(const char *opt, bool small, const char *val, bool &care_param);
  virtual int process_pseudo_op(insn_t &cmd);
};

srec_bin_t::srec_bin_t()
{
  s0_comment[0] = '\0';
  entrypoint = 0;
}

srec_bin_t::~srec_bin_t()
{
}

void srec_bin_t::init()
{
}

static void output_hex_digits(std::vector<uint8_t> &outbin, uint8_t hex)
{
  if ( ((hex >> 4) & 0xF) < 10 )
    outbin.push_back(((hex >> 4) & 0xF) + '0');
  else
    outbin.push_back(((hex >> 4) & 0xF) - 10 + 'A');
  if ( ((hex >> 0) & 0xF) < 10 )
    outbin.push_back(((hex >> 0) & 0xF) + '0');
  else
    outbin.push_back(((hex >> 0) & 0xF) - 10 + 'A');
}

void srec_bin_t::generate_output(std::map<ea_t,uint8_t> &inbin,
                                 std::vector<symbol_t> &insyms,
                                 std::vector<uint8_t> &outbin)
{
  ea_t low_addr=ea_t(-1), high_addr=0;
  for ( std::map<ea_t,uint8_t>::iterator p=inbin.begin(); p != inbin.end(); p++ )
  {
    if ( (*p).first < low_addr )
      low_addr = (*p).first;
    if ( (*p).first > high_addr )
      high_addr = (*p).first;
  }
  uint8_t xsum = 0;
  outbin.push_back('S'); // Start code
  outbin.push_back('0'); // Vendor data
  int cmtlen = strlen(s0_comment);
  if ( cmtlen > 0x80 )
    cmtlen = 0x80;
  output_hex_digits(outbin, cmtlen+3); xsum += cmtlen+3; // print length
  output_hex_digits(outbin, 0);
  output_hex_digits(outbin, 0);
  for ( int i=0; i < cmtlen; i++ )
  {
    output_hex_digits(outbin, s0_comment[i]); xsum += s0_comment[i];
  }
  output_hex_digits(outbin, ~xsum);
  outbin.push_back('\n');

  ea_t ea;
  uint32_t lines = 0;
  for ( ea = low_addr; ea <= high_addr; )
  {
    lines++;
    ea_t nextea = ea + 0x20;
    int run_len = 0;
    ea_t tmpea;
    for ( tmpea=ea; tmpea < nextea; tmpea++ )
    {
      if ( inbin.find(tmpea) == inbin.end() )
        break;
      run_len++;
    }
    if ( tmpea != nextea )
    {
      while ( inbin.find(++tmpea) == inbin.end() && tmpea <= high_addr );
      if ( tmpea > high_addr )
        nextea = high_addr+1;
      else
        nextea = tmpea;
    }
    xsum = 0;
    outbin.push_back('S'); // Start code
    outbin.push_back('3'); // Always S3 type
    output_hex_digits(outbin, run_len+5); xsum += run_len+5; // print length
    // print address
    output_hex_digits(outbin, ea >> 24); xsum += (ea>>24);
    output_hex_digits(outbin, ea >> 16); xsum += (ea>>16);
    output_hex_digits(outbin, ea >>  8); xsum += (ea>> 8);
    output_hex_digits(outbin, ea >>  0); xsum += (ea>> 0);
    for ( int i=0; i < run_len; i++ )
    {
      output_hex_digits(outbin, inbin[ea]); xsum += inbin[ea];
      ea++;
    }
    output_hex_digits(outbin, ~xsum);
    outbin.push_back('\n');
    ea = nextea;
  }
  xsum = 0;
  outbin.push_back('S'); // Start code
  outbin.push_back('5'); // Record count
  output_hex_digits(outbin, 3); xsum += 3; // print length
  output_hex_digits(outbin, lines >> 8); xsum += (lines>>8);
  output_hex_digits(outbin, lines >> 0); xsum += (lines>>0);
  output_hex_digits(outbin, ~xsum);
  outbin.push_back('\n');

  xsum = 0;
  uint32_t start_addr = entrypoint;
  outbin.push_back('S'); // Start code
  outbin.push_back('7'); // Start-address
  output_hex_digits(outbin, 5); xsum += 5; // print length
  output_hex_digits(outbin, start_addr >> 24); xsum += (uint8_t)(start_addr>>24);
  output_hex_digits(outbin, start_addr >> 16); xsum += (uint8_t)(start_addr>>16);
  output_hex_digits(outbin, start_addr >>  8); xsum += (uint8_t)(start_addr>> 8);
  output_hex_digits(outbin, start_addr >>  0); xsum += (uint8_t)(start_addr>> 0);
  output_hex_digits(outbin, ~xsum);
  outbin.push_back('\n');
}

void srec_bin_t::print_options()
{
  printopt("--comment=cmt", NULL, "Set a comment for the S0 record.");
  printopt("--entrypoint=entry", NULL, "Set the entrypoint for the S7 record.");
}

bool srec_bin_t::set_option(const char *opt, bool small, const char *val, bool &care_param)
{
  if ( (strcmp(opt, "comment") == 0 && !small) )
  {
    strncpy(s0_comment, val, 0x80);
    s0_comment[0x7F] = '\0';
  }
  else if ( (strcmp(opt, "entrypoint") == 0 && !small) )
  {
    op_t sink;
    if ( !asthma::make_num((char*)val, sink) )
    {
      asthma::error("options", 0, (char*)val, "Invalid number passed to --entrypoint");
      return false;
    }
    else
      entrypoint = sink.value;
  }
  else
    return false;
  return true;
}

int srec_bin_t::process_pseudo_op(insn_t &cmd)
{
  return PROC_ASM_ERR_BAD_MNEM;
}

fmt_init_def(srec)
{
  return new srec_bin_t();
}
