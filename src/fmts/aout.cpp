/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "proc.hpp"
#include "parser.hpp"
#include "binary.hpp"
#include "warn_err.hpp"
#include "getopt.hpp"

struct aout_bin_t : binary_t
{
  aout_bin_t();
  virtual ~aout_bin_t();
//--- Variables
  ea_t textstart;
  ea_t textsz;
  ea_t datastart;
  ea_t datasz;
  ea_t bssstart;
  ea_t bsssz;
  int current_sec;
  ea_t entrypoint;
//--- Functions
  virtual void init();
  virtual void generate_output(std::map<ea_t,uint8_t> &inbin,
                               std::vector<symbol_t> &insyms,
                               std::vector<uint8_t> &outbin);
  virtual void print_options();
  virtual bool set_option(const char *opt, bool small, const char *val, bool &care_param);
  virtual int process_pseudo_op(insn_t &cmd);
};

aout_bin_t::aout_bin_t()
{
  entrypoint = 0;
}

aout_bin_t::~aout_bin_t()
{
}

void aout_bin_t::init()
{
  textstart = 0;
  datastart = 0;
  bssstart = 0;
  textsz = 0;
  datasz = 0;
  bsssz = 0;
  current_sec = 0;
}

static void _output_int8(std::vector<uint8_t> &bin, uint8_t val)
{
  bin.push_back(val);
}

/*
static void _output_int16(std::vector<uint8_t> &bin, uint16_t val)
{
  if(asthma::proc->endian == PROC_ENDIAN_LE) {
    bin.push_back((val >> 0) & 0xFF);
    bin.push_back((val >> 8) & 0xFF);
  }else if(asthma::proc->endian == PROC_ENDIAN_BE) {
    bin.push_back((val >> 8) & 0xFF);
    bin.push_back((val >> 0) & 0xFF);
  }else if(asthma::proc->endian == PROC_ENDIAN_PDP) {
    bin.push_back((val >> 0) & 0xFF);
    bin.push_back((val >> 8) & 0xFF);
  }
}
*/

static void _output_int32(std::vector<uint8_t> &bin, uint32_t val)
{
  if(asthma::proc->endian == PROC_ENDIAN_LE) {
    bin.push_back((val >> 0) & 0xFF);
    bin.push_back((val >> 8) & 0xFF);
    bin.push_back((val >> 16) & 0xFF);
    bin.push_back((val >> 24) & 0xFF);
  }else if(asthma::proc->endian == PROC_ENDIAN_BE) {
    bin.push_back((val >> 24) & 0xFF);
    bin.push_back((val >> 16) & 0xFF);
    bin.push_back((val >> 8) & 0xFF);
    bin.push_back((val >> 0) & 0xFF);
  }else if(asthma::proc->endian == PROC_ENDIAN_PDP) {
    bin.push_back((val >> 16) & 0xFF);
    bin.push_back((val >> 24) & 0xFF);
    bin.push_back((val >> 0) & 0xFF);
    bin.push_back((val >> 8) & 0xFF);
  }
}

static int _output_str(std::vector<uint8_t> &bin, char *str, int len)
{
  int ret = bin.size();
  for ( int i=0; i < len; i++ )
  {
    bin.push_back(str[i]);
  }
  return ret;
}

void aout_bin_t::generate_output(std::map<ea_t,uint8_t> &inbin,
                                 std::vector<symbol_t> &insyms,
                                 std::vector<uint8_t> &outbin)
{
  std::vector<uint8_t> strtab;
  std::vector<uint8_t> symtab;

  switch ( current_sec )
  {
    case 0:
      textsz = asthma::ea - textstart;
      break;
    case 1:
      datasz = asthma::ea - datastart;
      break;
    case 2:
      bsssz = asthma::ea - bssstart;
      break;
  }

  // symtab
  // only includes extern and global syms
  for ( uint32_t i=0; i < insyms.size(); i++ )
  {
    int sec = 0;
    if ( (!insyms[i].global && !insyms[i].ext) || !insyms[i].valid )
      continue;
  //   $00 - strtab offset (from start of strtab) (4bytes)
    _output_int32(symtab, _output_str(strtab, insyms[i].name, strlen(insyms[i].name)+1) + 4);
  //   $04 - section (4bytes)
  //     $01 - extern
  //     $04 - .text
  //     $05 - global .text
  //     $06 - .data
  //     $07 - global .data
  //     $08 - .bss
  //     $09 - global .bss
    if ( (uint32_t)insyms[i].val >= textstart && (uint32_t)insyms[i].val < textstart+textsz )
      sec = 0x04;
    else if ( (uint32_t)insyms[i].val >= datastart && (uint32_t)insyms[i].val < datastart+datasz )
      sec = 0x06;
    else if ( (uint32_t)insyms[i].val >= bssstart && (uint32_t)insyms[i].val < bssstart+bsssz )
      sec = 0x08;
    if ( insyms[i].ext )
      _output_int32(symtab, 0x00000001);
    else if ( insyms[i].global )
      _output_int32(symtab, sec | 1);
    else
      _output_int32(symtab, sec);
  //   $08 - address
    if ( insyms[i].val_valid )
      _output_int32(symtab, insyms[i].val);
    else
      _output_int32(symtab, 0);
  }

  // reloctab TODO!!!
  //   $00 - address (4bytes)
  //   $04 - sym number (3bytes)
  //   $07 - flags
  //     $01 - PC relative
  //     $06 - Length
  //       $00 - 1-byte displacement
  //       $02 - 2-byte " "
  //       $04 - 4-byte " "
  //     $08 - Extern
  //     $40 - Relative

  // 0407 - OMAGIC
  // 0410 - NMAGIC
  // 0413 - ZMAGIC
  // 0314 - QMAGIC
  // 0421 - CMAGIC
  uint32_t magic = 0407; // OMAGIC;
  magic |= 0 /*machine*/ << 16;
  magic |= 0 /*flags*/ << 24;
  // magic (4bytes)
  _output_int32(outbin, magic);
  // text segment size (4bytes)
  _output_int32(outbin, textsz);
  // data segment size (4bytes)
  _output_int32(outbin, datasz);
  // bss segment size (4bytes)
  _output_int32(outbin, bsssz);
  // symtab size (4bytes)
  _output_int32(outbin, symtab.size());
  // entrypoint (4bytes) (set to 0 for objects?)
  _output_int32(outbin, 0x00000000);
  // text reloctab size (4bytes) TODO!!!
  _output_int32(outbin, 0x00000000);
  // data reloctab size (4bytes) TODO!!!
  _output_int32(outbin, 0x00000000);

  // reloc tab
  // TODO!!!

  // text section
  for ( ea_t i=textstart; i < textstart+textsz; i++ )
  {
    if ( inbin.find(i) == inbin.end() )
      _output_int8(outbin, 0x00);
    else
      _output_int8(outbin, inbin[i]);
  }

  // data section
  for ( ea_t i=datastart; i < datastart+datasz; i++ )
  {
    if ( inbin.find(i) == inbin.end() )
      _output_int8(outbin, 0x00);
    else
      _output_int8(outbin, inbin[i]);
  }

  // symtab
  outbin.insert(outbin.end(), symtab.begin(), symtab.end());

  // strtab
  // length of strtab in bytes (including this) (4bytes)
  _output_int32(outbin, strtab.size() + 4);
  // strings (\0 terminated)
  outbin.insert(outbin.end(), strtab.begin(), strtab.end());
}

void aout_bin_t::print_options()
{
}

bool aout_bin_t::set_option(const char *opt, bool small, const char *val, bool &care_param)
{
  return false;
}

enum aout_itypes
{
  OP_null = 0,

  OP_section,
  OP_text,
  OP_data,
  OP_bss,

  OP_last,
};

int aout_bin_t::process_pseudo_op(insn_t &cmd)
{
  int itype = OP_null;

  cmd.len = 0;

  if ( !asthma::proc->case_sense )
  {
    unsigned int i;
    for ( i=0; i < strlen(cmd.mnem) && i < MAX_MNEM_LEN-1; i++ )
      cmd.mnem[i] = tolower(cmd.mnem[i]);
    for ( ; i < MAX_MNEM_LEN-1; i++ )
      cmd.mnem[i] = 0;
  }
  cmd.mnem[MAX_MNEM_LEN-1] = 0;

  mnem_check(cmd, ".section", OP_section);
  if ( itype == OP_section )
  {
    if ( strcmp(cmd.op[0].argstr, ".text") == 0 )
      itype = OP_text;
    else if ( strcmp(cmd.op[0].argstr, ".data") == 0 )
      itype = OP_data;
    else if ( strcmp(cmd.op[0].argstr, ".bss") == 0 )
      itype = OP_bss;
  }
  else
  {
    mnem_check(cmd, ".text", OP_text);
    mnem_check(cmd, ".data", OP_data);
    mnem_check(cmd, ".bss", OP_bss);
  }

  switch ( itype )
  {
    default:
    case OP_null:
      return PROC_ASM_ERR_BAD_MNEM;

    case OP_section:
      asthma::error(asthma::file, asthma::line, asthma::line_orig, "Bad section name for a.out. Allowed section names are '.text', '.data', and '.bss'");
      return PROC_ASM_ERR_MISC;

    case OP_text:
      if ( current_sec == 0 )
        break;
      if ( textsz != 0 )
      {
        asthma::error(asthma::file, asthma::line, asthma::line_orig, "a.out cannot switch back to an already written section!");
        return PROC_ASM_ERR_MISC;
      }
      else
      {
        switch ( current_sec )
        {
          case 0:
            textsz = asthma::ea - textstart;
            break;
          case 1:
            datasz = asthma::ea - datastart;
            break;
          case 2:
            bsssz = asthma::ea - bssstart;
            break;
	}
        textstart = asthma::ea;
        current_sec = 0;
      }
      break;

    case OP_data:
      if ( current_sec == 1 )
        break;
      if ( datasz != 0 )
      {
        asthma::error(asthma::file, asthma::line, asthma::line_orig, "a.out cannot switch back to an already written section!");
        return PROC_ASM_ERR_MISC;
      }
      else
      {
        switch ( current_sec )
        {
          case 0:
            textsz = asthma::ea - textstart;
            break;
          case 1:
            datasz = asthma::ea - datastart;
            break;
          case 2:
            bsssz = asthma::ea - bssstart;
            break;
	}
        datastart = asthma::ea;
        current_sec = 1;
      }
      break;

    case OP_bss:
      if ( current_sec == 2 )
        break;
      if ( bsssz != 0 )
      {
        asthma::error(asthma::file, asthma::line, asthma::line_orig, "a.out cannot switch back to an already written section!");
        return PROC_ASM_ERR_MISC;
      }
      else
      {
        switch ( current_sec )
        {
          case 0:
            textsz = asthma::ea - textstart;
            break;
          case 1:
            datasz = asthma::ea - datastart;
            break;
          case 2:
            bsssz = asthma::ea - bssstart;
            break;
	}
        bssstart = asthma::ea;
        current_sec = 2;
      }
      break;
  }
  return PROC_ASM_ERR_OK;
}

fmt_init_def(aout)
{
  return new aout_bin_t();
}
