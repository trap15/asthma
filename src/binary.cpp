/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "proc.hpp"
#include "parser.hpp"
#include "binary.hpp"

binary_t::binary_t()
{
}

binary_t::~binary_t()
{
}

void binary_t::init()
{
}

void binary_t::generate_output(std::map<ea_t,uint8_t> &inbin,
                               std::vector<symbol_t> &insyms,
                               std::vector<uint8_t> &outbin)
{
  ea_t low_addr=ea_t(-1), high_addr=0;
  for ( std::map<ea_t,uint8_t>::iterator p=inbin.begin(); p != inbin.end(); p++ )
  {
    if ( (*p).first < low_addr )
      low_addr = (*p).first;
    if ( (*p).first > high_addr )
      high_addr = (*p).first;
  }
  ea_t ea;
  for ( ea = low_addr; ea <= high_addr; ea++ )
  {
    uint8_t tmp = inbin[ea];
    outbin.push_back(tmp);
  }
}

void binary_t::print_options()
{
}

bool binary_t::set_option(const char *opt, bool small, const char *val, bool &care_param)
{
  return false;
}

int binary_t::process_pseudo_op(insn_t &cmd)
{
  return PROC_ASM_ERR_BAD_MNEM;
}

fmt_init_def(binary)
{
  return new binary_t();
}
