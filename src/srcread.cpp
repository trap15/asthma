/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "srcread.hpp"

uint32_t asthma::read_line(char **line, char *cmts, FILE *fp)
{
  char *ptr = NULL;
  uint32_t linesize = 8; // Most lines are going to be at least 8 chars
  uint32_t idx = 0;
  bool pfx_done = false;
  bool commenting = false;
  bool running = true;
  ptr = (char*)malloc(linesize);
  for ( idx = 0; running; idx++ )
  {
    if ( idx >= linesize )
    {
      linesize *= 2;
      ptr = (char*)realloc(ptr, linesize);
    }
    int c = fgetc(fp);
    int nc;
    char *cmt;
    for ( cmt=cmts; *cmt != '\0'; cmt++ )
      if ( c == *cmt )
        commenting = true;
    switch ( c )
    {
      case '\t':
      case ' ':
        if ( commenting )
        {
          idx--;
          break;
        }
        if ( pfx_done )
          ptr[idx] = ' ';
        else
          idx--; // Reverse the upcoming idx++
        while ( asthma::is_whitespace(nc = fgetc(fp)) );
        fseek(fp, -1, SEEK_CUR);
        break;
      case '\n':
      case '\r':
        idx--;
        running = false;
        nc = fgetc(fp);
        if ( ((c == '\r' && nc != '\n')  ||
              (c == '\n' && nc != '\r')) &&
              nc != EOF )
          fseek(fp, -1, SEEK_CUR);
        break;
      case ',':
        if ( commenting )
        {
          idx--;
          break;
        }
        ptr[idx] = ',';
        while ( asthma::is_whitespace(nc = fgetc(fp)) );
        fseek(fp, -1, SEEK_CUR);
        break;
      case '\0':
      case EOF:
        running = false;
        free(ptr);
        *line = NULL;
        return -1;
      default:
        if ( commenting )
        {
          idx--;
          break;
        }
        pfx_done = true;
        ptr[idx] = c;
        break;
    }
  }
  ptr[idx] = 0;
  *line = ptr;
  return idx;
}

bool asthma::is_whitespace(char ch)
{
  switch ( ch )
  {
    case '\t':
    case ' ':
      return true;
    default:
      return false;
  }
}

