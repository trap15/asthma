/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011        Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef PARSER_HPP_
#define PARSER_HPP_

#include "proc.hpp"
#include "binary.hpp"

#define LAST_PASS 1

namespace asthma
{
  extern std::vector<symbol_t> syms;
  extern proc_t *proc;
  extern int pass;
  extern ea_t ea;
  extern std::vector<symbol_t> nullsyms;
  extern symbol_t ea_sym;
  extern std::map<ea_t,uint8_t> curbin;
  extern char lastsymname[MAX_SYMNAME_LEN];
  extern FILE *fp;
  extern char *file;
  extern char *line_orig;
  extern uint32_t line;
  struct context_info
  {
    int type; // 0 - .repeat, 1 - .if/.elseif/.else, 2 - .macro
    int count;
    uint32_t start_line;
    uint32_t start_tell;
    uint32_t end_line;
    uint32_t end_tell;
  };
  extern std::vector<context_info> context_lines;
  extern uint32_t context_depth;
  extern uint32_t last_tell;
  extern bool parsing_off;
  extern binary_t *outmeth;

//------------------------------------------------------------------------------
  int process_pseudo_op(insn_t &cmd);

//------------------------------------------------------------------------------
  bool pass_single(char *file, FILE *fp, std::map<ea_t,uint8_t> &outbin, ea_t start_addr);
  bool parse(char *file, std::map<ea_t,uint8_t> &outbin, ea_t start_addr);

//------------------------------------------------------------------------------
  bool make_reg(char *str, op_t &op, char **upd=NULL);
  bool make_disp(char *str, op_t &op, char **upd=NULL);
  bool make_str(char *str, op_t &op, char **upd=NULL);
  bool make_num(char *str, op_t &op, char **upd=NULL, bool forceupd=false);

//------------------------------------------------------------------------------
  bool add_symbol(char *str, intmax_t val);
  bool check_syms(char *str, symbol_t **sym, char **upd=NULL);
  bool check_sym_exist(char *str, intmax_t val);
  bool valid_sym_char(char ch);
  uint32_t clean_sym(char *str, char &ch); // returns where a non-allowed character was first used
  uint32_t clean_sym(char *str);
};

#endif
