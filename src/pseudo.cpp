/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "proc.hpp"
#include "srcread.hpp"
#include "warn_err.hpp"
#include "parser.hpp"
#include "proclist.hpp"

enum pseudo_itypes
{
  OP_null = 0,

  OP_include,
  OP_incbin,

  OP_org,
  OP_align,
  OP_global,
  OP_extern,
  OP_cpu,
  OP_endian,
  OP_assume,

  OP_byte,
  OP_hword,
  OP_word,
  OP_dword,
  OP_qword,
  OP_space,
  OP_ds_h,
  OP_ds_w,
  OP_ds_d,
  OP_ds_q,
  OP_ascii,
  OP_asciz,

  OP_set,

  OP_if,
  OP_elseif,
  OP_else,
  OP_repeat,
  OP_end,

  OP_last,
};

int asthma::process_pseudo_op(insn_t &cmd)
{
  int itype = OP_null;

  cmd.len = 0;

  if ( !asthma::proc->case_sense )
  {
    unsigned int i;
    for ( i=0; i < strlen(cmd.mnem) && i < MAX_MNEM_LEN-1; i++ )
      cmd.mnem[i] = tolower(cmd.mnem[i]);
    for ( ; i < MAX_MNEM_LEN-1; i++ )
      cmd.mnem[i] = 0;
  }
  cmd.mnem[MAX_MNEM_LEN-1] = 0;

  mnem_check(cmd, ".include", OP_include);
  mnem_check(cmd, ".incbin", OP_incbin);

  mnem_check(cmd, ".org", OP_org);
  mnem_check(cmd, ".align", OP_align);
  mnem_check(cmd, ".global", OP_global);
  mnem_check(cmd, ".extern", OP_extern);
  mnem_check(cmd, ".cpu", OP_cpu);
  mnem_check(cmd, ".endian", OP_endian);
  mnem_check(cmd, ".assume", OP_assume);

  mnem_check(cmd, ".byte", OP_byte);
  mnem_check(cmd, ".db",   OP_byte);
  mnem_check(cmd, ".dc.b", OP_byte);
  mnem_check(cmd, ".hword", OP_hword);
  mnem_check(cmd, ".dh",    OP_hword);
  mnem_check(cmd, ".dc.h",  OP_hword);
  mnem_check(cmd, ".word", OP_word);
  mnem_check(cmd, ".dw",   OP_word);
  mnem_check(cmd, ".dc.w", OP_word);
  mnem_check(cmd, ".dword", OP_dword);
  mnem_check(cmd, ".long",  OP_dword);
  mnem_check(cmd, ".dd",    OP_dword);
  mnem_check(cmd, ".dl",    OP_dword);
  mnem_check(cmd, ".dc.d",  OP_dword);
  mnem_check(cmd, ".dc.l",  OP_dword);
  mnem_check(cmd, ".qword", OP_qword);
  mnem_check(cmd, ".dq",    OP_qword);
  mnem_check(cmd, ".dc.q",  OP_qword);
  mnem_check(cmd, ".space", OP_space);
  mnem_check(cmd, ".ds.b",  OP_space);
  mnem_check(cmd, ".ds.h",  OP_ds_h);
  mnem_check(cmd, ".ds.w",  OP_ds_w);
  mnem_check(cmd, ".ds.d",  OP_ds_d);
  mnem_check(cmd, ".ds.l",  OP_ds_d);
  mnem_check(cmd, ".ds.q",  OP_ds_q);
  mnem_check(cmd, ".ascii", OP_ascii);
  mnem_check(cmd, ".asciz", OP_asciz);

  mnem_check(cmd, ".set", OP_set);

  mnem_check(cmd, ".if", OP_if);
  mnem_check(cmd, ".elseif", OP_elseif);
  mnem_check(cmd, ".else", OP_else);
  mnem_check(cmd, ".repeat", OP_repeat);
  mnem_check(cmd, ".rept",   OP_repeat);
  mnem_check(cmd, ".end", OP_end);

  if ( asthma::parsing_off )
  {
    if ( itype != OP_if &&
         itype != OP_elseif &&
         itype != OP_else &&
         itype != OP_repeat &&
         itype != OP_end )
    {
      return PROC_ASM_ERR_OK;
    }
  }

  switch ( itype )
  {
    default:
    case OP_null:
      return PROC_ASM_ERR_BAD_MNEM;

    case OP_include:
    {
      if ( asthma::context_depth != 0 ) return PROC_ASM_ERR_UNK;
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_STR ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      FILE *fp = fopen(cmd.op[0].str, "rb");
      if ( fp == NULL )
        return PROC_ASM_ERR_FILE_NOT_FOUND;
      // Save context
      char *asthma_file = asthma::file;
      char *asthma_line_orig = asthma::line_orig;
      uint32_t asthma_line = asthma::line;
      uint32_t asthma_last_tell = asthma::last_tell;
      FILE *asthma_fp = asthma::fp;

      bool error = pass_single(cmd.op[0].str, fp, asthma::curbin, asthma::ea);
      // Restore context
      asthma::file = asthma_file;
      asthma::line_orig = asthma_line_orig;
      asthma::line = asthma_line;
      asthma::last_tell = asthma_last_tell;
      asthma::fp = asthma_fp;

      fclose(fp);
      if ( error )
        return PROC_ASM_ERR_MISC;
      break;
    }

    case OP_incbin:
    {
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_STR ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      FILE *fp = fopen(cmd.op[0].str, "rb");
      if ( fp == NULL )
        return PROC_ASM_ERR_FILE_NOT_FOUND;
      fseek(fp, 0, SEEK_END);
      uintmax_t size = ftell(fp);
      fseek(fp, 0, SEEK_SET);
      cmd.code.resize(size);
      uintmax_t i;
      for ( i=0; i < size; i++ )
      {
        int c = fgetc(fp);
        if ( c == EOF )
          break;
        cmd.code[i] = c;
      }
      cmd.len = i;
      fclose(fp);
      break;
    }

    case OP_org:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM && cmd.op[0].type != OPT_ADDR ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      asthma::ea = cmd.op[0].value;
      if ( asthma::ea < asthma::proc->bot_addr ||
           asthma::ea > asthma::proc->top_addr )
        return PROC_ASM_ERR_EA_INVALID;
      break;

    case OP_align:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      switch ( cmd.op[0].value )
      {
        case 2: case 4: case 8: case 16: case 32: case 64: case 128: case 256:
        case 512: case 1024: case 2048: case 4096: case 8192: case 16384: case 32768:
        case 65536:
          cmd.op[0].value--;
          asthma::ea += cmd.op[0].value;
          asthma::ea &= ~cmd.op[0].value;
          break;
        default:
          return PROC_ASM_ERR_BAD_OP0_VAL;
      }
      if ( asthma::ea < asthma::proc->bot_addr ||
           asthma::ea > asthma::proc->top_addr )
        return PROC_ASM_ERR_EA_INVALID;
      break;

    case OP_cpu:
    {
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      delete asthma::proc;
      proc_def_list_entry *ptr;
      for ( ptr=proc_list; ptr->procinit != NULL; ptr++ )
      {
        if ( strncmp(ptr->name, cmd.op[0].argstr, MAX_PROCNAME_LEN) == 0 )
        {
          asthma::proc = ptr->procinit();
          break;
        }
      }
      if ( ptr->procinit == NULL )
        return PROC_ASM_ERR_BAD_OP0_VAL;
      break;
    }

    case OP_endian:
    {
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      int tgt_endian;
      if ( (strcmp(cmd.op[0].argstr, "big") == 0) ||
           (strcmp(cmd.op[0].argstr, "BIG") == 0) ||
           (strcmp(cmd.op[0].argstr, "be") == 0) ||
           (strcmp(cmd.op[0].argstr, "BE") == 0) )
      {
        tgt_endian = PROC_ENDIAN_BE;
      }
      else if ( (strcmp(cmd.op[0].argstr, "little") == 0) ||
                (strcmp(cmd.op[0].argstr, "LITTLE") == 0) ||
                (strcmp(cmd.op[0].argstr, "le") == 0) ||
                (strcmp(cmd.op[0].argstr, "LE") == 0) )
      {
        tgt_endian = PROC_ENDIAN_LE;
      }
      else if ( (strcmp(cmd.op[0].argstr, "pdp") == 0) ||
                (strcmp(cmd.op[0].argstr, "PDP") == 0) )
      {
        tgt_endian = PROC_ENDIAN_PDP;
      }
      else
        return PROC_ASM_ERR_BAD_OP0_VAL;

      if ( asthma::proc->allowed_endians & (1 << tgt_endian) )
        asthma::proc->endian = tgt_endian;
      else
        return PROC_ASM_ERR_BAD_OP0_VAL;
      break;
    }

    case OP_assume:
    {
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[1].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP1_TYPE;
      if ( !asthma::proc->assume_reg(cmd.op[0].argstr, cmd.op[1].value) )
        return PROC_ASM_ERR_BAD_OP0_VAL;
      break;
    }

#define CHUNKOUT(len, idx, val) do { \
      for ( int l=0; l < (len); l++ ) \
      { \
        int bytesrc; \
        switch ( asthma::proc->endian ) \
        { \
          case PROC_ENDIAN_BE: \
            bytesrc = ((len)-l-1)*8; \
            break; \
          case PROC_ENDIAN_LE: \
            bytesrc = l*8; \
            break; \
          case PROC_ENDIAN_PDP: \
            len = 0; \
            return PROC_ASM_ERR_UNIMP; \
          default: \
            bytesrc = 0; \
            len = 0; \
            return PROC_ASM_ERR_UNIMP; \
        } \
        cmd.code[l+(idx)] = ((val) >> bytesrc) & 0xFF; \
      } \
    } while(0)

    case OP_byte:
      cmd.len = 1;
      goto OUTPUT;
    case OP_hword:
      cmd.len = asthma::proc->word_size >> 1;
      goto OUTPUT;
    case OP_word:
      cmd.len = asthma::proc->word_size;
      goto OUTPUT;
    case OP_dword:
      cmd.len = asthma::proc->word_size << 1;
      goto OUTPUT;
    case OP_qword:
      cmd.len = asthma::proc->word_size << 2;
OUTPUT:
    {
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      for ( int i=0; i < cmd.opcnt; i++ )
      {
        if ( cmd.op[i].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP_TYPE;
        uintmax_t val = cmd.op[i].value;
        CHUNKOUT(cmd.len, i*cmd.len, val);
      }
      cmd.len *= cmd.opcnt;
      break;
    }

    case OP_space:
      cmd.len = 1;
      goto SPACEOUT;
    case OP_ds_h:
      cmd.len = asthma::proc->word_size >> 1;
      goto SPACEOUT;
    case OP_ds_w:
      cmd.len = asthma::proc->word_size;
      goto SPACEOUT;
    case OP_ds_d:
      cmd.len = asthma::proc->word_size << 1;
      goto SPACEOUT;
    case OP_ds_q:
      cmd.len = asthma::proc->word_size << 2;
SPACEOUT:
    {
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.opcnt > 1 && cmd.op[1].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP1_TYPE;
      int valsz = cmd.len;
      cmd.len *= cmd.op[0].value;
      uintmax_t val = 0;
      if ( cmd.opcnt > 1 )
        val = cmd.op[1].value;
      cmd.code.resize(cmd.len);
      for ( int i=0; i < cmd.len; i += valsz )
      {
        CHUNKOUT(valsz, i, val);
      }
      break;
    }

    case OP_asciz:
    case OP_ascii:
    {
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      size_t l;
      int i;
      for ( i=0, l=0; i < cmd.opcnt; i++ )
      {
        if ( cmd.op[i].type != OPT_STR ) return PROC_ASM_ERR_BAD_OP0_TYPE;
        cmd.len += strlen(cmd.op[i].str);
        if ( itype == OP_asciz ) // Zero terminate
          cmd.len++;
        cmd.code.resize(cmd.len);
        for ( size_t z=0; l < cmd.len; l++, z++ )
          cmd.code[l] = cmd.op[i].str[z];
      }
      break;
    }

    case OP_global:
      if ( asthma::pass != LAST_PASS )
        break;
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].sym == NULL ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( !cmd.op[0].sym->valid ) return PROC_ASM_ERR_BAD_OP0_VAL;
      cmd.op[0].sym->make_global();
      break;

    case OP_extern:
    {
      symbol_t *nsym;
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      asthma::add_symbol(cmd.op[0].argstr, 0);
      asthma::check_syms(cmd.op[0].argstr, &nsym);
      nsym->make_extern();
      break;
    }

    case OP_set:
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[1].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP1_TYPE;
      if ( cmd.op[1].sym != NULL ) // If we're setting to an invalid symbol, don't bother
        if ( !cmd.op[1].sym->valid )
          break;
      if ( !asthma::add_symbol(cmd.op[0].argstr, cmd.op[1].value) )
      {
        return PROC_ASM_ERR_MISC;
      }
      break;

    case OP_if:
    {
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      asthma::context_info ctxinfo;
      ctxinfo.type = 1;
      ctxinfo.start_tell = asthma::last_tell;
      ctxinfo.start_line = asthma::line;
      if ( !asthma::parsing_off )
      {
        ctxinfo.count = cmd.op[0].value ? 1 : 0;
        asthma::parsing_off = cmd.op[0].value ? false : true;
      }
      else
        ctxinfo.count = 2;
      asthma::context_lines.push_back(ctxinfo);
      asthma::context_depth++;
      break;
    }

    case OP_elseif:
    {
      if ( asthma::context_depth <= 0 ) return PROC_ASM_ERR_UNK;
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( asthma::parsing_off )
      {
        asthma::context_info &ctxinfo = asthma::context_lines[asthma::context_depth-1];
        if ( ctxinfo.count != 0 )
        {
          asthma::parsing_off = true;
          break;
        }
        ctxinfo.start_tell = asthma::last_tell;
        ctxinfo.start_line = asthma::line;
        ctxinfo.count = cmd.op[0].value ? 1 : 0;
        asthma::parsing_off = cmd.op[0].value ? false : true;
      }
      else
      {
        asthma::parsing_off = true;
      }
      break;
    }

    case OP_else:
    {
      if ( asthma::context_depth <= 0 ) return PROC_ASM_ERR_UNK;
      if ( cmd.opcnt > 0 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 0 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( asthma::parsing_off )
      {
        asthma::context_info &ctxinfo = asthma::context_lines[asthma::context_depth-1];
        if ( ctxinfo.count != 0 )
        {
          asthma::parsing_off = true;
          break;
        }
        ctxinfo.start_tell = asthma::last_tell;
        ctxinfo.start_line = asthma::line;
        ctxinfo.count = 1;
        asthma::parsing_off = false;
      }
      else
      {
        asthma::parsing_off = true;
      }
      break;
    }

    case OP_repeat:
    {
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      asthma::context_info ctxinfo;
      ctxinfo.type = 0;
      ctxinfo.start_tell = asthma::last_tell;
      ctxinfo.start_line = asthma::line;
      if ( asthma::parsing_off )
        ctxinfo.count = 1;
      else
        ctxinfo.count = cmd.op[0].value;
      asthma::context_lines.push_back(ctxinfo);
      asthma::context_depth++;
      break;
    }

    case OP_end:
    {
      if ( asthma::context_depth <= 0 ) return PROC_ASM_ERR_UNK;
      if ( cmd.opcnt > 0 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 0 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      asthma::context_info &ctxinfo = asthma::context_lines[asthma::context_depth-1];
      ctxinfo.end_tell = asthma::last_tell;
      ctxinfo.end_line = asthma::line;
      switch ( ctxinfo.type )
      {
        case 0: // .repeat
          if ( --ctxinfo.count == 0 )
          {
            asthma::context_depth--;
            asthma::context_lines.pop_back();
          }
          else
          {
            fseek(asthma::fp, ctxinfo.start_tell, SEEK_SET);
            asthma::line = ctxinfo.start_line;
          }
          break;
        case 1: // .if/.elseif/.else
          if ( ctxinfo.count != 2 )
          {
            asthma::parsing_off = false;
          }
          asthma::context_depth--;
          asthma::context_lines.pop_back();
          break;
      }
      break;
    }
  }
  return PROC_ASM_ERR_OK;
}




