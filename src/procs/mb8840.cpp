/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "parser.hpp"
#include "proc.hpp"

struct mb8840_proc_t : proc_t
{
  mb8840_proc_t();
  virtual ~mb8840_proc_t();
//--- Variables
//--- Functions
  virtual bool assume_reg(char *reg, uintmax_t value);
  virtual int process_insn(insn_t &cmd);
};

mb8840_proc_t::mb8840_proc_t()
{
  // Basic info
  sprintf(name, "MB8840");
  sprintf(cmts, ";");
  case_sense = false;
  deref_style = PROC_DEREF_NUM_BRACKS_REG;
  word_size = 1;
  addr_style = PROC_ADDR_NONE;
  endian = PROC_ENDIAN_LE;
  bot_addr = 0x0000;
  top_addr = 0xFFFF;
  num_pfx = '\0';
  allowed_endians = 0; // No endians.
}

mb8840_proc_t::~mb8840_proc_t()
{
}

bool mb8840_proc_t::assume_reg(char *reg, uintmax_t value)
{
  return false;
}

enum mb8840_itypes
{
  MB8840_null = 0,

  // insn
  MB8840_tath,
  MB8840_tatl,
  MB8840_tas,
  MB8840_tay,
  MB8840_tsa,
  MB8840_ttha,
  MB8840_ttla,
  MB8840_tya,
  MB8840_xx,

  MB8840_l,
  MB8840_ls,
  MB8840_st,
  MB8840_stdc,
  MB8840_stic,
  MB8840_sts,
  MB8840_x,

  MB8840_cla,

  MB8840_adc,
  MB8840_and,
  MB8840_c,
  MB8840_daa,
  MB8840_das,
  MB8840_dca,
  MB8840_dcm,
  MB8840_dcy,
  MB8840_eor,
  MB8840_ica,
  MB8840_icm,
  MB8840_icy,
  MB8840_neg,
  MB8840_or,
  MB8840_rol,
  MB8840_ror,
  MB8840_sbc,

  MB8840_in,
  MB8840_ink,
  MB8840_out,
  MB8840_outo,
  MB8840_outp,
  MB8840_rstr,
  MB8840_setr,
  MB8840_tstr,

  MB8840_rti,
  MB8840_rts,

  MB8840_rstc,
  MB8840_setc,
  MB8840_tstc,
  MB8840_tsti,
  MB8840_tsts,
  MB8840_tstv,
  MB8840_tstz,

  MB8840_nop,

  // insn imm2
  MB8840_xd,
  MB8840_xyd,

  MB8840_rbit,
  MB8840_sbit,
  MB8840_tba,
  MB8840_tbit,

  MB8840_rstd,
  MB8840_setd,
  MB8840_tstd,

  // insn imm3
  MB8840_lxi,

  // insn imm4
  MB8840_li,
  MB8840_lyi,

  MB8840_ai,
  MB8840_ci,
  MB8840_cyi,

  // insn imm6
  MB8840_jmp,
  MB8840_jpa,

  // insn imm8
  MB8840_en,
  MB8840_dis,

  // insn imm11
  MB8840_call,
  MB8840_jpl,

  MB8840_last
};

int mb8840_proc_t::process_insn(insn_t &cmd)
{
  int itype = MB8840_null;
  cmd.len = 1;

  mnem_check(cmd, "tath", MB8840_tath);
  mnem_check(cmd, "tatl", MB8840_tatl);
  mnem_check(cmd, "tas", MB8840_tas);
  mnem_check(cmd, "tay", MB8840_tay);
  mnem_check(cmd, "tsa", MB8840_tsa);
  mnem_check(cmd, "ttha", MB8840_ttha);
  mnem_check(cmd, "ttla", MB8840_ttla);
  mnem_check(cmd, "tya", MB8840_tya);
  mnem_check(cmd, "xx", MB8840_xx);

  mnem_check(cmd, "l", MB8840_l);
  mnem_check(cmd, "ls", MB8840_ls);
  mnem_check(cmd, "st", MB8840_st);
  mnem_check(cmd, "stdc", MB8840_stdc);
  mnem_check(cmd, "stic", MB8840_stic);
  mnem_check(cmd, "sts", MB8840_sts);
  mnem_check(cmd, "x", MB8840_x);

  mnem_check(cmd, "cla", MB8840_cla);

  mnem_check(cmd, "adc", MB8840_adc);
  mnem_check(cmd, "and", MB8840_and);
  mnem_check(cmd, "c", MB8840_c);
  mnem_check(cmd, "daa", MB8840_daa);
  mnem_check(cmd, "das", MB8840_das);
  mnem_check(cmd, "dca", MB8840_dca);
  mnem_check(cmd, "dcm", MB8840_dcm);
  mnem_check(cmd, "dcy", MB8840_dcy);
  mnem_check(cmd, "eor", MB8840_eor);
  mnem_check(cmd, "ica", MB8840_ica);
  mnem_check(cmd, "icm", MB8840_icm);
  mnem_check(cmd, "icy", MB8840_icy);
  mnem_check(cmd, "neg", MB8840_neg);
  mnem_check(cmd, "or", MB8840_or);
  mnem_check(cmd, "rol", MB8840_rol);
  mnem_check(cmd, "ror", MB8840_ror);
  mnem_check(cmd, "sbc", MB8840_sbc);

  mnem_check(cmd, "in", MB8840_in);
  mnem_check(cmd, "ink", MB8840_ink);
  mnem_check(cmd, "out", MB8840_out);
  mnem_check(cmd, "outo", MB8840_outo);
  mnem_check(cmd, "outp", MB8840_outp);
  mnem_check(cmd, "rstr", MB8840_rstr);
  mnem_check(cmd, "setr", MB8840_setr);
  mnem_check(cmd, "tstr", MB8840_tstr);

  mnem_check(cmd, "rti", MB8840_rti);
  mnem_check(cmd, "rts", MB8840_rts);

  mnem_check(cmd, "rstc", MB8840_rstc);
  mnem_check(cmd, "setc", MB8840_setc);
  mnem_check(cmd, "tstc", MB8840_tstc);
  mnem_check(cmd, "tsti", MB8840_tsti);
  mnem_check(cmd, "tsts", MB8840_tsts);
  mnem_check(cmd, "tstv", MB8840_tstv);
  mnem_check(cmd, "tstz", MB8840_tstz);

  mnem_check(cmd, "nop", MB8840_nop);

  // insn imm2
  mnem_check(cmd, "xd", MB8840_xd);
  mnem_check(cmd, "xyd", MB8840_xyd);

  mnem_check(cmd, "rbit", MB8840_rbit);
  mnem_check(cmd, "sbit", MB8840_sbit);
  mnem_check(cmd, "tba", MB8840_tba);
  mnem_check(cmd, "tbit", MB8840_tbit);

  mnem_check(cmd, "rstd", MB8840_rstd);
  mnem_check(cmd, "setd", MB8840_setd);
  mnem_check(cmd, "tstd", MB8840_tstd);

  // insn imm3
  mnem_check(cmd, "lxi", MB8840_lxi);

  // insn imm4
  mnem_check(cmd, "li", MB8840_li);
  mnem_check(cmd, "lyi", MB8840_lyi);

  mnem_check(cmd, "ai", MB8840_ai);
  mnem_check(cmd, "ci", MB8840_ci);
  mnem_check(cmd, "cyi", MB8840_cyi);

  // insn imm6
  mnem_check(cmd, "jmp", MB8840_jmp);
  mnem_check(cmd, "jpa", MB8840_jpa);

  // insn imm8
  mnem_check(cmd, "en", MB8840_en);
  mnem_check(cmd, "dis", MB8840_dis);

  // insn imm11
  mnem_check(cmd, "call", MB8840_call);
  mnem_check(cmd, "jpl", MB8840_jpl);
  mnem_check(cmd, "nop", MB8840_nop);

  switch ( itype )
  {
    default:
    case MB8840_null:
      return PROC_ASM_ERR_BAD_MNEM;

    case MB8840_nop:  cmd.code[0] = 0x00; goto MB8840_NO_ARGS;
    case MB8840_outo: cmd.code[0] = 0x01; goto MB8840_NO_ARGS;
    case MB8840_outp: cmd.code[0] = 0x02; goto MB8840_NO_ARGS;
    case MB8840_out:  cmd.code[0] = 0x03; goto MB8840_NO_ARGS;
    case MB8840_tay:  cmd.code[0] = 0x04; goto MB8840_NO_ARGS;
    case MB8840_tath: cmd.code[0] = 0x05; goto MB8840_NO_ARGS;
    case MB8840_tatl: cmd.code[0] = 0x06; goto MB8840_NO_ARGS;
    case MB8840_tas:  cmd.code[0] = 0x07; goto MB8840_NO_ARGS;
    case MB8840_icy:  cmd.code[0] = 0x08; goto MB8840_NO_ARGS;
    case MB8840_icm:  cmd.code[0] = 0x09; goto MB8840_NO_ARGS;
    case MB8840_stic: cmd.code[0] = 0x0A; goto MB8840_NO_ARGS;
    case MB8840_x:    cmd.code[0] = 0x0B; goto MB8840_NO_ARGS;
    case MB8840_rol:  cmd.code[0] = 0x0C; goto MB8840_NO_ARGS;
    case MB8840_l:    cmd.code[0] = 0x0D; goto MB8840_NO_ARGS;
    case MB8840_adc:  cmd.code[0] = 0x0E; goto MB8840_NO_ARGS;
    case MB8840_and:  cmd.code[0] = 0x0F; goto MB8840_NO_ARGS;

    case MB8840_daa:  cmd.code[0] = 0x10; goto MB8840_NO_ARGS;
    case MB8840_das:  cmd.code[0] = 0x11; goto MB8840_NO_ARGS;
    case MB8840_ink:  cmd.code[0] = 0x12; goto MB8840_NO_ARGS;
    case MB8840_in:   cmd.code[0] = 0x13; goto MB8840_NO_ARGS;
    case MB8840_tya:  cmd.code[0] = 0x14; goto MB8840_NO_ARGS;
    case MB8840_ttha: cmd.code[0] = 0x15; goto MB8840_NO_ARGS;
    case MB8840_ttla: cmd.code[0] = 0x16; goto MB8840_NO_ARGS;
    case MB8840_tsa:  cmd.code[0] = 0x17; goto MB8840_NO_ARGS;
    case MB8840_dcy:  cmd.code[0] = 0x18; goto MB8840_NO_ARGS;
    case MB8840_dcm:  cmd.code[0] = 0x19; goto MB8840_NO_ARGS;
    case MB8840_stdc: cmd.code[0] = 0x1A; goto MB8840_NO_ARGS;
    case MB8840_xx:   cmd.code[0] = 0x1B; goto MB8840_NO_ARGS;
    case MB8840_ror:  cmd.code[0] = 0x1C; goto MB8840_NO_ARGS;
    case MB8840_st:   cmd.code[0] = 0x1D; goto MB8840_NO_ARGS;
    case MB8840_sbc:  cmd.code[0] = 0x1E; goto MB8840_NO_ARGS;
    case MB8840_or:   cmd.code[0] = 0x1F; goto MB8840_NO_ARGS;

    case MB8840_setr: cmd.code[0] = 0x20; goto MB8840_NO_ARGS;
    case MB8840_setc: cmd.code[0] = 0x21; goto MB8840_NO_ARGS;
    case MB8840_rstr: cmd.code[0] = 0x22; goto MB8840_NO_ARGS;
    case MB8840_rstc: cmd.code[0] = 0x23; goto MB8840_NO_ARGS;
    case MB8840_tstr: cmd.code[0] = 0x24; goto MB8840_NO_ARGS;
    case MB8840_tsti: cmd.code[0] = 0x25; goto MB8840_NO_ARGS;
    case MB8840_tstv: cmd.code[0] = 0x26; goto MB8840_NO_ARGS;
    case MB8840_tsts: cmd.code[0] = 0x27; goto MB8840_NO_ARGS;
    case MB8840_tstc: cmd.code[0] = 0x28; goto MB8840_NO_ARGS;
    case MB8840_tstz: cmd.code[0] = 0x29; goto MB8840_NO_ARGS;
    case MB8840_sts:  cmd.code[0] = 0x2A; goto MB8840_NO_ARGS;
    case MB8840_ls:   cmd.code[0] = 0x2B; goto MB8840_NO_ARGS;
    case MB8840_rts:  cmd.code[0] = 0x2C; goto MB8840_NO_ARGS;
    case MB8840_neg:  cmd.code[0] = 0x2D; goto MB8840_NO_ARGS;
    case MB8840_c:    cmd.code[0] = 0x2E; goto MB8840_NO_ARGS;
    case MB8840_eor:  cmd.code[0] = 0x2F; goto MB8840_NO_ARGS;

    case MB8840_sbit: cmd.code[0] = 0x30; goto MB8840_IMM2;
    case MB8840_rbit: cmd.code[0] = 0x34; goto MB8840_IMM2;
    case MB8840_tbit: cmd.code[0] = 0x38; goto MB8840_IMM2;
    case MB8840_rti:  cmd.code[0] = 0x3C; goto MB8840_NO_ARGS;
    case MB8840_jpa:  cmd.code[0] = 0x3D; goto MB8840_IMM8;
    case MB8840_en:   cmd.code[0] = 0x3E; goto MB8840_IMM8;
    case MB8840_dis:  cmd.code[0] = 0x3F; goto MB8840_IMM8;

    case MB8840_setd: cmd.code[0] = 0x40; goto MB8840_IMM2;
    case MB8840_rstd: cmd.code[0] = 0x44; goto MB8840_IMM2;
    case MB8840_tstd: cmd.code[0] = 0x48; goto MB8840_IMM2;
    case MB8840_tba:  cmd.code[0] = 0x4C; goto MB8840_IMM2;

    case MB8840_xd:   cmd.code[0] = 0x50; goto MB8840_IMM2;
    case MB8840_xyd:  cmd.code[0] = 0x54; goto MB8840_IMM2;
    case MB8840_lxi:  cmd.code[0] = 0x58; goto MB8840_IMM3;

    case MB8840_call: cmd.code[0] = 0x60; goto MB8840_IMM11;
    case MB8840_jpl:  cmd.code[0] = 0x68; goto MB8840_IMM11;

    case MB8840_ai:   cmd.code[0] = 0x70; goto MB8840_IMM4;
    case MB8840_ica:  cmd.code[0] = 0x71; goto MB8840_NO_ARGS;
    case MB8840_dca:  cmd.code[0] = 0x7F; goto MB8840_NO_ARGS;

    case MB8840_lyi:  cmd.code[0] = 0x80; goto MB8840_IMM4;

    case MB8840_li:   cmd.code[0] = 0x90; goto MB8840_IMM4;
    case MB8840_cla:  cmd.code[0] = 0x90; goto MB8840_NO_ARGS;

    case MB8840_cyi:  cmd.code[0] = 0xA0; goto MB8840_IMM4;

    case MB8840_ci:   cmd.code[0] = 0xB0; goto MB8840_IMM4;

    case MB8840_jmp:  cmd.code[0] = 0xC0; goto MB8840_IMM6;

MB8840_NO_ARGS:
      if ( cmd.opcnt > 0 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      break;

MB8840_IMM2:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( itype == MB8840_xyd ) cmd.op[0].value -= 4;
      if ( cmd.op[0].value < 0 || cmd.op[0].value > 3 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      cmd.code[0] |= cmd.op[0].value;
      break;

MB8840_IMM3:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value < 0 || cmd.op[0].value > 7 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      cmd.code[0] |= cmd.op[0].value;
      break;

MB8840_IMM4:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value < 0 || cmd.op[0].value > 15 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      cmd.code[0] |= cmd.op[0].value;
      break;

MB8840_IMM6:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value < 0 || cmd.op[0].value > 0x3F ) return PROC_ASM_ERR_BAD_OP0_VAL;
      cmd.code[0] |= cmd.op[0].value;
      break;

MB8840_IMM8:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value < 0 || cmd.op[0].value > 0xFF ) return PROC_ASM_ERR_BAD_OP0_VAL;
      cmd.code[1] = cmd.op[0].value;
      cmd.len++;
      break;

MB8840_IMM11:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value < 0 || cmd.op[0].value > 0x7FF ) return PROC_ASM_ERR_BAD_OP0_VAL;
      cmd.code[0] |= (cmd.op[0].value >> 8) & 7;
      cmd.code[1]  = cmd.op[0].value & 0xFF;
      cmd.len++;
      break;
  }

  return PROC_ASM_ERR_OK;
}

proc_init_def(mb8840)
{
  return new mb8840_proc_t();
}
