/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "parser.hpp"
#include "proc.hpp"

struct reco_proc_t : proc_t
{
  reco_proc_t();
  virtual ~reco_proc_t();
//--- Variables
//--- Functions
  virtual bool assume_reg(char *reg, uintmax_t value);
  virtual int process_insn(insn_t &cmd);
};

reco_proc_t::reco_proc_t()
{
  // Basic info
  sprintf(name, "RECO");
  sprintf(cmts, ";");
  case_sense = false;
  deref_style = PROC_DEREF_NUM_PARENS_REG;
  word_size = 2;
  addr_style = PROC_ADDR_PARENS;
  endian = PROC_ENDIAN_BE;
  bot_addr = 0x000000;
  top_addr = 0xFFFFFF;
  num_pfx = '#';
  allowed_endians = (1 << PROC_ENDIAN_BE);

  // Push some pre-defined symbols

  // Register names
  regs.push_back(reg_t("r0",   0));
  regs.push_back(reg_t("zero", 0));
  regs.push_back(reg_t("r1",   1));
  regs.push_back(reg_t("r2",   2));
  regs.push_back(reg_t("r3",   3));
  regs.push_back(reg_t("r4",   4));
  regs.push_back(reg_t("r5",   5));
  regs.push_back(reg_t("r6",   6));
  regs.push_back(reg_t("r7",   7));
  regs.push_back(reg_t("sp",   7));
  regs.push_back(reg_t("r8",   8));
  regs.push_back(reg_t("r9",   9));
  regs.push_back(reg_t("r10", 10));
  regs.push_back(reg_t("r11", 11));
  regs.push_back(reg_t("r12", 12));
  regs.push_back(reg_t("r13", 13));
  regs.push_back(reg_t("r14", 14));
  regs.push_back(reg_t("r15", 15));

  regs.push_back(reg_t("dbr", 16));
}

reco_proc_t::~reco_proc_t()
{
}

bool reco_proc_t::assume_reg(char *reg, uintmax_t value)
{
  return false;
}

enum reco_itypes
{
  RECO_null = 0,

  // insn Rn,<ea>
  // insn <ea>,Rn
  RECO_add_b,
  RECO_add_w,
  RECO_addc_b,
  RECO_addc_w,
  RECO_sub_b,
  RECO_sub_w,
  RECO_subc_b,
  RECO_subc_w,
  RECO_cmp_b,
  RECO_cmp_w,
  RECO_move_b,
  RECO_move_w,
  RECO_and_b,
  RECO_and_w,
  RECO_or_b,
  RECO_or_w,
  RECO_xor_b,
  RECO_xor_w,

  // insn <ea>
  RECO_not_b,
  RECO_not_w,
  RECO_neg_b,
  RECO_neg_w,
  RECO_tst_b,
  RECO_tst_w,
  RECO_jmp, RECO_jmpeq, RECO_jmpne, RECO_jmpcs, RECO_jmpcc,
  RECO_jmpn, RECO_jmpp, RECO_jmpvs, RECO_jmpvc,
  RECO_jsr, RECO_jsreq, RECO_jsrne, RECO_jsrcs, RECO_jsrcc,
  RECO_jsrn, RECO_jsrp, RECO_jsrvs, RECO_jsrvc,

  // insn Rn,<ea>
  // insn #imm4,<ea>
  RECO_btst_b,
  RECO_btst_w,
  RECO_bset_b,
  RECO_bset_w,
  RECO_bclr_b,
  RECO_bclr_w,

  // insn Rn,Rc
  // insn #imm3,Rc
  RECO_lsl_b,
  RECO_lsl_w,
  RECO_lsr_b,
  RECO_lsr_w,
  RECO_asr_b,
  RECO_asr_w,
  RECO_rol_b,
  RECO_rol_w,
  RECO_rolc_b,
  RECO_rolc_w,
  RECO_ror_b,
  RECO_ror_w,
  RECO_rorc_b,
  RECO_rorc_w,

  // insn
  RECO_break,
  RECO_trap,
  RECO_halt,
  RECO_nop,
  RECO_rts,
  RECO_rte,
  RECO_mctl,
  RECO_xchg,

  // insn disp8
  RECO_beq, RECO_bne, RECO_bcs, RECO_bcc, RECO_bsr,
  RECO_bn,  RECO_bp,  RECO_bvs, RECO_bvc, RECO_bra,
  RECO_beq_s, RECO_bne_s, RECO_bcs_s, RECO_bcc_s, RECO_bsr_s,
  RECO_bn_s,  RECO_bp_s,  RECO_bvs_s, RECO_bvc_s, RECO_bra_s,
  RECO_beq_l, RECO_bne_l, RECO_bcs_l, RECO_bcc_l, RECO_bsr_l,
  RECO_bn_l,  RECO_bp_l,  RECO_bvs_l, RECO_bvc_l, RECO_bra_l,

  RECO_dline,
  RECO_eline,
  RECO_fline,

  RECO__b,
  RECO__w,
  RECO_last
};

#define MNEM_CHK_SZ(mnem) \
  mnem_check(cmd, #mnem ".b", RECO_##mnem##_b); \
  mnem_check(cmd, #mnem ".w", RECO_##mnem##_w); \
  mnem_check(cmd, #mnem     , RECO_##mnem##_w)
#define MNEM_CHK_LEN(mnem) \
  mnem_check(cmd, #mnem ".s", RECO_##mnem##_s); \
  mnem_check(cmd, #mnem ".l", RECO_##mnem##_l); \
  mnem_check(cmd, #mnem     , RECO_##mnem)

int reco_proc_t::process_insn(insn_t &cmd)
{
  uint16_t code = 0xFFFF;
  int itype = RECO_null;
  op_t *op0, *ea;
  int ea_opnum;
  cmd.len = 2;

  mnem_check(cmd, "break", RECO_break);
  mnem_check(cmd, "trap",  RECO_trap);
  mnem_check(cmd, "halt",  RECO_halt);
  mnem_check(cmd, "nop",   RECO_nop);
  mnem_check(cmd, "rts",   RECO_rts);
  mnem_check(cmd, "rte",   RECO_rte);
  mnem_check(cmd, "mctl",  RECO_mctl);
  mnem_check(cmd, "xchg",  RECO_xchg);

  mnem_check(cmd, "dline", RECO_dline);
  mnem_check(cmd, "eline", RECO_eline);
  mnem_check(cmd, "fline", RECO_fline);

  MNEM_CHK_SZ(add);
  MNEM_CHK_SZ(addc);
  MNEM_CHK_SZ(sub);
  MNEM_CHK_SZ(subc);
  MNEM_CHK_SZ(not);
  MNEM_CHK_SZ(neg);
  MNEM_CHK_SZ(tst);
  MNEM_CHK_SZ(cmp);
  MNEM_CHK_SZ(move);
  MNEM_CHK_SZ(and);
  MNEM_CHK_SZ(or);
  MNEM_CHK_SZ(xor);

  MNEM_CHK_SZ(btst);
  MNEM_CHK_SZ(bset);
  MNEM_CHK_SZ(bclr);

  MNEM_CHK_SZ(lsl);
  MNEM_CHK_SZ(lsr);
  MNEM_CHK_SZ(asr);
  MNEM_CHK_SZ(rol);
  MNEM_CHK_SZ(rolc);
  MNEM_CHK_SZ(ror);
  MNEM_CHK_SZ(rorc);

  mnem_check(cmd, "jmp",   RECO_jmp);
  mnem_check(cmd, "jmpeq", RECO_jmpeq);
  mnem_check(cmd, "jmpne", RECO_jmpne);
  mnem_check(cmd, "jmpcs", RECO_jmpcs);
  mnem_check(cmd, "jmpcc", RECO_jmpcc);
  mnem_check(cmd, "jmpn",  RECO_jmpn);
  mnem_check(cmd, "jmpp",  RECO_jmpp);
  mnem_check(cmd, "jmpvs", RECO_jmpvs);
  mnem_check(cmd, "jmpvc", RECO_jmpvc);
  mnem_check(cmd, "jsr",   RECO_jsr);
  mnem_check(cmd, "jsreq", RECO_jsreq);
  mnem_check(cmd, "jsrne", RECO_jsrne);
  mnem_check(cmd, "jsrcs", RECO_jsrcs);
  mnem_check(cmd, "jsrcc", RECO_jsrcc);
  mnem_check(cmd, "jsrn",  RECO_jsrn);
  mnem_check(cmd, "jsrp",  RECO_jsrp);
  mnem_check(cmd, "jsrvs", RECO_jsrvs);
  mnem_check(cmd, "jsrvc", RECO_jsrvc);

  MNEM_CHK_LEN(beq);
  MNEM_CHK_LEN(bne);
  MNEM_CHK_LEN(bcs);
  MNEM_CHK_LEN(bcc);
  MNEM_CHK_LEN(bn);
  MNEM_CHK_LEN(bp);
  MNEM_CHK_LEN(bvs);
  MNEM_CHK_LEN(bvc);
  MNEM_CHK_LEN(bra);
  MNEM_CHK_LEN(bsr);

  switch ( itype )
  {
    default:
    case RECO_null:
      return PROC_ASM_ERR_BAD_MNEM;

      // insn
    case RECO_halt: code = 0xC200; goto CODE_FMT_NONE;
    case RECO_nop:  code = 0xC201; goto CODE_FMT_NONE;
    case RECO_rts:  code = 0xC202; goto CODE_FMT_NONE;
    case RECO_rte:  code = 0xC203; goto CODE_FMT_NONE;

      // insn #data12
    case RECO_dline: code = 0xD000; goto CODE_FMT_IMM12;
    case RECO_eline: code = 0xE000; goto CODE_FMT_IMM12;
    case RECO_fline: code = 0xF000; goto CODE_FMT_IMM12;

      // insn Rn,<ea>
      // insn <ea>,Rn
    case RECO_add_w:  code = 0x0000; goto CODE_FMT_RN_EA_DIR;
    case RECO_add_b:  code = 0x8000; goto CODE_FMT_RN_EA_DIR;
    case RECO_addc_w: code = 0x0400; goto CODE_FMT_RN_EA_DIR;
    case RECO_addc_b: code = 0x8400; goto CODE_FMT_RN_EA_DIR;
    case RECO_sub_w:  code = 0x0800; goto CODE_FMT_RN_EA_DIR;
    case RECO_sub_b:  code = 0x8800; goto CODE_FMT_RN_EA_DIR;
    case RECO_subc_w: code = 0x0C00; goto CODE_FMT_RN_EA_DIR;
    case RECO_subc_b: code = 0x8C00; goto CODE_FMT_RN_EA_DIR;
    case RECO_cmp_w:  code = 0x1800; goto CODE_FMT_RN_EA_DIR;
    case RECO_cmp_b:  code = 0x9800; goto CODE_FMT_RN_EA_DIR;
    case RECO_move_w: code = 0x1C00; goto CODE_FMT_RN_EA_DIR;
    case RECO_move_b: code = 0x9C00; goto CODE_FMT_RN_EA_DIR;
    case RECO_and_w:  code = 0x2000; goto CODE_FMT_RN_EA_DIR;
    case RECO_and_b:  code = 0xA000; goto CODE_FMT_RN_EA_DIR;
    case RECO_or_w:   code = 0x2400; goto CODE_FMT_RN_EA_DIR;
    case RECO_or_b:   code = 0xA400; goto CODE_FMT_RN_EA_DIR;
    case RECO_xor_w:  code = 0x2800; goto CODE_FMT_RN_EA_DIR;
    case RECO_xor_b:  code = 0xA800; goto CODE_FMT_RN_EA_DIR;

      // insn <ea>
    case RECO_not_w: code = 0x1000; goto CODE_FMT_EA;
    case RECO_not_b: code = 0x9000; goto CODE_FMT_EA;
    case RECO_neg_w: code = 0x1040; goto CODE_FMT_EA;
    case RECO_neg_b: code = 0x9040; goto CODE_FMT_EA;
    case RECO_tst_w: code = 0x1400; goto CODE_FMT_EA;
    case RECO_tst_b: code = 0x9400; goto CODE_FMT_EA;

    case RECO_jmpeq: code = 0x6000; goto CODE_FMT_EA;
    case RECO_jmpne: code = 0x6400; goto CODE_FMT_EA;
    case RECO_jmpcs: code = 0x6800; goto CODE_FMT_EA;
    case RECO_jmpcc: code = 0x6C00; goto CODE_FMT_EA;
    case RECO_jmpn:  code = 0x7000; goto CODE_FMT_EA;
    case RECO_jmpp:  code = 0x7400; goto CODE_FMT_EA;
    case RECO_jmpvs: code = 0x7800; goto CODE_FMT_EA;
    case RECO_jmpvc: code = 0x7C00; goto CODE_FMT_EA;
    case RECO_jmp:   code = 0x6200; goto CODE_FMT_EA;
    case RECO_jsreq: code = 0x6100; goto CODE_FMT_EA;
    case RECO_jsrne: code = 0x6500; goto CODE_FMT_EA;
    case RECO_jsrcs: code = 0x6900; goto CODE_FMT_EA;
    case RECO_jsrcc: code = 0x6D00; goto CODE_FMT_EA;
    case RECO_jsrn:  code = 0x7100; goto CODE_FMT_EA;
    case RECO_jsrp:  code = 0x7500; goto CODE_FMT_EA;
    case RECO_jsrvs: code = 0x7900; goto CODE_FMT_EA;
    case RECO_jsrvc: code = 0x7D00; goto CODE_FMT_EA;
    case RECO_jsr:   code = 0x6300; goto CODE_FMT_EA;

      // insn Rn,<ea>
      // insn #imm4,<ea>
    case RECO_btst_b: code = 0x3000; goto CODE_FMT_IMM4oRN_EA;
    case RECO_btst_w: code = 0xB000; goto CODE_FMT_IMM4oRN_EA;
    case RECO_bset_b: code = 0x3400; goto CODE_FMT_IMM4oRN_EA;
    case RECO_bset_w: code = 0xB400; goto CODE_FMT_IMM4oRN_EA;
    case RECO_bclr_b: code = 0x3800; goto CODE_FMT_IMM4oRN_EA;
    case RECO_bclr_w: code = 0xB800; goto CODE_FMT_IMM4oRN_EA;

      // insn Rn,Rc
      // insn #imm3,Rc
    case RECO_lsl_b:  code = 0x3C00; goto CODE_FMT_IMM3oRN_RN;
    case RECO_lsl_w:  code = 0xBC00; goto CODE_FMT_IMM3oRN_RN;
    case RECO_lsr_b:  code = 0x3C10; goto CODE_FMT_IMM3oRN_RN;
    case RECO_lsr_w:  code = 0xBC10; goto CODE_FMT_IMM3oRN_RN;
    case RECO_asr_b:  code = 0x3C18; goto CODE_FMT_IMM3oRN_RN;
    case RECO_asr_w:  code = 0xBC18; goto CODE_FMT_IMM3oRN_RN;
    case RECO_rol_b:  code = 0x3C20; goto CODE_FMT_IMM3oRN_RN;
    case RECO_rol_w:  code = 0xBC20; goto CODE_FMT_IMM3oRN_RN;
    case RECO_rolc_b: code = 0x3C28; goto CODE_FMT_IMM3oRN_RN;
    case RECO_rolc_w: code = 0xBC28; goto CODE_FMT_IMM3oRN_RN;
    case RECO_ror_b:  code = 0x3C30; goto CODE_FMT_IMM3oRN_RN;
    case RECO_ror_w:  code = 0xBC30; goto CODE_FMT_IMM3oRN_RN;
    case RECO_rorc_b: code = 0x3C38; goto CODE_FMT_IMM3oRN_RN;
    case RECO_rorc_w: code = 0xBC38; goto CODE_FMT_IMM3oRN_RN;

      // insn disp8
    case RECO_beq: code = 0x4000; goto CODE_FMT_DISPX;
    case RECO_bne: code = 0x4400; goto CODE_FMT_DISPX;
    case RECO_bcs: code = 0x4800; goto CODE_FMT_DISPX;
    case RECO_bcc: code = 0x4C00; goto CODE_FMT_DISPX;
    case RECO_bn:  code = 0x5000; goto CODE_FMT_DISPX;
    case RECO_bp:  code = 0x5400; goto CODE_FMT_DISPX;
    case RECO_bvs: code = 0x5800; goto CODE_FMT_DISPX;
    case RECO_bvc: code = 0x5C00; goto CODE_FMT_DISPX;
    case RECO_bra: code = 0x4200; goto CODE_FMT_DISPX;
    case RECO_bsr: code = 0x4600; goto CODE_FMT_DISPX;

    case RECO_beq_s: code = 0x4000; goto CODE_FMT_DISP8;
    case RECO_bne_s: code = 0x4400; goto CODE_FMT_DISP8;
    case RECO_bcs_s: code = 0x4800; goto CODE_FMT_DISP8;
    case RECO_bcc_s: code = 0x4C00; goto CODE_FMT_DISP8;
    case RECO_bn_s:  code = 0x5000; goto CODE_FMT_DISP8;
    case RECO_bp_s:  code = 0x5400; goto CODE_FMT_DISP8;
    case RECO_bvs_s: code = 0x5800; goto CODE_FMT_DISP8;
    case RECO_bvc_s: code = 0x5C00; goto CODE_FMT_DISP8;
    case RECO_bra_s: code = 0x4200; goto CODE_FMT_DISP8;
    case RECO_bsr_s: code = 0x4600; goto CODE_FMT_DISP8;

    case RECO_beq_l: code = 0x4100; goto CODE_FMT_DISP16;
    case RECO_bne_l: code = 0x4500; goto CODE_FMT_DISP16;
    case RECO_bcs_l: code = 0x4900; goto CODE_FMT_DISP16;
    case RECO_bcc_l: code = 0x4D00; goto CODE_FMT_DISP16;
    case RECO_bn_l:  code = 0x5100; goto CODE_FMT_DISP16;
    case RECO_bp_l:  code = 0x5500; goto CODE_FMT_DISP16;
    case RECO_bvs_l: code = 0x5900; goto CODE_FMT_DISP16;
    case RECO_bvc_l: code = 0x5D00; goto CODE_FMT_DISP16;
    case RECO_bra_l: code = 0x4300; goto CODE_FMT_DISP16;
    case RECO_bsr_l: code = 0x4700; goto CODE_FMT_DISP16;

      // (other)
    case RECO_break: {
      code = 0xC000;
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[1].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP1_TYPE;
      if ( cmd.op[0].value >= 8 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      if ( cmd.op[1].value >= 0x40 ) return PROC_ASM_ERR_BAD_OP1_VAL;
      code |= (cmd.op[1].value << 0);
      code |= (cmd.op[0].value << 6);
      break;
    }
    case RECO_trap: {
      code = 0xC1D0;
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value >= 16 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      code |= (cmd.op[0].value << 0);
      break;
    }
    case RECO_mctl: {
      code = 0xC400;
      int dir = 0;
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_REG && cmd.op[1].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP_TYPE;
      if ( cmd.op[1].type == OPT_REG && cmd.op[1].reg >= 16 ) dir = 1;
      if ( dir == 0 ) { op0 = &cmd.op[0]; ea_opnum = 1; }
      else            { op0 = &cmd.op[1]; ea_opnum = 0; }
      ea = &cmd.op[ea_opnum];
      if ( op0->reg < 16 ) return PROC_ASM_ERR_BAD_OP1_VAL - ea_opnum;
      if ( ea->reg >= 8 ) return PROC_ASM_ERR_BAD_OP0_VAL + ea_opnum;
      if ( dir == 0 && !ea->is_writable() ) return PROC_ASM_ERR_BAD_OP0_TYPE + ea_opnum;
      code |= (dir << 9);
      op0->reg &= 15;
      code |= (op0->reg << 6);
      goto CODE_EA;
    }
    case RECO_xchg: {
      code = 0xC800;
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[1].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP1_TYPE;
      if ( cmd.op[0].reg >= 16 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      if ( cmd.op[1].reg >= 16 ) return PROC_ASM_ERR_BAD_OP1_VAL;
      code |= (cmd.op[1].reg << 5);
      code |= (cmd.op[0].reg << 0);
      break;
    }

CODE_FMT_NONE: {
      if ( cmd.opcnt > 0 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      break;
    }
CODE_FMT_DISP8: {
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;

      int32_t disp = cmd.op[0].value - cmd.ea;
      if ( disp < -0x80 || disp > 0x7F ) return PROC_ASM_ERR_OUT_OF_RANGE;
      code |= (disp & 0xFF);
      break;
    }
CODE_FMT_DISP16: {
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;

      int32_t disp = cmd.op[0].value - cmd.ea;
      if ( disp < -0x8000 || disp > 0x7FFF ) return PROC_ASM_ERR_OUT_OF_RANGE;
      code |= (disp & 0xFF);
      cmd.code[cmd.len++] = (disp >> 8) & 0xFF;
      break;
    }
CODE_FMT_DISPX: {
      int32_t disp = cmd.op[0].value - cmd.ea;
      if ( disp < -0x80 || disp > 0x7F )
        goto CODE_FMT_DISP16;
      else
        goto CODE_FMT_DISP8;
    }
CODE_FMT_IMM12: {
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value >= 0x1000 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      code |= cmd.op[0].value & 0xFFF;
      break;
    }
CODE_FMT_RN_EA_DIR: {
      int dir = 0;
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_REG && cmd.op[1].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP_TYPE;
      if ( cmd.op[0].reg >= 8 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      if ( cmd.op[1].reg >= 8 ) return PROC_ASM_ERR_BAD_OP1_VAL;
      if ( cmd.op[1].type == OPT_REG ) dir = 1;
      if ( dir == 0 ) { op0 = &cmd.op[0]; ea_opnum = 1; }
      else            { op0 = &cmd.op[1]; ea_opnum = 0; }
      ea = &cmd.op[ea_opnum];
      if ( dir == 0 && !ea->is_writable() ) return PROC_ASM_ERR_BAD_OP0_TYPE + ea_opnum;
      code |= (dir << 9);
      code |= (op0->reg << 6);
      goto CODE_EA;
    }
CODE_FMT_EA: {
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].reg >= 8 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      ea_opnum = 0;
      ea = &cmd.op[ea_opnum];
      if ( !ea->is_writable() ) return PROC_ASM_ERR_BAD_OP0_TYPE + ea_opnum;
      goto CODE_EA;
    }
CODE_FMT_IMM4oRN_EA: {
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_REG && cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].reg >= 8 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      if ( cmd.op[0].value >= 16 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      if ( cmd.op[1].reg >= 8 ) return PROC_ASM_ERR_BAD_OP1_VAL;
      ea_opnum = 1;
      ea = &cmd.op[ea_opnum];
      if ( !ea->is_writable() ) return PROC_ASM_ERR_BAD_OP0_TYPE + ea_opnum;
      if ( cmd.op[0].type == OPT_NUM )
      {
        code |= 1 << 9;
        cmd.code[cmd.len++] = cmd.op[0].value;
      }
      else
        code |= cmd.op[0].reg << 6;
      goto CODE_EA;
    }
CODE_FMT_IMM3oRN_RN: {
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_REG && cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[1].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP1_TYPE;
      if ( cmd.op[0].reg >= 8 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      if ( cmd.op[0].value >= 8 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      if ( cmd.op[1].reg >= 8 ) return PROC_ASM_ERR_BAD_OP1_VAL;
      if ( cmd.op[0].type == OPT_NUM )
      {
        code |= 1 << 9;
        code |= cmd.op[0].value << 6;
      }
      else
        code |= cmd.op[0].reg << 6;
      code |= cmd.op[1].reg << 0;
      break;
    }
CODE_EA: {
      switch(ea->type) {
        default:
        case OPT_UNK:
          return PROC_ASM_ERR_BAD_OP0_TYPE + ea_opnum;
        case OPT_REG:
          code |= 0 << 3;
          code |= (ea->reg << 0);
          break;
        case OPT_POSTINC:
          code |= 2 << 3;
          code |= (ea->reg << 0);
          break;
        case OPT_PREDEC:
          code |= 3 << 3;
          code |= (ea->reg << 0);
          break;
        case OPT_DISP:
          if ( ea->disp == 0 )
          {
            code |= 1 << 3;
            code |= (ea->reg << 0);
          }
          else
          {
            code |= 4 << 3;
            code |= (ea->reg << 0);
            if ( ea->disp > 255 )
            {
              return PROC_ASM_ERR_BAD_OP0_VAL + ea_opnum;
            }
            cmd.code[cmd.len++] = ea->disp;
          }
          break;
        case OPT_PHRASE:
          code |= 5 << 3;
          code |= (ea->reg << 0);
          if ( ea->disp > 15 )
          {
            return PROC_ASM_ERR_BAD_OP0_VAL + ea_opnum;
          }
          cmd.code[cmd.len++] = (ea->disp << 4) | ea->reg2;
          break;
        case OPT_NUM:
          if ( ea->value > 65535 )
          {
            cmd.code[cmd.len++] = ea->value >> 16;
            cmd.code[cmd.len++] = ea->value >> 8;
            cmd.code[cmd.len++] = ea->value >> 0;
            code |= 6 << 3;
            code |= 2 << 0;
          }
          else if ( ea->value > 255 )
          {
            cmd.code[cmd.len++] = ea->value >> 8;
            cmd.code[cmd.len++] = ea->value >> 0;
            code |= 6 << 3;
            code |= 1 << 0;
          }
          else if ( ea->value > 7 )
          {
            cmd.code[cmd.len++] = ea->value >> 0;
            code |= 6 << 3;
            code |= 0 << 0;
          }
          else
          {
            code |= 7 << 3;
            code |= ea->value & 7;
          }
          break;
        case OPT_ADDR:
          if ( ea->value > 65535 )
          {
            cmd.code[cmd.len++] = ea->value >> 16;
            cmd.code[cmd.len++] = ea->value >> 8;
            cmd.code[cmd.len++] = ea->value >> 0;
            code |= 6 << 3;
            code |= 6 << 0;
          }
          else if ( ea->value > 255 )
          {
            cmd.code[cmd.len++] = ea->value >> 8;
            cmd.code[cmd.len++] = ea->value >> 0;
            code |= 6 << 3;
            code |= 5 << 0;
          }
          else
          {
            cmd.code[cmd.len++] = ea->value >> 0;
            code |= 6 << 3;
            code |= 4 << 0;
          }
          break;
        case OPT_STR:
          return PROC_ASM_ERR_BAD_OP0_TYPE + ea_opnum;
      }
      break;
    }
  }
  cmd.code[0] = (code >> 8) & 0xFF;
  cmd.code[1] = (code >> 0) & 0xFF;

  return PROC_ASM_ERR_OK;
}

proc_init_def(reco)
{
  return new reco_proc_t();
}
