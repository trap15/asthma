/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "parser.hpp"
#include "proc.hpp"

struct v810_proc_t : proc_t
{
  v810_proc_t();
  virtual ~v810_proc_t();
//--- Variables
//--- Functions
  virtual bool assume_reg(char *reg, uintmax_t value);
  virtual int process_insn(insn_t &cmd);
};

v810_proc_t::v810_proc_t()
{
  // Basic info
  sprintf(name, "V810");
  sprintf(cmts, "#;");
  case_sense = false;
  deref_style = PROC_DEREF_NUM_BRACKS_REG;
  word_size = 4;
  addr_style = PROC_ADDR_NONE;
  endian = PROC_ENDIAN_LE;
  bot_addr = 0x00000000;
  top_addr = 0xFFFFFFFF;
  num_pfx = '\0';
  allowed_endians = (1 << PROC_ENDIAN_LE);

  // Push some pre-defined symbols
  // SRs
  asthma::syms.push_back(symbol_t("eipc",  0));
  asthma::syms.push_back(symbol_t("eipsw", 1));
  asthma::syms.push_back(symbol_t("fepc",  2));
  asthma::syms.push_back(symbol_t("fepsw", 3));
  asthma::syms.push_back(symbol_t("ecr",   4));
  asthma::syms.push_back(symbol_t("psw",   5));
  asthma::syms.push_back(symbol_t("pir",   6));
  asthma::syms.push_back(symbol_t("tkcw",  7));
  asthma::syms.push_back(symbol_t("chcw",  24));
  asthma::syms.push_back(symbol_t("adtre", 25));
  // Condition codes
  asthma::syms.push_back(symbol_t("v",  0));
  asthma::syms.push_back(symbol_t("c",  1));
  asthma::syms.push_back(symbol_t("l",  1));
  asthma::syms.push_back(symbol_t("z",  2));
  asthma::syms.push_back(symbol_t("nh", 3));
  asthma::syms.push_back(symbol_t("s",  4));
  asthma::syms.push_back(symbol_t("n",  4));
  asthma::syms.push_back(symbol_t("t",  5));
  asthma::syms.push_back(symbol_t("lt", 6));
  asthma::syms.push_back(symbol_t("le", 7));
  asthma::syms.push_back(symbol_t("nv", 8));
  asthma::syms.push_back(symbol_t("nc", 9));
  asthma::syms.push_back(symbol_t("nl", 9));
  asthma::syms.push_back(symbol_t("nz", 10));
  asthma::syms.push_back(symbol_t("h",  11));
  asthma::syms.push_back(symbol_t("ns", 12));
  asthma::syms.push_back(symbol_t("p",  12));
  asthma::syms.push_back(symbol_t("f",  13));
  asthma::syms.push_back(symbol_t("ge", 14));
  asthma::syms.push_back(symbol_t("gt", 15));

  // Register names
  regs.push_back(reg_t("r0",   0));
  regs.push_back(reg_t("r1",   1));
  regs.push_back(reg_t("r2",   2));
  regs.push_back(reg_t("hp",   2));
  regs.push_back(reg_t("r3",   3));
  regs.push_back(reg_t("sp",   3));
  regs.push_back(reg_t("r4",   4));
  regs.push_back(reg_t("gp",   4));
  regs.push_back(reg_t("r5",   5));
  regs.push_back(reg_t("tp",   5));
  regs.push_back(reg_t("r6",   6));
  regs.push_back(reg_t("r7",   7));
  regs.push_back(reg_t("r8",   8));
  regs.push_back(reg_t("r9",   9));
  regs.push_back(reg_t("r10", 10));
  regs.push_back(reg_t("r11", 11));
  regs.push_back(reg_t("r12", 12));
  regs.push_back(reg_t("r13", 13));
  regs.push_back(reg_t("r14", 14));
  regs.push_back(reg_t("r15", 15));
  regs.push_back(reg_t("r16", 16));
  regs.push_back(reg_t("r17", 17));
  regs.push_back(reg_t("r18", 18));
  regs.push_back(reg_t("r19", 19));
  regs.push_back(reg_t("r20", 20));
  regs.push_back(reg_t("r21", 21));
  regs.push_back(reg_t("r22", 22));
  regs.push_back(reg_t("r23", 23));
  regs.push_back(reg_t("r24", 24));
  regs.push_back(reg_t("r25", 25));
  regs.push_back(reg_t("r26", 26));
  regs.push_back(reg_t("r27", 27));
  regs.push_back(reg_t("r28", 28));
  regs.push_back(reg_t("r29", 29));
  regs.push_back(reg_t("r30", 30));
  regs.push_back(reg_t("r31", 31));
  regs.push_back(reg_t("lp",  31));
}

v810_proc_t::~v810_proc_t()
{
}

bool v810_proc_t::assume_reg(char *reg, uintmax_t value)
{
  return false;
}

enum v810_itypes
{
  V810_null = 0,

  // insn
  V810_halt,
  V810_reti,
  V810_sch0bsu,
  V810_sch0bsd,
  V810_sch1bsu,
  V810_sch1bsd,
  V810_orbsu,
  V810_andbsu,
  V810_xorbsu,
  V810_movbsu,
  V810_ornbsu,
  V810_andnbsu,
  V810_xornbsu,
  V810_notbsu,

  // insn reg1, reg2
  // insn imm5, reg2
  V810_mov,
  V810_sar,
  V810_shr,
  V810_shl,
  V810_cmp,
  V810_add,

  // insn reg1, reg2
  V810_sub,
  V810_mul,
  V810_div,
  V810_mulu,
  V810_divu,
  V810_or,
  V810_and,
  V810_xor,
  V810_not,

  // insn imm5, reg2
  V810_setf,
  V810_stsr,

  // insn imm16, reg1, reg2
  V810_movea,
  V810_addi,
  V810_ori,
  V810_andi,
  V810_xori,
  V810_movhi,

  // insn disp16[reg1], reg2
  V810_ld_b,
  V810_ld_h,
  V810_ld_w,
  V810_in_b,
  V810_in_h,
  V810_caxi,
  V810_in_w,

  // insn reg2, disp16[reg1]
  V810_st_b,
  V810_st_h,
  V810_st_w,
  V810_out_b,
  V810_out_h,
  V810_out_w,

  // insn disp26
  V810_jr,
  V810_jal,

  // insn [reg1]
  V810_jmp,

  // insn vector
  V810_trap,

  // insncc disp9
  V810_bv,
  V810_bc,
  V810_bz, // be
  V810_bnh,
  V810_bn,
  V810_br,
  V810_blt,
  V810_ble,
  V810_bnv,
  V810_bnc,
  V810_bnz, // bne
  V810_bh,
  V810_bp,
  V810_bge,
  V810_bgt,

  // ldsr reg2, regID
  V810_ldsr,

  // psuedo-ops
  V810_nop,

  V810_last
};

int v810_proc_t::process_insn(insn_t &cmd)
{
  uint16_t code = 0x0000;
  int itype = V810_null;
  cmd.len = 2;

  // pseudo-ops
  mnem_check(cmd, "nop", V810_nop);

  // insn
  mnem_check(cmd, "halt", V810_halt);
  mnem_check(cmd, "reti", V810_reti);
  mnem_check(cmd, "sch0bsu", V810_sch0bsu);
  mnem_check(cmd, "sch0bsd", V810_sch0bsd);
  mnem_check(cmd, "sch1bsu", V810_sch1bsu);
  mnem_check(cmd, "sch1bsd", V810_sch1bsd);
  mnem_check(cmd, "orbsu", V810_orbsu);
  mnem_check(cmd, "andbsu", V810_andbsu);
  mnem_check(cmd, "xorbsu", V810_xorbsu);
  mnem_check(cmd, "movbsu", V810_movbsu);
  mnem_check(cmd, "ornbsu", V810_ornbsu);
  mnem_check(cmd, "andnbsu", V810_andnbsu);
  mnem_check(cmd, "xornbsu", V810_xornbsu);
  mnem_check(cmd, "notbsu", V810_notbsu);

  // insn reg1, reg2
  // insn imm5, reg2
  mnem_check(cmd, "mov", V810_mov);
  mnem_check(cmd, "sar", V810_sar);
  mnem_check(cmd, "shr", V810_shr);
  mnem_check(cmd, "shl", V810_shl);
  mnem_check(cmd, "cmp", V810_cmp);
  mnem_check(cmd, "add", V810_add);

  // insn reg1, reg2
  mnem_check(cmd, "sub", V810_sub);
  mnem_check(cmd, "mul", V810_mul);
  mnem_check(cmd, "div", V810_div);
  mnem_check(cmd, "mulu", V810_mulu);
  mnem_check(cmd, "divu", V810_divu);
  mnem_check(cmd, "or", V810_or);
  mnem_check(cmd, "and", V810_and);
  mnem_check(cmd, "xor", V810_xor);
  mnem_check(cmd, "not", V810_not);

  // insn imm5, reg2
  mnem_check(cmd, "setf", V810_setf);
  mnem_check(cmd, "stsr", V810_stsr);

  // insn imm16, reg1, reg2
  mnem_check(cmd, "movea", V810_movea);
  mnem_check(cmd, "addi", V810_addi);
  mnem_check(cmd, "ori", V810_ori);
  mnem_check(cmd, "andi", V810_andi);
  mnem_check(cmd, "xori", V810_xori);
  mnem_check(cmd, "movhi", V810_movhi);

  // insn disp16[reg1], reg2
  mnem_check(cmd, "ld.b", V810_ld_b);
  mnem_check(cmd, "ld.h", V810_ld_h);
  mnem_check(cmd, "ld.w", V810_ld_w);
  mnem_check(cmd, "in.b", V810_in_b);
  mnem_check(cmd, "in.h", V810_in_h);
  mnem_check(cmd, "caxi", V810_caxi);
  mnem_check(cmd, "in.w", V810_in_w);

  // insn reg2, disp16[reg1]
  mnem_check(cmd, "st.b", V810_st_b);
  mnem_check(cmd, "st.h", V810_st_h);
  mnem_check(cmd, "st.w", V810_st_w);
  mnem_check(cmd, "out.b", V810_out_b);
  mnem_check(cmd, "out.h", V810_out_h);
  mnem_check(cmd, "out.w", V810_out_w);

  // insn disp26
  mnem_check(cmd, "jr", V810_jr);
  mnem_check(cmd, "jal", V810_jal);

  // insn [reg1]
  mnem_check(cmd, "jmp", V810_jmp);

  // insn vector
  mnem_check(cmd, "trap", V810_trap);

  // insncc disp9
  mnem_check(cmd, "bv", V810_bv);
  mnem_check(cmd, "bc", V810_bc);
  mnem_check(cmd, "bz", V810_bz);
  mnem_check(cmd, "be", V810_bz);
  mnem_check(cmd, "bnh", V810_bnh);
  mnem_check(cmd, "bn", V810_bn);
  mnem_check(cmd, "br", V810_br);
  mnem_check(cmd, "blt", V810_blt);
  mnem_check(cmd, "ble", V810_ble);
  mnem_check(cmd, "bnv", V810_bnv);
  mnem_check(cmd, "bnc", V810_bnc);
  mnem_check(cmd, "bnz", V810_bnz);
  mnem_check(cmd, "bne", V810_bnz);
  mnem_check(cmd, "bh", V810_bh);
  mnem_check(cmd, "bp", V810_bp);
  mnem_check(cmd, "bge", V810_bge);
  mnem_check(cmd, "bgt", V810_bgt);

  // ldsr reg2, regID
  mnem_check(cmd, "ldsr", V810_ldsr);

  switch ( itype )
  {
    default:
    case V810_null:
      return PROC_ASM_ERR_BAD_MNEM;

      // psuedo-ops
    case V810_nop:
      if ( cmd.opcnt > 0 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      code = 0x0000;
      break;

      // insn
    case V810_reti:    code = 0x6400; break;
    case V810_halt:    code = 0x6800; break;
    case V810_sch0bsu: code = 0x7C00; break;
    case V810_sch0bsd: code = 0x7C01; break;
    case V810_sch1bsu: code = 0x7C02; break;
    case V810_sch1bsd: code = 0x7C03; break;
    case V810_orbsu:   code = 0x7C08; break;
    case V810_andbsu:  code = 0x7C09; break;
    case V810_xorbsu:  code = 0x7C0A; break;
    case V810_movbsu:  code = 0x7C0B; break;
    case V810_ornbsu:  code = 0x7C0C; break;
    case V810_andnbsu: code = 0x7C0D; break;
    case V810_xornbsu: code = 0x7C0E; break;
    case V810_notbsu:  code = 0x7C0F; break;

      // insn reg1, reg2
      // insn imm5, reg2
    case V810_sar: code = 0x1C00; goto CODE_FMT_I_II;
    case V810_shr: code = 0x1400; goto CODE_FMT_I_II;
    case V810_shl: code = 0x1000; goto CODE_FMT_I_II;
    case V810_cmp: code = 0x0C00; goto CODE_FMT_I_II;
    case V810_add: code = 0x0400; goto CODE_FMT_I_II;
    case V810_mov: code = 0x0000;
CODE_FMT_I_II:
      if ( cmd.op[0].type == OPT_REG )
        goto CODE_FMT_I;
      else if ( cmd.op[0].type == OPT_NUM )
      {
        code |= 0x4000;
        goto CODE_FMT_II;
      }
      else
        return PROC_ASM_ERR_BAD_OP0_TYPE;

      // insn reg1, reg2
    case V810_sub:  code = 0x0800; goto CODE_FMT_I;
    case V810_mul:  code = 0x2000; goto CODE_FMT_I;
    case V810_div:  code = 0x2400; goto CODE_FMT_I;
    case V810_mulu: code = 0x2800; goto CODE_FMT_I;
    case V810_divu: code = 0x2C00; goto CODE_FMT_I;
    case V810_or:   code = 0x3000; goto CODE_FMT_I;
    case V810_and:  code = 0x3400; goto CODE_FMT_I;
    case V810_xor:  code = 0x3800; goto CODE_FMT_I;
    case V810_not:  code = 0x3C00; goto CODE_FMT_I;

      // insn imm5, reg2
    case V810_setf: code = 0x4800; goto CODE_FMT_II;
    case V810_stsr: code = 0x7400; goto CODE_FMT_II;

      // insn imm16, reg1, reg2
    case V810_movea: code = 0xA000; goto CODE_FMT_V;
    case V810_addi:  code = 0xA400; goto CODE_FMT_V;
    case V810_ori:   code = 0xB000; goto CODE_FMT_V;
    case V810_andi:  code = 0xB400; goto CODE_FMT_V;
    case V810_xori:  code = 0xB800; goto CODE_FMT_V;
    case V810_movhi: code = 0xBC00; goto CODE_FMT_V;

      // insn disp16[reg1], reg2
    case V810_ld_b: code = 0xC000; goto CODE_FMT_VI_a;
    case V810_ld_h: code = 0xC400; goto CODE_FMT_VI_a;
    case V810_ld_w: code = 0xCC00; goto CODE_FMT_VI_a;
    case V810_in_b: code = 0xE000; goto CODE_FMT_VI_a;
    case V810_in_h: code = 0xE400; goto CODE_FMT_VI_a;
    case V810_caxi: code = 0xE800; goto CODE_FMT_VI_a;
    case V810_in_w: code = 0xEC00; goto CODE_FMT_VI_a;

      // insn reg2, disp16[reg1]
    case V810_st_b:  code = 0xD000; goto CODE_FMT_VI_b;
    case V810_st_h:  code = 0xD400; goto CODE_FMT_VI_b;
    case V810_st_w:  code = 0xDC00; goto CODE_FMT_VI_b;
    case V810_out_b: code = 0xF000; goto CODE_FMT_VI_b;
    case V810_out_h: code = 0xF400; goto CODE_FMT_VI_b;
    case V810_out_w: code = 0xFC00; goto CODE_FMT_VI_b;

      // insn disp26
    case V810_jr:  code = 0xA800; goto CODE_FMT_IV;
    case V810_jal: code = 0xAC00; goto CODE_FMT_IV;

      // insn [reg1]
    case V810_jmp:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_DISP ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].disp != 0 ) return PROC_ASM_ERR_BAD_OP0_VAL;

      code = 0x1800;
      code |= (cmd.op[0].reg << 0);
      break;

      // insn vector
    case V810_trap:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value < 0 || cmd.op[0].value > 0x1F ) return PROC_ASM_ERR_OUT_OF_RANGE;

      code = 0x6000;
      code |= ((cmd.op[0].value & 0x1F) << 0);
      break;

      // insncc disp9
    case V810_bv:  code = 0x8000; goto CODE_FMT_III;
    case V810_bc:  code = 0x8200; goto CODE_FMT_III;
    case V810_bz:  code = 0x8400; goto CODE_FMT_III;
    case V810_bnh: code = 0x8600; goto CODE_FMT_III;
    case V810_bn:  code = 0x8800; goto CODE_FMT_III;
    case V810_br:  code = 0x8A00; goto CODE_FMT_III;
    case V810_blt: code = 0x8C00; goto CODE_FMT_III;
    case V810_ble: code = 0x8E00; goto CODE_FMT_III;
    case V810_bnv: code = 0x9000; goto CODE_FMT_III;
    case V810_bnc: code = 0x9200; goto CODE_FMT_III;
    case V810_bnz: code = 0x9400; goto CODE_FMT_III;
    case V810_bh:  code = 0x9600; goto CODE_FMT_III;
    case V810_bp:  code = 0x9800; goto CODE_FMT_III;
    case V810_bge: code = 0x9C00; goto CODE_FMT_III;
    case V810_bgt: code = 0x9E00; goto CODE_FMT_III;

      // ldsr reg2, regID
    case V810_ldsr:
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_REG )  return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[1].type != OPT_NUM )  return PROC_ASM_ERR_BAD_OP1_TYPE;
      if ( cmd.op[1].value < 0 || cmd.op[0].value > 0x1F ) return PROC_ASM_ERR_OUT_OF_RANGE;

      code = 0x7000;
      code |= ((cmd.op[1].value & 0x1F) << 0);
      code |= (cmd.op[0].reg << 5);
      break;



CODE_FMT_I:  // insn reg1, reg2
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[1].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP1_TYPE;

      code |= (cmd.op[0].reg << 0);
      code |= (cmd.op[1].reg << 5);
      break;

CODE_FMT_II: // insn imm5, reg2
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[1].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP1_TYPE;
      if ( itype == V810_sar ||
           itype == V810_shr ||
           itype == V810_shl ||
           itype == V810_setf )
      { // unsigned param
        if ( cmd.op[0].value < 0 || cmd.op[0].value > 0x1F ) return PROC_ASM_ERR_OUT_OF_RANGE;
      }
      else // signed param
      {
        if ( cmd.op[0].value < -0x10 || cmd.op[0].value > 0xF ) return PROC_ASM_ERR_OUT_OF_RANGE;
      }

      code |= ((cmd.op[0].value & 0x1F) << 0);
      code |= (cmd.op[1].reg << 5);
      break;

CODE_FMT_III: // insncc disp9
    {
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value & 1 ) return PROC_ASM_ERR_BAD_OP0_VAL;

      int32_t disp = cmd.op[0].value - cmd.ea;
      if ( disp < -0x100 || disp > 0xFE ) return PROC_ASM_ERR_OUT_OF_RANGE;
      code |= (disp & 0x1FE);
      break;
    }

CODE_FMT_IV: // insn disp26
    {
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value & 1 ) return PROC_ASM_ERR_BAD_OP0_VAL;

      int32_t disp = cmd.op[0].value - cmd.ea;
      if ( disp < -0x2000000 || disp > 0x1FFFFFE )
        return PROC_ASM_ERR_OUT_OF_RANGE;
      disp &= 0x3FFFFFE;
      code |= disp >> 16;
      cmd.code[3] = (disp >> 8) & 0xFF;
      cmd.code[2] = (disp >> 0) & 0xFF;
      cmd.len = 4;
      break;
    }

CODE_FMT_V: // insn imm16, reg1, reg2
      if ( cmd.opcnt > 3 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 3 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[1].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP1_TYPE;
      if ( cmd.op[2].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP2_TYPE;

      code |= (cmd.op[1].reg << 0);
      code |= (cmd.op[2].reg << 5);
      cmd.code[3] = (cmd.op[0].value >> 8) & 0xFF;
      cmd.code[2] = (cmd.op[0].value >> 0) & 0xFF;
      cmd.len = 4;
      break;

CODE_FMT_VI_a: // insn disp16[reg1], reg2
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_DISP ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[1].type != OPT_REG )  return PROC_ASM_ERR_BAD_OP1_TYPE;

      code |= (cmd.op[0].reg << 0);
      code |= (cmd.op[1].reg << 5);
      cmd.code[3] = (cmd.op[0].disp >> 8) & 0xFF;
      cmd.code[2] = (cmd.op[0].disp >> 0) & 0xFF;
      cmd.len = 4;
      break;

CODE_FMT_VI_b: // insn reg2, disp16[reg1]
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_REG )  return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[1].type != OPT_DISP ) return PROC_ASM_ERR_BAD_OP1_TYPE;

      code |= (cmd.op[1].reg << 0);
      code |= (cmd.op[0].reg << 5);
      cmd.code[3] = (cmd.op[1].disp >> 8) & 0xFF;
      cmd.code[2] = (cmd.op[1].disp >> 0) & 0xFF;
      cmd.len = 4;
      break;
  }
  cmd.code[1] = (code >> 8) & 0xFF;
  cmd.code[0] = (code >> 0) & 0xFF;

  return PROC_ASM_ERR_OK;
}

proc_init_def(v810)
{
  return new v810_proc_t();
}
