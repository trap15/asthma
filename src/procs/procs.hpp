/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011        Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

proc_define(proc,   "NONE")
proc_define(v810,   "V810")
proc_define(i4004,  "I4004")
proc_define(ppc,    "PPC")
proc_define(reco,   "RECO")
proc_define(pgmmus, "PGMMUS")
proc_define(mb8840, "MB8840")
// More CPUs go here
