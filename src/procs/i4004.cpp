/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "parser.hpp"
#include "proc.hpp"

struct i4004_proc_t : proc_t
{
  i4004_proc_t();
  virtual ~i4004_proc_t();
//--- Variables
//--- Functions
  virtual bool assume_reg(char *reg, uintmax_t value);
  virtual int process_insn(insn_t &cmd);
};

i4004_proc_t::i4004_proc_t()
{
  // Basic info
  sprintf(name, "I4004");
  sprintf(cmts, ";");
  case_sense = false;
  deref_style = PROC_DEREF_NUM_BRACKS_REG;
  word_size = 1;
  addr_style = PROC_ADDR_NONE;
  endian = PROC_ENDIAN_LE;
  bot_addr = 0x000;
  top_addr = 0xFFF;
  num_pfx = '\0';
  allowed_endians = 0; // No endians.

  // Push some pre-defined symbols
  // Condition codes
  asthma::syms.push_back(symbol_t("tz",  1));
  asthma::syms.push_back(symbol_t("c1",  2));
  asthma::syms.push_back(symbol_t("az",  4));
  asthma::syms.push_back(symbol_t("tn",  9));
  asthma::syms.push_back(symbol_t("c0",  10));
  asthma::syms.push_back(symbol_t("an",  12));

  // Register names
  regs.push_back(reg_t("r0",   0));
  regs.push_back(reg_t("r1",   1));
  regs.push_back(reg_t("r2",   2));
  regs.push_back(reg_t("r3",   3));
  regs.push_back(reg_t("r4",   4));
  regs.push_back(reg_t("r5",   5));
  regs.push_back(reg_t("r6",   6));
  regs.push_back(reg_t("r7",   7));
  regs.push_back(reg_t("r8",   8));
  regs.push_back(reg_t("r9",   9));
  regs.push_back(reg_t("r10", 10));
  regs.push_back(reg_t("r11", 11));
  regs.push_back(reg_t("r12", 12));
  regs.push_back(reg_t("r13", 13));
  regs.push_back(reg_t("r14", 14));
  regs.push_back(reg_t("r15", 15));
  regs.push_back(reg_t("p0",  16));
  regs.push_back(reg_t("p1",  17));
  regs.push_back(reg_t("p2",  18));
  regs.push_back(reg_t("p3",  19));
  regs.push_back(reg_t("p4",  20));
  regs.push_back(reg_t("p5",  21));
  regs.push_back(reg_t("p6",  22));
  regs.push_back(reg_t("p7",  23));
}

i4004_proc_t::~i4004_proc_t()
{
}

bool i4004_proc_t::assume_reg(char *reg, uintmax_t value)
{
  return false;
}

enum i4004_itypes
{
  I4004_null = 0,

  // insn
  I4004_nop,
  I4004_wrm,
  I4004_wmp,
  I4004_wrr,
  I4004_wr0,
  I4004_wr1,
  I4004_wr2,
  I4004_wr3,
  I4004_sbm,
  I4004_rdm,
  I4004_rdr,
  I4004_adm,
  I4004_rd0,
  I4004_rd1,
  I4004_rd2,
  I4004_rd3,
  I4004_clb,
  I4004_clc,
  I4004_iac,
  I4004_cmc,
  I4004_cma,
  I4004_ral,
  I4004_rar,
  I4004_tcc,
  I4004_dac,
  I4004_tcs,
  I4004_stc,
  I4004_daa,
  I4004_kbp,
  I4004_dcl,

  // insn data
  I4004_bbl,
  I4004_ldm,

  // insn reg
  I4004_inc,
  I4004_add,
  I4004_sub,
  I4004_ld,
  I4004_xch,

  // insn reg, addr
  I4004_isz,

  // insn addr
  I4004_jun,
  I4004_jms,

  // insn regpair
  I4004_src,
  I4004_fin,
  I4004_jin,

  // insn regpair, data
  I4004_fim,

  // insn cc, addr
  I4004_jcn,

  I4004_last
};

int i4004_proc_t::process_insn(insn_t &cmd)
{
  int itype = I4004_null;
  cmd.len = 1;

  mnem_check(cmd, "nop", I4004_nop);
  mnem_check(cmd, "wrm", I4004_wrm);
  mnem_check(cmd, "wmp", I4004_wmp);
  mnem_check(cmd, "wrr", I4004_wrr);
  mnem_check(cmd, "wr0", I4004_wr0);
  mnem_check(cmd, "wr1", I4004_wr1);
  mnem_check(cmd, "wr2", I4004_wr2);
  mnem_check(cmd, "wr3", I4004_wr3);
  mnem_check(cmd, "sbm", I4004_sbm);
  mnem_check(cmd, "rdm", I4004_rdm);
  mnem_check(cmd, "rdr", I4004_rdr);
  mnem_check(cmd, "adm", I4004_adm);
  mnem_check(cmd, "rd0", I4004_rd0);
  mnem_check(cmd, "rd1", I4004_rd1);
  mnem_check(cmd, "rd2", I4004_rd2);
  mnem_check(cmd, "rd3", I4004_rd3);
  mnem_check(cmd, "clb", I4004_clb);
  mnem_check(cmd, "clc", I4004_clc);
  mnem_check(cmd, "iac", I4004_iac);
  mnem_check(cmd, "cmc", I4004_cmc);
  mnem_check(cmd, "cma", I4004_cma);
  mnem_check(cmd, "ral", I4004_ral);
  mnem_check(cmd, "rar", I4004_rar);
  mnem_check(cmd, "tcc", I4004_tcc);
  mnem_check(cmd, "dac", I4004_dac);
  mnem_check(cmd, "tcs", I4004_tcs);
  mnem_check(cmd, "stc", I4004_stc);
  mnem_check(cmd, "daa", I4004_daa);
  mnem_check(cmd, "kbp", I4004_kbp);
  mnem_check(cmd, "dcl", I4004_dcl);

  mnem_check(cmd, "bbl", I4004_bbl);
  mnem_check(cmd, "ldm", I4004_ldm);

  mnem_check(cmd, "inc", I4004_inc);
  mnem_check(cmd, "add", I4004_add);
  mnem_check(cmd, "sub", I4004_sub);
  mnem_check(cmd, "ld",  I4004_ld);
  mnem_check(cmd, "xch", I4004_xch);

  mnem_check(cmd, "isz", I4004_isz);

  mnem_check(cmd, "jun", I4004_jun);
  mnem_check(cmd, "jms", I4004_jms);

  mnem_check(cmd, "src", I4004_src);
  mnem_check(cmd, "fin", I4004_fin);
  mnem_check(cmd, "jin", I4004_jin);

  mnem_check(cmd, "fim", I4004_fim);

  mnem_check(cmd, "jcn", I4004_jcn);

  switch ( itype )
  {
    default:
    case I4004_null:
      return PROC_ASM_ERR_BAD_MNEM;

    case I4004_nop: cmd.code[0] = 0x00; goto I4004_NO_ARGS;
    case I4004_wrm: cmd.code[0] = 0xE0; goto I4004_NO_ARGS;
    case I4004_wmp: cmd.code[0] = 0xE1; goto I4004_NO_ARGS;
    case I4004_wrr: cmd.code[0] = 0xE2; goto I4004_NO_ARGS;
    case I4004_wr0: cmd.code[0] = 0xE4; goto I4004_NO_ARGS;
    case I4004_wr1: cmd.code[0] = 0xE5; goto I4004_NO_ARGS;
    case I4004_wr2: cmd.code[0] = 0xE6; goto I4004_NO_ARGS;
    case I4004_wr3: cmd.code[0] = 0xE7; goto I4004_NO_ARGS;
    case I4004_sbm: cmd.code[0] = 0xE8; goto I4004_NO_ARGS;
    case I4004_rdm: cmd.code[0] = 0xE9; goto I4004_NO_ARGS;
    case I4004_rdr: cmd.code[0] = 0xEA; goto I4004_NO_ARGS;
    case I4004_adm: cmd.code[0] = 0xEB; goto I4004_NO_ARGS;
    case I4004_rd0: cmd.code[0] = 0xEC; goto I4004_NO_ARGS;
    case I4004_rd1: cmd.code[0] = 0xED; goto I4004_NO_ARGS;
    case I4004_rd2: cmd.code[0] = 0xEE; goto I4004_NO_ARGS;
    case I4004_rd3: cmd.code[0] = 0xEF; goto I4004_NO_ARGS;
    case I4004_clb: cmd.code[0] = 0xF0; goto I4004_NO_ARGS;
    case I4004_clc: cmd.code[0] = 0xF1; goto I4004_NO_ARGS;
    case I4004_iac: cmd.code[0] = 0xF2; goto I4004_NO_ARGS;
    case I4004_cmc: cmd.code[0] = 0xF3; goto I4004_NO_ARGS;
    case I4004_cma: cmd.code[0] = 0xF4; goto I4004_NO_ARGS;
    case I4004_ral: cmd.code[0] = 0xF5; goto I4004_NO_ARGS;
    case I4004_rar: cmd.code[0] = 0xF6; goto I4004_NO_ARGS;
    case I4004_tcc: cmd.code[0] = 0xF7; goto I4004_NO_ARGS;
    case I4004_dac: cmd.code[0] = 0xF8; goto I4004_NO_ARGS;
    case I4004_tcs: cmd.code[0] = 0xF9; goto I4004_NO_ARGS;
    case I4004_stc: cmd.code[0] = 0xFA; goto I4004_NO_ARGS;
    case I4004_daa: cmd.code[0] = 0xFB; goto I4004_NO_ARGS;
    case I4004_kbp: cmd.code[0] = 0xFC; goto I4004_NO_ARGS;
    case I4004_dcl: cmd.code[0] = 0xFD; goto I4004_NO_ARGS;

    case I4004_bbl: cmd.code[0] = 0xC0; goto I4004_DATA_ARG;
    case I4004_ldm: cmd.code[0] = 0xD0; goto I4004_DATA_ARG;

    case I4004_inc: cmd.code[0] = 0x60; goto I4004_REG_ARG;
    case I4004_add: cmd.code[0] = 0x80; goto I4004_REG_ARG;
    case I4004_sub: cmd.code[0] = 0x90; goto I4004_REG_ARG;
    case I4004_ld:  cmd.code[0] = 0xA0; goto I4004_REG_ARG;
    case I4004_xch: cmd.code[0] = 0xB0; goto I4004_REG_ARG;

    case I4004_isz:
    {
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].reg > 15 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      cmd.code[0] = 0x70;
      cmd.code[0] |= cmd.op[0].reg;
I4004_PAGED_JMP:
      if ( cmd.op[1].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP1_TYPE;
      ea_t tgt_page;
      tgt_page = cmd.ea & ~0xFF;
      if ( (cmd.ea & 0xFE) == 0xFE ) // next page
      {
        tgt_page += 0x100;
      }
      if ( !((ea_t)cmd.op[1].value >= tgt_page && ((ea_t)cmd.op[1].value < tgt_page+0x100)) )
        return PROC_ASM_ERR_BAD_OP1_VAL;
      cmd.code[1] = cmd.op[1].value & 0xFF;
      cmd.len = 2;
      break;
    }

    case I4004_jun: cmd.code[0] = 0x40; goto I4004_JUMP;
    case I4004_jms: cmd.code[0] = 0x50; goto I4004_JUMP;

    case I4004_src: cmd.code[0] = 0x21; goto I4004_REGPAIR_ARG;
    case I4004_fin: cmd.code[0] = 0x30; goto I4004_REGPAIR_ARG;
    case I4004_jin: cmd.code[0] = 0x31; goto I4004_REGPAIR_ARG;

    case I4004_fim:
      if ( cmd.op[0].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].reg < 16 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      if ( cmd.op[1].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP1_TYPE;
      if ( cmd.op[1].value < -0x80 || cmd.op[1].value > 0xFF ) return PROC_ASM_ERR_BAD_OP1_VAL;
      cmd.code[0] = 0x20;
      cmd.code[0] |= (cmd.op[0].reg - 16) << 1;
      cmd.code[1] = cmd.op[1].value & 0xFF;
      cmd.len = 2;
      break;

    case I4004_jcn:
    {
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value > 15 || cmd.op[0].value < 0 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      cmd.code[0] = 0x10;
      cmd.code[0] |= cmd.op[0].value;
      goto I4004_PAGED_JMP;
    }

I4004_REGPAIR_ARG:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].reg < 16 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      cmd.code[0] |= (cmd.op[0].reg - 16) << 1;
      break;

I4004_JUMP:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( (ea_t)cmd.op[0].value > top_addr ) return PROC_ASM_ERR_BAD_OP0_VAL;
      cmd.code[0] |= ((ea_t)cmd.op[0].value >> 8) & 0xF;
      cmd.code[1]  = (ea_t)cmd.op[0].value & 0xFF;
      cmd.len = 2;
      break;

I4004_REG_ARG:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_REG ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].reg > 15 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      cmd.code[0] |= cmd.op[0].reg;
      break;

I4004_DATA_ARG:
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value > 15 || cmd.op[0].value < -8 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      cmd.code[0] |= cmd.op[0].value & 15;
      break;

I4004_NO_ARGS:
      if ( cmd.opcnt > 0 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      break;
  }

  return PROC_ASM_ERR_OK;
}

proc_init_def(i4004)
{
  return new i4004_proc_t();
}
