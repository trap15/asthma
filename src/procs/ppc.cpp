/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "parser.hpp"
#include "proc.hpp"

struct ppc_proc_t : proc_t
{
  ppc_proc_t();
  virtual ~ppc_proc_t();
//--- Variables
//--- Functions
  virtual bool assume_reg(char *reg, uintmax_t value);
  virtual int process_insn(insn_t &cmd);
};

ppc_proc_t::ppc_proc_t()
{
  // Basic info
  sprintf(name, "PPC");
  sprintf(cmts, ";");
  case_sense = false;
  deref_style = PROC_DEREF_NUM_PARENS_REG;
  word_size = 4;
  addr_style = PROC_ADDR_NONE;
  endian = PROC_ENDIAN_BE;
  bot_addr = 0x00000000;
  top_addr = 0xFFFFFFFF;
  num_pfx = '\0';
  allowed_endians = (1 << PROC_ENDIAN_BE) | (1 << PROC_ENDIAN_LE);

  // Push some pre-defined symbols
  // SPRs
  asthma::syms.push_back(symbol_t("xer",    1));
  asthma::syms.push_back(symbol_t("lr",     8));
  asthma::syms.push_back(symbol_t("ctr",    9));
  asthma::syms.push_back(symbol_t("dsisr",  18));
  asthma::syms.push_back(symbol_t("dar",    19));
  asthma::syms.push_back(symbol_t("dec",    22));
  asthma::syms.push_back(symbol_t("sdr1",   25));
  asthma::syms.push_back(symbol_t("srr0",   26));
  asthma::syms.push_back(symbol_t("srr1",   27));
  asthma::syms.push_back(symbol_t("tbl",    268));
  asthma::syms.push_back(symbol_t("tbu",    269));
  asthma::syms.push_back(symbol_t("sprg0",  272));
  asthma::syms.push_back(symbol_t("sprg1",  273));
  asthma::syms.push_back(symbol_t("sprg2",  274));
  asthma::syms.push_back(symbol_t("sprg3",  275));
  asthma::syms.push_back(symbol_t("ear",    282));
  asthma::syms.push_back(symbol_t("tbl_w",  284));
  asthma::syms.push_back(symbol_t("tbu_w",  285));
  asthma::syms.push_back(symbol_t("pvr",    287));
  asthma::syms.push_back(symbol_t("ibat0u", 528));
  asthma::syms.push_back(symbol_t("ibat0l", 529));
  asthma::syms.push_back(symbol_t("ibat1u", 530));
  asthma::syms.push_back(symbol_t("ibat1l", 531));
  asthma::syms.push_back(symbol_t("ibat2u", 532));
  asthma::syms.push_back(symbol_t("ibat2l", 533));
  asthma::syms.push_back(symbol_t("ibat3u", 534));
  asthma::syms.push_back(symbol_t("ibat3l", 535));
  asthma::syms.push_back(symbol_t("dbat0u", 536));
  asthma::syms.push_back(symbol_t("dbat0l", 537));
  asthma::syms.push_back(symbol_t("dbat1u", 538));
  asthma::syms.push_back(symbol_t("dbat1l", 539));
  asthma::syms.push_back(symbol_t("dbat2u", 540));
  asthma::syms.push_back(symbol_t("dbat2l", 541));
  asthma::syms.push_back(symbol_t("dbat3u", 542));
  asthma::syms.push_back(symbol_t("dbat3l", 543));
  asthma::syms.push_back(symbol_t("dabr",   1013));
  asthma::syms.push_back(symbol_t("fpecr",  1022));
  asthma::syms.push_back(symbol_t("pir",  1023));

  // Register names
  regs.push_back(reg_t("r0",   0));
  regs.push_back(reg_t("r1",   1));
  regs.push_back(reg_t("r2",   2));
  regs.push_back(reg_t("r3",   3));
  regs.push_back(reg_t("r4",   4));
  regs.push_back(reg_t("r5",   5));
  regs.push_back(reg_t("r6",   6));
  regs.push_back(reg_t("r7",   7));
  regs.push_back(reg_t("r8",   8));
  regs.push_back(reg_t("r9",   9));
  regs.push_back(reg_t("r10", 10));
  regs.push_back(reg_t("r11", 11));
  regs.push_back(reg_t("r12", 12));
  regs.push_back(reg_t("r13", 13));
  regs.push_back(reg_t("r14", 14));
  regs.push_back(reg_t("r15", 15));
  regs.push_back(reg_t("r16", 16));
  regs.push_back(reg_t("r17", 17));
  regs.push_back(reg_t("r18", 18));
  regs.push_back(reg_t("r19", 19));
  regs.push_back(reg_t("r20", 20));
  regs.push_back(reg_t("r21", 21));
  regs.push_back(reg_t("r22", 22));
  regs.push_back(reg_t("r23", 23));
  regs.push_back(reg_t("r24", 24));
  regs.push_back(reg_t("r25", 25));
  regs.push_back(reg_t("r26", 26));
  regs.push_back(reg_t("r27", 27));
  regs.push_back(reg_t("r28", 28));
  regs.push_back(reg_t("r29", 29));
  regs.push_back(reg_t("r30", 30));
  regs.push_back(reg_t("r31", 31));
  regs.push_back(reg_t("fr0",  32));
  regs.push_back(reg_t("fr1",  33));
  regs.push_back(reg_t("fr2",  34));
  regs.push_back(reg_t("fr3",  35));
  regs.push_back(reg_t("fr4",  36));
  regs.push_back(reg_t("fr5",  37));
  regs.push_back(reg_t("fr6",  38));
  regs.push_back(reg_t("fr7",  39));
  regs.push_back(reg_t("fr8",  40));
  regs.push_back(reg_t("fr9",  41));
  regs.push_back(reg_t("fr10", 42));
  regs.push_back(reg_t("fr11", 43));
  regs.push_back(reg_t("fr12", 44));
  regs.push_back(reg_t("fr13", 45));
  regs.push_back(reg_t("fr14", 46));
  regs.push_back(reg_t("fr15", 47));
  regs.push_back(reg_t("fr16", 48));
  regs.push_back(reg_t("fr17", 49));
  regs.push_back(reg_t("fr18", 50));
  regs.push_back(reg_t("fr19", 51));
  regs.push_back(reg_t("fr20", 52));
  regs.push_back(reg_t("fr21", 53));
  regs.push_back(reg_t("fr22", 54));
  regs.push_back(reg_t("fr23", 55));
  regs.push_back(reg_t("fr24", 56));
  regs.push_back(reg_t("fr25", 57));
  regs.push_back(reg_t("fr26", 58));
  regs.push_back(reg_t("fr27", 59));
  regs.push_back(reg_t("fr28", 60));
  regs.push_back(reg_t("fr29", 61));
  regs.push_back(reg_t("fr30", 62));
  regs.push_back(reg_t("fr31", 63));
  // Alias frX to fX
  regs.push_back(reg_t("f0",   32));
  regs.push_back(reg_t("f1",   33));
  regs.push_back(reg_t("f2",   34));
  regs.push_back(reg_t("f3",   35));
  regs.push_back(reg_t("f4",   36));
  regs.push_back(reg_t("f5",   37));
  regs.push_back(reg_t("f6",   38));
  regs.push_back(reg_t("f7",   39));
  regs.push_back(reg_t("f8",   40));
  regs.push_back(reg_t("f9",   41));
  regs.push_back(reg_t("f10",  42));
  regs.push_back(reg_t("f11",  43));
  regs.push_back(reg_t("f12",  44));
  regs.push_back(reg_t("f13",  45));
  regs.push_back(reg_t("f14",  46));
  regs.push_back(reg_t("f15",  47));
  regs.push_back(reg_t("f16",  48));
  regs.push_back(reg_t("f17",  49));
  regs.push_back(reg_t("f18",  50));
  regs.push_back(reg_t("f19",  51));
  regs.push_back(reg_t("f20",  52));
  regs.push_back(reg_t("f21",  53));
  regs.push_back(reg_t("f22",  54));
  regs.push_back(reg_t("f23",  55));
  regs.push_back(reg_t("f24",  56));
  regs.push_back(reg_t("f25",  57));
  regs.push_back(reg_t("f26",  58));
  regs.push_back(reg_t("f27",  59));
  regs.push_back(reg_t("f28",  60));
  regs.push_back(reg_t("f29",  61));
  regs.push_back(reg_t("f30",  62));
  regs.push_back(reg_t("f31",  63));
  regs.push_back(reg_t("cr0",  64));
  regs.push_back(reg_t("cr1",  65));
  regs.push_back(reg_t("cr2",  66));
  regs.push_back(reg_t("cr3",  67));
  regs.push_back(reg_t("cr4",  68));
  regs.push_back(reg_t("cr5",  69));
  regs.push_back(reg_t("cr6",  70));
  regs.push_back(reg_t("cr7",  71));
}

ppc_proc_t::~ppc_proc_t()
{
}

bool ppc_proc_t::assume_reg(char *reg, uintmax_t value)
{
  return false;
}

static int get_gpr_reg(op_t &op)
{
  int r;
  if ( (op.type == OPT_REG) || (op.type == OPT_DISP) )
  {
    if ( op.reg > 31 )
      return -1;
    r = op.reg;
  }
  else
    return -2;
  return r;
}

static int get_fpr_reg(op_t &op)
{
  int r;
  if ( op.type == OPT_REG )
  {
    if ( op.reg < 32 || op.reg > 63 )
      return -1;
    r = op.reg - 32;
  }
  else
    return -2;
  return r;
}

static int get_cr_reg(op_t &op)
{
  int r;
  if ( op.type == OPT_REG )
  {
    if ( op.reg < 64 )
      return -1;
    r = op.reg - 64;
  }
  else
    return -2;
  return r;
}

enum ppc_op_operand_t
{
  PPC_NA = 0,
  PPC_rD,
  PPC_rS=PPC_rD,
  PPC_rA,
  PPC_rAb, // may not be 0
  PPC_rB,
  PPC_frD,
  PPC_frS=PPC_frD,
  PPC_frA,
  PPC_frB,
  PPC_frC,
  PPC_UIMM16,
  PPC_SIMM16,
  PPC_OFF26,
  PPC_OFF16,
  PPC_ADDR26,
  PPC_ADDR16,
  PPC_BO,
  PPC_BI,
  PPC_crfD,
  PPC_crfS,
  PPC_L,
  PPC_crbD,
  PPC_crbA,
  PPC_crbB,
  PPC_DISP16,
  PPC_DISP16b, // reg may not be 0
  PPC_NB,
  PPC_SPR,
  PPC_DCR,
  PPC_SR,
  PPC_CRM,
  PPC_FM,
  PPC_FPIMM,
  PPC_SH,
  PPC_MB,
  PPC_ME,
  PPC_TO,
};

enum ppc_op_flags_t
{
  PPC_Rc  = 1, // Set 0x00000001
  PPC_Rc2 = 2, // Set 0x04000000
};

#define MAX_PPC_OP_CNT (5)

struct ppc_op_t
{
  char mnem[MAX_MNEM_LEN];
  uint32_t code;
  uint32_t flags;
  ppc_op_operand_t oper[MAX_PPC_OP_CNT];
};

static ppc_op_t ppcops[] = {
  { "add",        0x7C000214,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "addo",       0x7C000614,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "addc",       0x7C000014,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "addco",      0x7C000414,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "adde",       0x7C000114,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "addeo",      0x7C000514,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "divw",       0x7C0003D6,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "divwo",      0x7C0007D6,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "divwu",      0x7C000396,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "divwuo",     0x7C000796,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "eciwx",      0x7C00026C,  0,          { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "ecowx",      0x7C00036C,  0,          { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },

  { "addi",       0x38000000,  0,          { PPC_rD,      PPC_rA,      PPC_SIMM16,  PPC_NA,      PPC_NA } },
  { "addic",      0x30000000,  PPC_Rc2,    { PPC_rD,      PPC_rA,      PPC_SIMM16,  PPC_NA,      PPC_NA } },
  { "addis",      0x3C000000,  0,          { PPC_rD,      PPC_rA,      PPC_SIMM16,  PPC_NA,      PPC_NA } },

  { "addme",      0x7C0001D4,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "addmeo",     0x7C0005D4,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "addze",      0x7C000194,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "addzeo",     0x7C000594,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_NA,      PPC_NA,      PPC_NA } },

  { "and",        0x7C000038,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "andc",       0x7C000078,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "eqv",        0x7C000238,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_rB,      PPC_NA,      PPC_NA } },

  { "andi.",      0x70000000,  0,          { PPC_rA,      PPC_rS,      PPC_UIMM16,  PPC_NA,      PPC_NA } },
  { "andis.",     0x74000000,  0,          { PPC_rA,      PPC_rS,      PPC_UIMM16,  PPC_NA,      PPC_NA } },

  { "b",          0x48000000,  0,          { PPC_OFF26,   PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "ba",         0x48000002,  0,          { PPC_ADDR26,  PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "bl",         0x48000001,  0,          { PPC_OFF26,   PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "bla",        0x48000003,  0,          { PPC_ADDR26,  PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },

  { "bc",         0x40000000,  0,          { PPC_BO,      PPC_BI,      PPC_OFF16,   PPC_NA,      PPC_NA } },
  { "bca",        0x40000002,  0,          { PPC_BO,      PPC_BI,      PPC_ADDR16,  PPC_NA,      PPC_NA } },
  { "bcl",        0x40000001,  0,          { PPC_BO,      PPC_BI,      PPC_OFF16,   PPC_NA,      PPC_NA } },
  { "bcla",       0x40000003,  0,          { PPC_BO,      PPC_BI,      PPC_ADDR16,  PPC_NA,      PPC_NA } },

  { "bcctr",      0x4C000420,  0,          { PPC_BO,      PPC_BI,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "bcctrl",     0x4C000421,  0,          { PPC_BO,      PPC_BI,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "bclr",       0x4C000020,  0,          { PPC_BO,      PPC_BI,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "bclrl",      0x4C000021,  0,          { PPC_BO,      PPC_BI,      PPC_NA,      PPC_NA,      PPC_NA } },

  { "cmp",        0x7C000000,  0,          { PPC_crfD,    PPC_L,       PPC_rA,      PPC_rB,      PPC_NA } },
  { "cmpl",       0x7C000040,  0,          { PPC_crfD,    PPC_L,       PPC_rA,      PPC_rB,      PPC_NA } },

  { "cmpi",       0x2C000000,  0,          { PPC_crfD,    PPC_L,       PPC_rA,      PPC_SIMM16,  PPC_NA } },
  { "cmpli",      0x28000000,  0,          { PPC_crfD,    PPC_L,       PPC_rA,      PPC_UIMM16,  PPC_NA } },

  { "cntlzw",     0x7C000034,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "extsb",      0x7C000774,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "extsh",      0x7C000734,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_NA,      PPC_NA,      PPC_NA } },

  { "crand",      0x4C000202,  0,          { PPC_crbD,    PPC_crbA,    PPC_crbB,    PPC_NA,      PPC_NA } },
  { "crandc",     0x4C000102,  0,          { PPC_crbD,    PPC_crbA,    PPC_crbB,    PPC_NA,      PPC_NA } },
  { "creqv",      0x4C000242,  0,          { PPC_crbD,    PPC_crbA,    PPC_crbB,    PPC_NA,      PPC_NA } },
  { "crnand",     0x4C0001C2,  0,          { PPC_crbD,    PPC_crbA,    PPC_crbB,    PPC_NA,      PPC_NA } },
  { "crnor",      0x4C000042,  0,          { PPC_crbD,    PPC_crbA,    PPC_crbB,    PPC_NA,      PPC_NA } },
  { "cror",       0x4C000382,  0,          { PPC_crbD,    PPC_crbA,    PPC_crbB,    PPC_NA,      PPC_NA } },
  { "crorc",      0x4C000342,  0,          { PPC_crbD,    PPC_crbA,    PPC_crbB,    PPC_NA,      PPC_NA } },
  { "crxor",      0x4C000182,  0,          { PPC_crbD,    PPC_crbA,    PPC_crbB,    PPC_NA,      PPC_NA } },

  { "dcba",       0x7C0005EC,  0,          { PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "dcbf",       0x7C0000AC,  0,          { PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "dcbi",       0x7C0003AC,  0,          { PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "dcbst",      0x7C00006C,  0,          { PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "dcbt",       0x7C00022C,  0,          { PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "dcbtst",     0x7C0001EC,  0,          { PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "dcbz",       0x7C0007EC,  0,          { PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "icbi",       0x7C0007AC,  0,          { PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA,      PPC_NA } },

  { "eieio",      0x7C0006AC,  0,          { PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "isync",      0x4C00012C,  0,          { PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },

  { "fabs",       0xFC000210,  PPC_Rc,     { PPC_frD,     PPC_frB,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "fctiw",      0xFC00001C,  PPC_Rc,     { PPC_frD,     PPC_frB,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "fctiwz",     0xFC00001E,  PPC_Rc,     { PPC_frD,     PPC_frB,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "fmr",        0xFC000090,  PPC_Rc,     { PPC_frD,     PPC_frB,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "fnabs",      0xFC000110,  PPC_Rc,     { PPC_frD,     PPC_frB,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "fneg",       0xFC000050,  PPC_Rc,     { PPC_frD,     PPC_frB,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "fres",       0xEC000030,  PPC_Rc,     { PPC_frD,     PPC_frB,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "frsp",       0xFC000018,  PPC_Rc,     { PPC_frD,     PPC_frB,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "frsqrte",    0xFC000034,  PPC_Rc,     { PPC_frD,     PPC_frB,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "fsqrt",      0xFC00002C,  PPC_Rc,     { PPC_frD,     PPC_frB,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "fsqrts",     0xEC00002C,  PPC_Rc,     { PPC_frD,     PPC_frB,     PPC_NA,      PPC_NA,      PPC_NA } },

  { "fadd",       0xFC00002A,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frB,     PPC_NA,      PPC_NA } },
  { "fadds",      0xEC00002A,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frB,     PPC_NA,      PPC_NA } },
  { "fdiv",       0xFC000024,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frB,     PPC_NA,      PPC_NA } },
  { "fdivs",      0xEC000024,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frB,     PPC_NA,      PPC_NA } },
  { "fsub",       0xFC000028,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frB,     PPC_NA,      PPC_NA } },
  { "fsubs",      0xEC000028,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frB,     PPC_NA,      PPC_NA } },

  { "fcmpo",      0xFC000040,  0,          { PPC_crfD,    PPC_frA,     PPC_frB,     PPC_NA,      PPC_NA } },
  { "fcmpu",      0xFC000000,  0,          { PPC_crfD,    PPC_frA,     PPC_frB,     PPC_NA,      PPC_NA } },

  { "fmadd",      0xFC00003A,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frC,     PPC_frB,     PPC_NA } },
  { "fmadds",     0xEC00003A,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frC,     PPC_frB,     PPC_NA } },
  { "fmsub",      0xFC000038,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frC,     PPC_frB,     PPC_NA } },
  { "fmsubs",     0xEC000038,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frC,     PPC_frB,     PPC_NA } },
  { "fnmadd",     0xFC00003E,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frC,     PPC_frB,     PPC_NA } },
  { "fnmadds",    0xEC00003E,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frC,     PPC_frB,     PPC_NA } },
  { "fnmsub",     0xFC00003C,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frC,     PPC_frB,     PPC_NA } },
  { "fnmsubs",    0xEC00003C,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frC,     PPC_frB,     PPC_NA } },
  { "fsel",       0xFC00002E,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frC,     PPC_frB,     PPC_NA } },

  { "fmul",       0xFC000032,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frC,     PPC_NA,      PPC_NA } },
  { "fmuls",      0xEC000032,  PPC_Rc,     { PPC_frD,     PPC_frA,     PPC_frC,     PPC_NA,      PPC_NA } },

  { "lbz",        0x88000000,  0,          { PPC_rD,      PPC_DISP16,  PPC_NA,      PPC_NA,      PPC_NA } },
  { "lbzu",       0x8C000000,  0,          { PPC_rD,      PPC_DISP16b, PPC_NA,      PPC_NA,      PPC_NA } },
  { "lbzux",      0x7C0000EE,  0,          { PPC_rD,      PPC_rAb,     PPC_rB,      PPC_NA,      PPC_NA } },
  { "lbzx",       0x7C0000AE,  0,          { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "lfd",        0xC8000000,  0,          { PPC_frD,     PPC_DISP16,  PPC_NA,      PPC_NA,      PPC_NA } },
  { "lfdu",       0xCC000000,  0,          { PPC_frD,     PPC_DISP16b, PPC_NA,      PPC_NA,      PPC_NA } },
  { "lfdux",      0x7C0004EE,  0,          { PPC_frD,     PPC_rAb,     PPC_rB,      PPC_NA,      PPC_NA } },
  { "lfdx",       0x7C0004AE,  0,          { PPC_frD,     PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "lfs",        0xC0000000,  0,          { PPC_frD,     PPC_DISP16,  PPC_NA,      PPC_NA,      PPC_NA } },
  { "lfsu",       0xC4000000,  0,          { PPC_frD,     PPC_DISP16b, PPC_NA,      PPC_NA,      PPC_NA } },
  { "lfsux",      0x7C00046E,  0,          { PPC_frD,     PPC_rAb,     PPC_rB,      PPC_NA,      PPC_NA } },
  { "lfsx",       0x7C00042E,  0,          { PPC_frD,     PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "lha",        0xA8000000,  0,          { PPC_rD,      PPC_DISP16,  PPC_NA,      PPC_NA,      PPC_NA } },
  { "lhau",       0xAC000000,  0,          { PPC_rD,      PPC_DISP16b, PPC_NA,      PPC_NA,      PPC_NA } },
  { "lhaux",      0x7C0002EE,  0,          { PPC_rD,      PPC_rAb,     PPC_rB,      PPC_NA,      PPC_NA } },
  { "lhax",       0x7C0002AE,  0,          { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "lhbrx",      0x7C00062C,  0,          { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "lhz",        0xA0000000,  0,          { PPC_rD,      PPC_DISP16,  PPC_NA,      PPC_NA,      PPC_NA } },
  { "lhzu",       0xA4000000,  0,          { PPC_rD,      PPC_DISP16b, PPC_NA,      PPC_NA,      PPC_NA } },
  { "lhzux",      0x7C00026E,  0,          { PPC_rD,      PPC_rAb,     PPC_rB,      PPC_NA,      PPC_NA } },
  { "lhzx",       0x7C00022E,  0,          { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "lmw",        0xB8000000,  0,          { PPC_rD,      PPC_DISP16,  PPC_NA,      PPC_NA,      PPC_NA } },
  { "lswi",       0x7C0004AA,  0,          { PPC_rD,      PPC_rA,      PPC_NB,      PPC_NA,      PPC_NA } },
  { "lswx",       0x7C00042A,  0,          { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "lwarx",      0x7C000028,  0,          { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "lwbrx",      0x7C00042C,  0,          { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "lwz",        0x80000000,  0,          { PPC_rD,      PPC_DISP16,  PPC_NA,      PPC_NA,      PPC_NA } },
  { "lwzu",       0x84000000,  0,          { PPC_rD,      PPC_DISP16b, PPC_NA,      PPC_NA,      PPC_NA } },
  { "lwzux",      0x7C00006E,  0,          { PPC_rD,      PPC_rAb,     PPC_rB,      PPC_NA,      PPC_NA } },
  { "lwzx",       0x7C00002E,  0,          { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },

  { "mcrf",       0x4C000000,  0,          { PPC_crfD,    PPC_crfS,    PPC_NA,      PPC_NA,      PPC_NA } },
  { "mcrfs",      0xFC000080,  0,          { PPC_crfD,    PPC_crfS,    PPC_NA,      PPC_NA,      PPC_NA } },
  { "mcrxr",      0x7C000400,  0,          { PPC_crfD,    PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "mfcr",       0x7C000026,  0,          { PPC_rD,      PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "mffs",       0xFC00048E,  PPC_Rc,     { PPC_frD,     PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "mfmsr",      0x7C0000A6,  0,          { PPC_rD,      PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "mfdcr",      0x7C000286,  0,          { PPC_rD,      PPC_DCR,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "mfspr",      0x7C0002A6,  0,          { PPC_rD,      PPC_SPR,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "mfsr",       0x7C0004A6,  0,          { PPC_rD,      PPC_SR,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "mfsrin",     0x7C000526,  0,          { PPC_rD,      PPC_rB,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "mftb",       0x7C0002E6,  0,          { PPC_rD,      PPC_SPR,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "mtcrf",      0x7C000120,  0,          { PPC_CRM,     PPC_rS,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "mtfsb0",     0xFC00008C,  PPC_Rc,     { PPC_crbD,    PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "mtfsb1",     0xFC00004C,  PPC_Rc,     { PPC_crbD,    PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "mtfsf",      0xFC00058E,  PPC_Rc,     { PPC_FM,      PPC_frB,     PPC_NA,      PPC_NA,      PPC_NA } },
  { "mtfsfi",     0xFC00010C,  PPC_Rc,     { PPC_crfD,    PPC_FPIMM,   PPC_NA,      PPC_NA,      PPC_NA } },

  { "mtmsr",      0x7C000124,  0,          { PPC_rS,      PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "mtdcr",      0x7C000386,  0,          { PPC_DCR,     PPC_rS,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "mtspr",      0x7C0003A6,  0,          { PPC_SPR,     PPC_rS,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "mtsr",       0x7C0001A4,  0,          { PPC_SR,      PPC_rS,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "mtsrin",     0x7C0001E4,  0,          { PPC_rS,      PPC_rB,      PPC_NA,      PPC_NA,      PPC_NA } },

  { "mulhw",      0x7C000096,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "mulhwu",     0x7C000016,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "mulli",      0x1C000000,  0,          { PPC_rD,      PPC_rA,      PPC_SIMM16,  PPC_NA,      PPC_NA } },
  { "mullw",      0x7C0001D6,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "mullwo",     0x7C0005D6,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },

  { "nand",       0x7C0003B8,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "neg",        0x7C0000D0,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "nego",       0x7C0004D0,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "nor",        0x7C0000F8,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "or",         0x7C000378,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "orc",        0x7C000338,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "ori",        0x60000000,  0,          { PPC_rA,      PPC_rS,      PPC_UIMM16,  PPC_NA,      PPC_NA } },
  { "oris",       0x64000000,  0,          { PPC_rA,      PPC_rS,      PPC_UIMM16,  PPC_NA,      PPC_NA } },

  { "rfi",        0x4C000064,  0,          { PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },

  { "rlwimi",     0x50000000,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_SH,      PPC_MB,      PPC_ME } },
  { "rlwinm",     0x54000000,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_SH,      PPC_MB,      PPC_ME } },
  { "rlwnm",      0x5C000000,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_SH,      PPC_MB,      PPC_ME } },

  { "sc",         0x44000002,  0,          { PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },

  { "slw",        0x7C000030,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "sraw",       0x7C000630,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "srawi",      0x7C000670,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_SH,      PPC_NA,      PPC_NA } },
  { "srw",        0x7C000430,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_rB,      PPC_NA,      PPC_NA } },

  { "stb",        0x98000000,  0,          { PPC_rS,      PPC_DISP16,  PPC_NA,      PPC_NA,      PPC_NA } },
  { "stbu",       0x9C000000,  0,          { PPC_rS,      PPC_DISP16b, PPC_NA,      PPC_NA,      PPC_NA } },
  { "stbux",      0x7C0001EE,  0,          { PPC_rS,      PPC_rAb,     PPC_rB,      PPC_NA,      PPC_NA } },
  { "stbx",       0x7C0001AE,  0,          { PPC_rS,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "stfd",       0xD8000000,  0,          { PPC_frS,     PPC_DISP16,  PPC_NA,      PPC_NA,      PPC_NA } },
  { "stfdu",      0xDC000000,  0,          { PPC_frS,     PPC_DISP16b, PPC_NA,      PPC_NA,      PPC_NA } },
  { "stfdux",     0x7C0005EE,  0,          { PPC_frS,     PPC_rAb,     PPC_rB,      PPC_NA,      PPC_NA } },
  { "stfdx",      0x7C0005AE,  0,          { PPC_frS,     PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "stfiwx",     0x7C0007AE,  0,          { PPC_frS,     PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "stfs",       0xD0000000,  0,          { PPC_frS,     PPC_DISP16,  PPC_NA,      PPC_NA,      PPC_NA } },
  { "stfsu",      0xD4000000,  0,          { PPC_frS,     PPC_DISP16b, PPC_NA,      PPC_NA,      PPC_NA } },
  { "stfsux",     0x7C00056E,  0,          { PPC_frS,     PPC_rAb,     PPC_rB,      PPC_NA,      PPC_NA } },
  { "stfsx",      0x7C00052E,  0,          { PPC_frS,     PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "sth",        0xB0000000,  0,          { PPC_rS,      PPC_DISP16,  PPC_NA,      PPC_NA,      PPC_NA } },
  { "sthbrx",     0x7C00072C,  0,          { PPC_rS,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "sthu",       0xB4000000,  0,          { PPC_rS,      PPC_DISP16b, PPC_NA,      PPC_NA,      PPC_NA } },
  { "sthux",      0x7C00036E,  0,          { PPC_rS,      PPC_rAb,     PPC_rB,      PPC_NA,      PPC_NA } },
  { "sthx",       0x7C00032E,  0,          { PPC_rS,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "stmw",       0xBC000000,  0,          { PPC_rS,      PPC_DISP16,  PPC_NA,      PPC_NA,      PPC_NA } },
  { "stswi",      0x7C0005AA,  0,          { PPC_rS,      PPC_rA,      PPC_NB,      PPC_NA,      PPC_NA } },
  { "stswx",      0x7C00052A,  0,          { PPC_rS,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "stw",        0x90000000,  0,          { PPC_rS,      PPC_DISP16,  PPC_NA,      PPC_NA,      PPC_NA } },
  { "stwbrx",     0x7C00052C,  0,          { PPC_rS,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "stwcx.",     0x7C00012D,  0,          { PPC_rS,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "stwu",       0x94000000,  0,          { PPC_rS,      PPC_DISP16b, PPC_NA,      PPC_NA,      PPC_NA } },
  { "stwux",      0x7C0001EE,  0,          { PPC_rS,      PPC_rAb,     PPC_rB,      PPC_NA,      PPC_NA } },
  { "stwx",       0x7C0001AE,  0,          { PPC_rS,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },

  { "subf",       0x7C000050,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "subfo",      0x7C000450,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "subfc",      0x7C000010,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "subfco",     0x7C000410,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "subfe",      0x7C000110,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "subfeo",     0x7C000510,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "subfic",     0x20000000,  0,          { PPC_rD,      PPC_rA,      PPC_SIMM16,  PPC_NA,      PPC_NA } },
  { "subfme",     0x7C0001D0,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "subfmeo",    0x7C0005D0,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "subfze",     0x7C000190,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "subfzeo",    0x7C000590,  PPC_Rc,     { PPC_rD,      PPC_rA,      PPC_NA,      PPC_NA,      PPC_NA } },

  { "sync",       0x7C0004AC,  0,          { PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "tlbia",      0x7C0002E4,  0,          { PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "tlbie",      0x7C000264,  0,          { PPC_rB,      PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },
  { "tlbsync",    0x7C00046C,  0,          { PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA,      PPC_NA } },

  { "tw",         0x7C000008,  0,          { PPC_TO,      PPC_rA,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "twi",        0x0C000008,  0,          { PPC_TO,      PPC_rA,      PPC_SIMM16,  PPC_NA,      PPC_NA } },

  { "xor",        0x7C000278,  PPC_Rc,     { PPC_rA,      PPC_rS,      PPC_rB,      PPC_NA,      PPC_NA } },
  { "xori",       0x68000000,  0,          { PPC_rA,      PPC_rS,      PPC_UIMM16,  PPC_NA,      PPC_NA } },
  { "xoris",      0x6C000000,  0,          { PPC_rA,      PPC_rS,      PPC_UIMM16,  PPC_NA,      PPC_NA } },
};

int ppc_proc_t::process_insn(insn_t &cmd)
{
  uint32_t code = 0x00000000;
  cmd.len = 4;

  bool pass = false;
  ppc_op_t thisop;
  char mnemtmp[MAX_MNEM_LEN+1];
  for ( unsigned int i=0; i < acount(ppcops) && !pass; i++ )
  {
    thisop = ppcops[i];
    code = thisop.code;
    if ( (thisop.flags & PPC_Rc) || (thisop.flags & PPC_Rc2) )
    {
      sprintf(mnemtmp, "%s.", thisop.mnem);
      if ( strcmp(mnemtmp, cmd.mnem) == 0 )
      {
        if ( thisop.flags & PPC_Rc )
          code |= 1;
        if ( thisop.flags & PPC_Rc2 )
          code |= 0x04000000;
      }
      else
        sprintf(mnemtmp, "%s", thisop.mnem);
    }
    else
      sprintf(mnemtmp, "%s", thisop.mnem);
    if ( strcmp(mnemtmp, cmd.mnem) != 0 )
      continue;

    pass = true;

    // Handle the instruction
    int opcnt;
    for ( opcnt=0; opcnt < MAX_PPC_OP_CNT; opcnt++ )
    {
      if ( thisop.oper[opcnt] == PPC_NA )
        break;
    }
    if ( cmd.opcnt > opcnt ) return PROC_ASM_ERR_TOO_MANY_OPS;
    if ( cmd.opcnt < opcnt ) return PROC_ASM_ERR_TOO_FEW_OPS;
    for ( int l=0; l < opcnt; l++ )
    {
      int reg;
      switch ( thisop.oper[l] )
      {
        case PPC_NA:
          break;
        case PPC_rD:
          reg = get_gpr_reg(cmd.op[l]);
          if ( reg == -2 ) return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( reg == -1 ) return PROC_ASM_ERR_BAD_OP_VAL  + l;
          code |= reg << 21;
          break;
        case PPC_rAb:
        case PPC_rA:
          reg = get_gpr_reg(cmd.op[l]);
          if ( reg == -2 )                             return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( reg == -1 )                             return PROC_ASM_ERR_BAD_OP_VAL  + l;
          if ( thisop.oper[l] == PPC_rAb && reg == 0 ) return PROC_ASM_ERR_BAD_OP_VAL  + l;
          code |= reg << 16;
          break;
        case PPC_rB:
          reg = get_gpr_reg(cmd.op[l]);
          if ( reg == -2 ) return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( reg == -1 ) return PROC_ASM_ERR_BAD_OP_VAL  + l;
          code |= reg << 11;
          break;
        case PPC_frD:
          reg = get_fpr_reg(cmd.op[l]);
          if ( reg == -2 ) return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( reg == -1 ) return PROC_ASM_ERR_BAD_OP_VAL  + l;
          code |= reg << 21;
          break;
        case PPC_frA:
          reg = get_fpr_reg(cmd.op[l]);
          if ( reg == -2 ) return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( reg == -1 ) return PROC_ASM_ERR_BAD_OP_VAL  + l;
          code |= reg << 16;
          break;
        case PPC_frB:
          reg = get_fpr_reg(cmd.op[l]);
          if ( reg == -2 ) return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( reg == -1 ) return PROC_ASM_ERR_BAD_OP_VAL  + l;
          code |= reg << 11;
          break;
        case PPC_frC:
          reg = get_fpr_reg(cmd.op[l]);
          if ( reg == -2 ) return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( reg == -1 ) return PROC_ASM_ERR_BAD_OP_VAL  + l;
          code |= reg << 6;
          break;
        case PPC_UIMM16:
          if ( cmd.op[l].type != OPT_NUM )                       return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0xFFFF ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value;
          break;
        case PPC_SIMM16:
          if ( cmd.op[l].type != OPT_NUM )                             return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < -0x8000 || cmd.op[l].value > 0x7FFF ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= (cmd.op[l].value & 0xFFFF);
          break;
        case PPC_OFF26:
        {
          if ( cmd.op[l].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value & 3 )       return PROC_ASM_ERR_BAD_OP_VAL  + l;
          int32_t disp = cmd.op[l].value - cmd.ea;
          if ( disp < -0x2000000 || disp > 0x1FFFFFC )
            return PROC_ASM_ERR_OUT_OF_RANGE;
          disp &= 0x3FFFFFC;
          code |= disp;
          break;
        }
        case PPC_OFF16:
        {
          if ( cmd.op[l].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value & 3 )       return PROC_ASM_ERR_BAD_OP_VAL  + l;
          int32_t disp = cmd.op[l].value - cmd.ea;
          if ( disp < -0x8000 || disp > 0x7FFC )
            return PROC_ASM_ERR_OUT_OF_RANGE;
          disp &= 0xFFFC;
          code |= disp;
          break;
        }
        case PPC_ADDR26:
        {
          if ( cmd.op[l].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value & 3 )       return PROC_ASM_ERR_BAD_OP_VAL  + l;
          uint32_t addr = cmd.op[l].value;
          if ( addr < 0 || addr > 0x3FFFFFC )
            return PROC_ASM_ERR_OUT_OF_RANGE;
          addr &= 0x3FFFFFC;
          code |= addr;
          break;
        }
        case PPC_ADDR16:
        {
          if ( cmd.op[l].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value & 3 )       return PROC_ASM_ERR_BAD_OP_VAL  + l;
          uint32_t addr = cmd.op[l].value;
          if ( addr < 0 || addr > 0xFFFC )
            return PROC_ASM_ERR_OUT_OF_RANGE;
          addr &= 0xFFFC;
          code |= addr;
          break;
        }
        case PPC_BO:
          if ( cmd.op[l].type != OPT_NUM )                     return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0x1F ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 21;
          break;
        case PPC_BI:
          if ( cmd.op[l].type != OPT_NUM )                     return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0x1F ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 16;
          break;
        case PPC_crfD:
          reg = get_cr_reg(cmd.op[l]);
          if ( reg == -2 ) return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( reg == -1 ) return PROC_ASM_ERR_BAD_OP_VAL  + l;
          code |= reg << 23;
          break;
        case PPC_crfS:
          reg = get_cr_reg(cmd.op[l]);
          if ( reg == -2 ) return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( reg == -1 ) return PROC_ASM_ERR_BAD_OP_VAL  + l;
          code |= reg << 18;
          break;
        case PPC_L:
          if ( cmd.op[l].type != OPT_NUM )                  return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 1 ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 21;
          break;
        case PPC_crbD:
          if ( cmd.op[l].type != OPT_NUM )                     return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0x1F ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 21;
          break;
        case PPC_crbA:
          if ( cmd.op[l].type != OPT_NUM )                     return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0x1F ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 16;
          break;
        case PPC_crbB:
          if ( cmd.op[l].type != OPT_NUM )                     return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0x1F ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 11;
          break;
        case PPC_DISP16:
        case PPC_DISP16b:
        {
          if ( cmd.op[l].type != OPT_DISP )                return PROC_ASM_ERR_BAD_OP_TYPE + l;
          reg = get_gpr_reg(cmd.op[l]);
          if ( reg == -1 )                                 return PROC_ASM_ERR_BAD_OP_VAL  + l;
          if ( thisop.oper[l] == PPC_DISP16b && reg == 0 ) return PROC_ASM_ERR_BAD_OP_VAL  + l;
          if ( cmd.op[l].disp < -0x8000 || cmd.op[l].disp > 0x7FFF )
            return PROC_ASM_ERR_OUT_OF_RANGE;
          code |= reg << 16;
          code |= cmd.op[l].value & 0xFFFF;
          break;
        }
        case PPC_TO:
          if ( cmd.op[l].type != OPT_NUM )                     return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0x1F ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 21;
          break;
        case PPC_SH:
        case PPC_NB:
          if ( cmd.op[l].type != OPT_NUM )                     return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0x1F ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 11;
          break;
        case PPC_MB:
          if ( cmd.op[l].type != OPT_NUM )                     return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0x1F ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 6;
          break;
        case PPC_ME:
          if ( cmd.op[l].type != OPT_NUM )                     return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0x1F ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 1;
          break;
        case PPC_SPR:
          if ( cmd.op[l].type != OPT_NUM )                      return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0x3FF ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= ((cmd.op[l].value >> 0) & 0x1F) << 16;
          code |= ((cmd.op[l].value >> 5) & 0x1F) << 11;
          break;
        case PPC_DCR:
          if ( cmd.op[l].type != OPT_NUM )                      return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0x3FF ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= ((cmd.op[l].value >> 0) & 0x1F) << 16;
          code |= ((cmd.op[l].value >> 5) & 0x1F) << 11;
          break;
        case PPC_SR:
          if ( cmd.op[l].type != OPT_NUM )                    return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0xF ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 16;
          break;
        case PPC_CRM:
          if ( cmd.op[l].type != OPT_NUM )                     return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0xFF ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 12;
          break;
        case PPC_FM:
          if ( cmd.op[l].type != OPT_NUM )                     return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0xFF ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 17;
          break;
        case PPC_FPIMM:
          if ( cmd.op[l].type != OPT_NUM )                    return PROC_ASM_ERR_BAD_OP_TYPE + l;
          if ( cmd.op[l].value < 0 || cmd.op[l].value > 0xF ) return PROC_ASM_ERR_BAD_OP_VAL + l;
          code |= cmd.op[l].value << 12;
          break;
      }
    }
  }
  if ( !pass )
    return PROC_ASM_ERR_BAD_MNEM;
  if ( endian == PROC_ENDIAN_BE )
  {
    cmd.code[3] = (code >>  0) & 0xFF;
    cmd.code[2] = (code >>  8) & 0xFF;
    cmd.code[1] = (code >> 16) & 0xFF;
    cmd.code[0] = (code >> 24) & 0xFF;
  }
  else // LE
  {
    cmd.code[0] = (code >>  0) & 0xFF;
    cmd.code[1] = (code >>  8) & 0xFF;
    cmd.code[2] = (code >> 16) & 0xFF;
    cmd.code[3] = (code >> 24) & 0xFF;
  }

  return PROC_ASM_ERR_OK;
}

proc_init_def(ppc)
{
  return new ppc_proc_t();
}
