/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "parser.hpp"
#include "proc.hpp"

struct pgmmus_proc_t : proc_t
{
  pgmmus_proc_t();
  virtual ~pgmmus_proc_t();
//--- Variables
//--- Functions
  virtual bool assume_reg(char *reg, uintmax_t value);
  virtual int process_insn(insn_t &cmd);
};

pgmmus_proc_t::pgmmus_proc_t()
{
  // Basic info
  sprintf(name, "PGMMUS");
  sprintf(cmts, ";");
  case_sense = false;
  deref_style = PROC_DEREF_NUM_PARENS_REG;
  word_size = 2;
  addr_style = PROC_ADDR_PARENS;
  endian = PROC_ENDIAN_BE;
  bot_addr = 0x000000;
  top_addr = 0xFFFFFF;
  num_pfx = '#';
  allowed_endians = (1 << PROC_ENDIAN_BE);

  // Push some pre-defined symbols

  // Register names
}

pgmmus_proc_t::~pgmmus_proc_t()
{
}

bool pgmmus_proc_t::assume_reg(char *reg, uintmax_t value)
{
  return false;
}

enum pgmmus_itypes
{
  PGMMUS_null = 0,

  PGMMUS_wait,
  PGMMUS_fade,
  PGMMUS_off,
  PGMMUS_repeat,
  PGMMUS_release,
  PGMMUS_tempo,
  PGMMUS_effect,
  PGMMUS_playsmp,
  PGMMUS_playfx,
  PGMMUS_nop,

  PGMMUS_last
};

int pgmmus_proc_t::process_insn(insn_t &cmd)
{
  uint8_t codes[4] = { 0x00, };
  int itype = PGMMUS_null;
  cmd.len = 1;

  mnem_check(cmd, "wait",    PGMMUS_wait);
  mnem_check(cmd, "fade",    PGMMUS_fade);
  mnem_check(cmd, "off",     PGMMUS_off);
  mnem_check(cmd, "repeat",  PGMMUS_repeat);
  mnem_check(cmd, "release", PGMMUS_release);
  mnem_check(cmd, "tempo",   PGMMUS_tempo);
  mnem_check(cmd, "effect",  PGMMUS_effect);
  mnem_check(cmd, "playsmp", PGMMUS_playsmp);
  mnem_check(cmd, "playfx",  PGMMUS_playfx);
  mnem_check(cmd, "nop",     PGMMUS_nop);

  switch ( itype )
  {
    default:
    case PGMMUS_null:
      return PROC_ASM_ERR_BAD_MNEM;

    case PGMMUS_wait:
      codes[0] = 0x00;
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value >= 0x40 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      codes[0] |= cmd.op[0].value & 0x3F;
      break;

    case PGMMUS_fade:
      codes[0] = 0x4A;
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value >= 0x100 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      codes[1] = cmd.op[0].value & 0xFF;
      cmd.len = 2;
      break;

    case PGMMUS_tempo:
      codes[0] = 0x4F;
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value >= 0x100 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      codes[1] = cmd.op[0].value & 0xFF;
      cmd.len = 2;
      break;

    case PGMMUS_nop:
      codes[0] = 0x43;
      codes[1] = 0x00;
      if ( cmd.opcnt > 0 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      cmd.len = 2;
      break;

    case PGMMUS_off:
      codes[0] = 0x4E;
      codes[1] = 0xC0;
      if ( cmd.opcnt > 0 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      cmd.len = 2;
      break;

    case PGMMUS_repeat:
      codes[0] = 0x4E;
      codes[1] = 0xD0;
      if ( cmd.opcnt > 0 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      cmd.len = 2;
      break;

    case PGMMUS_release:
      codes[0] = 0x4E;
      if ( cmd.opcnt > 1 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 1 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value >= 0x100 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      codes[1] = cmd.op[0].value & 0xFF;
      cmd.len = 2;
      break;

    case PGMMUS_effect:
      codes[0] = 0x40;
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value >= 0x10 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      if ( cmd.op[1].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP1_TYPE;
      if ( cmd.op[1].value >= 0x100 ) return PROC_ASM_ERR_BAD_OP1_VAL;
      codes[0] |= cmd.op[0].value & 0xF;
      codes[1] = cmd.op[1].value & 0xFF;
      cmd.len = 2;
      break;

    case PGMMUS_playsmp:
      codes[0] = 0x80;
      if ( cmd.opcnt > 2 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 2 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value >= 0x40 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      if ( cmd.op[1].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP1_TYPE;
      if ( cmd.op[1].value >= 0x100 ) return PROC_ASM_ERR_BAD_OP1_VAL;
      codes[0] |= cmd.op[0].value & 0x3F;
      codes[1] = cmd.op[1].value & 0xFF;
      cmd.len = 2;
      break;

    case PGMMUS_playfx:
      codes[0] = 0xC0;
      if ( cmd.opcnt > 4 ) return PROC_ASM_ERR_TOO_MANY_OPS;
      if ( cmd.opcnt < 4 ) return PROC_ASM_ERR_TOO_FEW_OPS;
      if ( cmd.op[0].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP0_TYPE;
      if ( cmd.op[0].value >= 0x40 ) return PROC_ASM_ERR_BAD_OP0_VAL;
      if ( (cmd.op[0].value & 0x30) == 0x10) return PROC_ASM_ERR_BAD_OP0_VAL;
      if ( (cmd.op[0].value & 0x30) == 0x20) return PROC_ASM_ERR_BAD_OP0_VAL;
      if ( cmd.op[1].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP1_TYPE;
      if ( cmd.op[1].value >= 0x100 ) return PROC_ASM_ERR_BAD_OP1_VAL;
      if ( cmd.op[2].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP2_TYPE;
      if ( cmd.op[2].value >= 0x100 ) return PROC_ASM_ERR_BAD_OP2_VAL;
      if ( cmd.op[3].type != OPT_NUM ) return PROC_ASM_ERR_BAD_OP3_TYPE;
      if ( cmd.op[3].value >= 0x100 ) return PROC_ASM_ERR_BAD_OP3_VAL;
      codes[0] |= cmd.op[0].value & 0x3F;
      codes[1] = cmd.op[1].value & 0xFF;
      codes[2] = cmd.op[2].value & 0xFF;
      codes[3] = cmd.op[3].value & 0xFF;
      cmd.len = 4;
      break;
  }
  cmd.code[0] = codes[0];
  cmd.code[1] = codes[1];
  cmd.code[2] = codes[2];
  cmd.code[3] = codes[3];

  return PROC_ASM_ERR_OK;
}

proc_init_def(pgmmus)
{
  return new pgmmus_proc_t();
}
