/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "warn_err.hpp"

#define _warnerr_core(type) \
  va_list va; \
  va_start(va, message); \
  fprintf(stderr, type " %s: ", fname); \
  if ( line != uint32_t(-1) ) \
  { \
    fprintf(stderr, "line %5d: ", line+1); \
  } \
  vfprintf(stderr, message, va); \
  fprintf(stderr, "\n"); \
  if ( l != NULL ) fprintf(stderr, "%s\n", l); \
  va_end(va)

void asthma::error(char *fname, uint32_t line, char *l, char *message, ...)
{
  _warnerr_core("[ERROR]  ");
}

void asthma::warning(char *fname, uint32_t line, char *l, char *message, ...)
{
  _warnerr_core("[WARNING]");
}
