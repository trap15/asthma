/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011        Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef BINARY_HPP_
#define BINARY_HPP_

struct binary_t
{
  binary_t();
  virtual ~binary_t();
//--- Variables
//--- Functions
  virtual void init();
  virtual void generate_output(std::map<ea_t,uint8_t> &inbin,
                               std::vector<symbol_t> &insyms,
                               std::vector<uint8_t> &outbin);
  virtual void print_options();
  virtual bool set_option(const char *opt, bool small, const char *val, bool &care_param);
  virtual int process_pseudo_op(insn_t &cmd);
};

#define fmt_init_func(f) fmt_gen_##f
#define fmt_init(f) fmt_init_func(f)()
#define fmt_init_def(f) binary_t *fmt_init(f)

#endif
