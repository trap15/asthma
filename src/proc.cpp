/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "proc.hpp"
#include "parser.hpp"

proc_t::proc_t()
{
  sprintf(name, "NULLPROC");
  cmts[0] = 0;
  case_sense = false;
  deref_style = PROC_DEREF_NUM_PARENS_REG;
  word_size = 4;
  endian = PROC_ENDIAN_BE;
  bot_addr = 0x00000000;
  top_addr = 0xFFFFFFFF;
  num_pfx = '\0';
  allowed_endians = (1 << PROC_ENDIAN_BE) | (1 << PROC_ENDIAN_LE) |
                    (1 << PROC_ENDIAN_PDP);
}

proc_t::~proc_t()
{
  regs.clear();
}

bool proc_t::assume_reg(char *reg, uintmax_t value)
{
  return false;
}

int proc_t::process_insn(insn_t &cmd)
{
  cmd.len = 0;
  return PROC_ASM_ERR_OK;
}

uint_fast16_t proc_t::get_reg(const char *reg)
{
  std::vector<reg_t>::iterator p;
  char chkreg[MAX_REGNAME_LEN];
  size_t len, i;
  len = strlen(reg);
  for ( i=0; i < len; i++ )
  {
    if(reg[i] == ',') { len = i; break; }
    if(reg[i] == ')') { len = i; break; }
    if ( !asthma::proc->case_sense )
      chkreg[i] = tolower(reg[i]);
    else
      chkreg[i] = reg[i];
  }
  chkreg[i] = 0;
  for ( p=regs.begin(); p != regs.end(); p++ )
    if ( strncmp((*p).name, chkreg, MAX_REGNAME_LEN) == 0 )
      return (*p).regno;
  return -1;
}

proc_init_def(proc)
{
  return new proc_t();
}
