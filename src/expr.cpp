/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "proc.hpp"
#include "parser.hpp"
#include "expr.hpp"

static bool verify_base_valid_char(int base, char ch)
{
  if ( base <= 10 )
  {
    if ( ch < '0' || ch > (base - 1 + '0') )
      return false;
  }
  else
  {
    if ( !((ch >= '0' && ch <= '9') ||
           (ch >= 'A' && ch <= (base - 11 + 'A')) ||
           (ch >= 'a' && ch <= (base - 11 + 'a'))) )
      return false;
  }
  return true;
}

// Wheeee
static intmax_t all_to_i(char **ostr, op_t *op=NULL, bool *valid=NULL)
{
  char *str = *ostr;
  if ( valid ) *valid = false;
  intmax_t val = 0;
  int base = -1;
  switch ( *str )
  {
    case '0': // some prefix: 0x, 0b, 0OCTAL
      str++;
      switch ( *str )
      {
        case 'x': base = 16; str++; break;
        case 'b': base =  2; str++; break;
        case '\0':
          if ( valid ) *valid = true;
          *ostr = str;
          return 0;
        case '0': case '1': case '2': case '3':
        case '4': case '5': case '6': case '7': base =  8; break;
        default:
          if ( valid ) *valid = true;
          *ostr = str;
          return 0;
      }
      break;
    case '$': if ( !verify_base_valid_char(16, *++str) ) str--; else base = 16; break;
    case '%': if ( !verify_base_valid_char( 2, *++str) ) str--; else base =  2; break;
    case '@': if ( !verify_base_valid_char( 8, *++str) ) str--; else base =  8; break;
    case 'h': case 'H': str++; if ( *str++ == '\'' ) base = 16; else str -= 2; break;
    case 'b': case 'B': str++; if ( *str++ == '\'' ) base =  2; else str -= 2; break;
    case 'q': case 'Q': str++; if ( *str++ == '\'' ) base =  8; else str -= 2; break;
    case 'd': case 'D': str++; if ( *str++ == '\'' ) base = 10; else str -= 2; break;
  }
  symbol_t *sym;
  if ( !isdigit(*str) && base == -1 && asthma::check_syms(str, &sym, ostr) )
  {
    if ( valid ) *valid = true;
    if ( op ) op->sym = sym;
    return sym->val;
  }
  if ( base == -1 )
    base = 10;
  for ( ; ; str++ )
  {
    *ostr = str;
    // Verify valid character
    if ( !verify_base_valid_char(base, *str) )
      return val;

    if ( valid ) *valid = true;
    val *= base;
    if ( base <= 10 )
      val += *str - '0';
    else
    {
      if ( *str >= '0' && *str <= '9' )
        val += *str - '0';
      else if ( *str >= 'A' && *str <= (base - 11 + 'A') )
        val += *str - 'A' + 10;
      else if ( *str >= 'a' && *str <= (base - 11 + 'a') )
        val += *str - 'a' + 10;
    }
  }
  *ostr = str;
  return val;
}

bool asthma::expr_eval(char *str, intmax_t &val, op_t *op, char **upd)
{
  intmax_t tmpval=0;
  bool valid = false;
  bool eval = true;
  int paren_level = 0;
  intmax_t *tgt;
  intmax_t vals[128]; // If you paren 128+ times, you should die anyways.
  int *nextop;
  int next_opers[128]; // Next operation to perform
  op_t trashop;
  bool newop = false;

  tgt = vals+0;
  nextop = next_opers+0;

  *tgt = *nextop = 0;

  int outtype = OPT_NUM;
  if ( asthma::proc->num_pfx != '\0' )
  {
    if ( *str == asthma::proc->num_pfx )
    {
      str++;
    }
    else // OPT_ADDR is only used if there is a prefix for numerics
    {
      if ( asthma::proc->addr_style == PROC_ADDR_PARENS )
      {
        if ( *str == '(' )
        {
          str++;
          outtype = OPT_ADDR;
        }
      }
      else
      {
        outtype = OPT_ADDR;
      }
    }
  }
  while ( eval )
  {
    while ( newop )
    {
      newop = false;
      switch ( *nextop )
      {
        case 0:  *tgt   = tmpval; break;
        case 1:  *tgt  += tmpval; break;
        case 2:  *tgt  -= tmpval; break;
        case 3:  *tgt  *= tmpval; break;
        case 4:  *tgt  /= tmpval; break;
        case 5:  *tgt >>= tmpval; break;
        case 6:  *tgt   = *tgt >= tmpval; break;
        case 7:  *tgt   = *tgt >  tmpval; break;
        case 8:  *tgt <<= tmpval; break;
        case 9:  *tgt   = *tgt <= tmpval; break;
        case 10: *tgt   = *tgt <  tmpval; break;
        case 11: *tgt   = *tgt != tmpval; break;
        case 12: tmpval =!tmpval; nextop--; newop = true; break;
        case 13: *tgt   = *tgt == tmpval; break;
        case 14: *tgt  ^= tmpval; break;
        case 15: *tgt  &= tmpval; break;
        case 16: *tgt  |= tmpval; break;
        case 17: tmpval =~tmpval; nextop--; newop = true; break;
      }
    }
    switch ( *str++ )
    {
      case '[': // Displacement or bad character? either way it's bad.
        str--;
        goto end_expr_eval;
      case '(':
      {
        char *endparen = strchr(str, ')');
        if ( endparen != NULL )
          *endparen = '\0';
        bool reg = make_reg(str, trashop);
        if ( endparen != NULL )
          *endparen = ')';
        if ( reg ) // This is a displacement
        {
          str--;
          goto end_expr_eval;
        }
        paren_level++;
        tgt++;
        nextop++;
        *nextop = 0;
        break;
      }
      case ')':
        if ( paren_level <= 0 )
        {
          // Uhh......
	        valid = false;
          goto end_expr_eval;
        }
        paren_level--;
        tmpval = *tgt;
        tgt--;
        nextop--;
        newop = true;
        break;
      case '+': *nextop = 1; break; // ADD
      case '-': *nextop = 2; break; // SUB
      case '*': *nextop = 3; break; // MUL
      case '/': *nextop = 4; break; // DIV
      case '>':
        switch ( *str++ )
        {
          case '>':       *nextop = 5; break; // SHR
          case '=':       *nextop = 6; break; // GE
          default: str--; *nextop = 7; break; // GT
        }
        break;
      case '<':
        switch ( *str++ )
        {
          case '<':       *nextop = 8; break; // SHL
          case '=':       *nextop = 9; break; // LE
          default: str--; *nextop = 10; break; // LT
        }
        break;
      case '!':
        switch ( *str++ )
        {
          case '=':       *nextop = 11; break; // NE
          default: str--; *(++nextop) = 12; break; // Logic NOT
        }
        break;
      case '=':
        switch ( *str++ )
        {
          case '=':       *nextop = 13; break; // EQ
          default: str--; *nextop = 13; break; // EQ
        }
        break;
      case '^': *nextop = 14; break; // XOR
      case '&': *nextop = 15; break; // AND
      case '|': *nextop = 16; break; // OR
      case '~': *(++nextop) = 17; break; // NOT
      case ' ': break;
      case '\0':
        eval = false;
        break;
      default: // numeric?
      {
        str--;
        bool tmpvalid;
        tmpval = all_to_i(&str, op, &tmpvalid);
        if ( !tmpvalid )
        {
          goto end_expr_eval;
        }
        valid = true;
        newop = true;
        break;
      }
    }
  }
end_expr_eval:
  if ( valid )
  {
    if ( asthma::proc->addr_style == PROC_ADDR_PARENS )
    {
      if ( *str == ')' )
        str++;
    }
  }
  op->type = outtype;
  val = *tgt;
  if ( upd ) *upd = str;
  return valid;
}
