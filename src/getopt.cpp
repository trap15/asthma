/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "getopt.hpp"

bool agetopt(int argc, char *argv[], bool (*process)(const char *opt, bool small, const char *param, bool &care_param))
{
  char *opt, *param;
  bool small;
  bool ret = false;
  int i;
  for ( i=1; i < argc; i++ )
  {
    opt = argv[i];
    if ( *opt == '-' )
    {
      opt++;
      if ( *opt == '-' )
      {
        small = false;
        opt++;
        param = strchr(opt, '=');
        if ( param != NULL )
        {
          *param++ = '\0';
        }
      }
      else
      {
        small = true;
        param = argv[++i];
      }
    }
    else
    {
      opt = NULL;
      small = false;
      param = argv[i];
    }
    bool param_cared = true;
    ret = process(opt, small, param, param_cared);
    if ( !param_cared && small ) // We didn't care about that param.
      i--;
    if ( !ret ) // stop processing
      return false;
  }
  return ret;
}

void printopt(char *big, char *small, char *desc)
{
  fprintf(stderr, " %-20s ", big);
  if ( small )
    fprintf(stderr, "| %-10s ", small);
  else
    fprintf(stderr, "             ");
  fprintf(stderr, "%-49s\n", desc);
}

void printopt_extra(char *extra)
{
  fprintf(stderr, "                                       %-40s\n", extra);
}
