/*
 *  Asthma -- Simple Target-Agnostic Assembler
 *  Copyright (C) 2011~2012   Alex Marshall "trap15" <trap15@raidenii.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "asthma.hpp"
#include "proc.hpp"
#include "srcread.hpp"
#include "warn_err.hpp"
#include "parser.hpp"
#include "expr.hpp"

std::vector<symbol_t> asthma::syms;
proc_t *asthma::proc;
int asthma::pass;
ea_t asthma::ea;
std::vector<symbol_t> asthma::nullsyms;
symbol_t asthma::ea_sym("$", 0);
std::map<ea_t,uint8_t> asthma::curbin;
char asthma::lastsymname[MAX_SYMNAME_LEN];
FILE *asthma::fp;
char *asthma::file;
char *asthma::line_orig;
uint32_t asthma::line;
std::vector<asthma::context_info> asthma::context_lines;
uint32_t asthma::context_depth;
uint32_t asthma::last_tell;
bool asthma::parsing_off;
binary_t *asthma::outmeth;

//------------------------------------------------------------------------------
bool asthma::parse(char *file, std::map<ea_t,uint8_t> &outbin, ea_t start_addr)
{
  FILE *fp = fopen(file, "rb");

  bool error = false;
  for ( asthma::pass=0; !error && asthma::pass < LAST_PASS+1; asthma::pass++ )
  {
    asthma::context_lines.clear();
    asthma::context_depth = 0;
    printf("pass %d...\n", asthma::pass);
    fseek(fp, 0, SEEK_SET);
    asthma::curbin.clear();
    asthma::parsing_off = false;
    asthma::outmeth->init();

    // kill invalid symbols
    for ( std::vector<symbol_t>::iterator p=asthma::syms.begin(); p != asthma::syms.end(); p++ )
    {
      if ( !(*p).valid )
        p = asthma::syms.erase(p)-1;
    }

    error = pass_single(file, fp, asthma::curbin, start_addr);
  }

  for ( std::map<ea_t,uint8_t>::iterator p=asthma::curbin.begin(); p != asthma::curbin.end(); p++ )
  {
    ea_t tmpaddr = (*p).first;
    uint8_t tmpval = (*p).second;
    outbin[tmpaddr] = tmpval;
  }

  fclose(fp);
  return error;
}

//------------------------------------------------------------------------------
bool asthma::pass_single(char *file, FILE *fp, std::map<ea_t,uint8_t> &binary, ea_t start_addr)
{
  bool error = false;
  char *line;
  uint32_t linelen;
  ea = start_addr;
  insn_t insn;
  int errcode;
  bool no_arg;

  asthma::fp = fp;
  asthma::file = file;
  for ( asthma::line=0; !error && (linelen = asthma::read_line(&line, asthma::proc->cmts, fp)) != uint32_t(-1); asthma::line++ )
  {
    asthma::last_tell = ftell(fp);
    asthma::line_orig = line;
    if ( linelen == 0 )
      continue;
    //- Handle symbol definition:
    char *col = strchr(line, ':');
    char *spc = strchr(line, ' ');
    if ( col != NULL )
    {
      *col = '\0';
      if ( spc != NULL )
      {
        if ( spc < col )
        {
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Invalid symbol '%s' for label.", line);
          error = true;
          goto end_line_parse;
        }
      }

      // TODO: Handling symbol stuff while in a context
      if ( !asthma::parsing_off )
      {
        if ( !asthma::add_symbol(line, asthma::ea) )
        {
          error = true;
          goto end_line_parse;
        }
      }

      *col = ':';
      if ( *(col+1) == ' ' )
        line = col + 2;
      else
        line = col + 1;
    }
    if ( *line == '\0' ) // no instruction
      continue;
    //- Handle parsing instruction:
    //-- Handle reading mnemonic:
    spc = strchr(line, ' ');
    no_arg = false;
    if ( spc == NULL )
    {
      no_arg = true;
      spc = line + strlen(line);
    }
    *spc = 0;
    if ( !asthma::proc->case_sense )
    {
      unsigned int i;
      for ( i=0; i < strlen(line) && i < MAX_MNEM_LEN-1; i++ )
        insn.mnem[i] = tolower(line[i]);
      for ( ; i < MAX_MNEM_LEN-1; i++ )
        insn.mnem[i] = 0;
    }
    else
      strncpy(insn.mnem, line, MAX_MNEM_LEN);
    insn.mnem[MAX_MNEM_LEN-1] = 0;
    if ( !no_arg )
    {
      *spc = ' ';
      line = spc + 1;
    }
    else
      line = spc;
    //-- Handle reading arguments:
    for ( insn.opcnt=0; *line != '\0'; insn.opcnt++ )
    {
      insn.op.resize(insn.opcnt+1);
      char *com;
      bool last_arg = false;
      int pbracks = 0, sbracks = 0;
      for ( com = line; ; com++ )
      {
        if ( *com == '(' ) pbracks++;
        if ( *com == '[' ) sbracks++;
        if ( *com == ')' ) pbracks--;
        if ( *com == ']' ) sbracks--;
        if ( *com == ',' && pbracks == 0 && sbracks == 0 ) break;
        if ( *com == '\0' ) break;
      }
      if ( *com == '\0' )
      {
        com = line + strlen(line);
        last_arg = true;
      }
      *com = 0;
      strncpy(insn.op[insn.opcnt].argstr, line, MAX_ARG_LEN);
      insn.op[insn.opcnt].argstr[MAX_ARG_LEN-1] = 0;
      if ( asthma::make_reg(insn.op[insn.opcnt].argstr, insn.op[insn.opcnt]) ) ;
      else if ( asthma::make_disp(insn.op[insn.opcnt].argstr, insn.op[insn.opcnt]) ) ;
      else if ( asthma::make_str(insn.op[insn.opcnt].argstr, insn.op[insn.opcnt]) ) ;
      else if ( asthma::make_num(insn.op[insn.opcnt].argstr, insn.op[insn.opcnt]) ) ;
      else
      {
        if ( insn.mnem[0] != '.' ) // Don't throw argument errors on pseudo-ops. They'll handle it.
        {
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Unknown argument %d '%s'.", insn.opcnt, insn.op[insn.opcnt].argstr);
          error = true;
          goto end_line_parse;
        }
        else
        {
          insn.op[insn.opcnt].type = OPT_UNK;
        }
      }
      if ( !last_arg )
      {
        *com = ',';
        line = com + 1;
      }
      else
        line = com;
    }
    insn.ea = asthma::ea;
    //- Convert insn_t to opcode
    if ( insn.mnem[0] == '.' )
    {
      errcode = asthma::process_pseudo_op(insn);
      if ( errcode == PROC_ASM_ERR_BAD_MNEM )
        errcode = asthma::outmeth->process_pseudo_op(insn);
    }
    else if ( !asthma::parsing_off )
      errcode = asthma::proc->process_insn(insn);
    else
      errcode = PROC_ASM_ERR_OK;
    if ( errcode != PROC_ASM_ERR_OK )
    {
      switch ( errcode )
      {
        default:
        case PROC_ASM_ERR_UNK:
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Unknown error %d", errcode);
          break;
        case PROC_ASM_ERR_MISC:
          break;
        case PROC_ASM_ERR_FILE_NOT_FOUND:
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "File not found", errcode);
          break;
        case PROC_ASM_ERR_UNIMP:
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Unimplemented mnemonic '%s'", insn.mnem);
          break;
        case PROC_ASM_ERR_BAD_MNEM:
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Bad mnemonic '%s'", insn.mnem);
          break;
        case PROC_ASM_ERR_TOO_MANY_OPS:
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Too many operands");
          break;
        case PROC_ASM_ERR_TOO_FEW_OPS:
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Too few operands");
          break;
        case PROC_ASM_ERR_OUT_OF_RANGE:
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Operand out of range");
          break;
        case PROC_ASM_ERR_EA_INVALID:
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Effective address invalid");
          break;
        case PROC_ASM_ERR_BAD_OP_TYPE:
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Bad type on operands");
          break;
        case PROC_ASM_ERR_BAD_OP0_TYPE:
        case PROC_ASM_ERR_BAD_OP1_TYPE:
        case PROC_ASM_ERR_BAD_OP2_TYPE:
        case PROC_ASM_ERR_BAD_OP3_TYPE:
        case PROC_ASM_ERR_BAD_OP4_TYPE:
        case PROC_ASM_ERR_BAD_OP5_TYPE:
        case PROC_ASM_ERR_BAD_OP6_TYPE:
        case PROC_ASM_ERR_BAD_OP7_TYPE:
        case PROC_ASM_ERR_BAD_OP8_TYPE:
        case PROC_ASM_ERR_BAD_OP9_TYPE:
        case PROC_ASM_ERR_BAD_OP10_TYPE:
        case PROC_ASM_ERR_BAD_OP11_TYPE:
        case PROC_ASM_ERR_BAD_OP12_TYPE:
        case PROC_ASM_ERR_BAD_OP13_TYPE:
        case PROC_ASM_ERR_BAD_OP14_TYPE:
        case PROC_ASM_ERR_BAD_OP15_TYPE:
        {
          int op = errcode - PROC_ASM_ERR_BAD_OP0_TYPE;
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Bad type on operand %d '%d'", op, insn.op[op].type);
          break;
        }
        case PROC_ASM_ERR_BAD_OP_VAL:
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Bad value on operands");
          break;
        case PROC_ASM_ERR_BAD_OP0_VAL:
        case PROC_ASM_ERR_BAD_OP1_VAL:
        case PROC_ASM_ERR_BAD_OP2_VAL:
        case PROC_ASM_ERR_BAD_OP3_VAL:
        case PROC_ASM_ERR_BAD_OP4_VAL:
        case PROC_ASM_ERR_BAD_OP5_VAL:
        case PROC_ASM_ERR_BAD_OP6_VAL:
        case PROC_ASM_ERR_BAD_OP7_VAL:
        case PROC_ASM_ERR_BAD_OP8_VAL:
        case PROC_ASM_ERR_BAD_OP9_VAL:
        case PROC_ASM_ERR_BAD_OP10_VAL:
        case PROC_ASM_ERR_BAD_OP11_VAL:
        case PROC_ASM_ERR_BAD_OP12_VAL:
        case PROC_ASM_ERR_BAD_OP13_VAL:
        case PROC_ASM_ERR_BAD_OP14_VAL:
        case PROC_ASM_ERR_BAD_OP15_VAL:
        {
          int op = errcode - PROC_ASM_ERR_BAD_OP0_VAL;
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Bad value on operand %d", op);
          break;
        }
      }
      error = true;
      goto end_line_parse;
    }
    if ( !asthma::parsing_off )
    {
      for ( int i=0; i < insn.len; i++ )
      {
        if ( asthma::pass == LAST_PASS )
          binary[asthma::ea] = insn.code[i];
        asthma::ea++;
        if ( asthma::ea < asthma::proc->bot_addr ||
             asthma::ea > asthma::proc->top_addr )
        {
          asthma::error(asthma::file, asthma::line, asthma::line_orig, "Effective address invalid");
          error = true;
          goto end_line_parse;
        }
      }
    }
end_line_parse:
    free(asthma::line_orig);
  }
  return error;
}

//------------------------------------------------------------------------------
bool asthma::make_reg(char *str, op_t &op, char **upd)
{
  op.reg = asthma::proc->get_reg(str);
  int len;
  if ( op.reg == uint_fast16_t(-1) )
    return false;
  len = strlen(str);
  for ( int i=0; i < len; i++ )
  {
    if(str[i] == ',') { len = i; break; }
    if(str[i] == ')') { len = i; break; }
  }
  if ( upd ) *upd = str+len;
  op.type = OPT_REG;
  return true;
}

bool asthma::make_disp(char *str, op_t &op, char **upd)
{
  op_type_t type = OPT_DISP;
  bool ret = false;
  bool numval = false;
  int bracks = 1;
  switch ( asthma::proc->deref_style )
  {
    case PROC_DEREF_NUM_PARENS_REG:
      bracks = 0;
    case PROC_DEREF_NUM_BRACKS_REG:
    {
      char *clparen = strchr(str, bracks ? ']' : ')');
      if ( clparen == NULL )
        goto disp_end;

      if ( *str != (bracks ? '[' : '(') )
        numval = asthma::make_num(str, op, &str);
      else
        op.value = 0;

      if ( !numval )
      {
        if ( *str == '-' )
        {
          type = OPT_PREDEC;
          str++;
        }
        else if ( *str == '+' )
        {
          type = OPT_PREINC;
          str++;
        }
      }

      if ( *str++ != (bracks ? '[' : '(') )
        goto disp_end;
      op.disp = op.value;

      *clparen = '\0';
      if ( !asthma::make_reg(str, op, &str) )
        goto disp_end;
      *clparen = bracks ? ']' : ')';
      if ( *str == ',' )
      {
        int oreg = op.reg;
        str++;
        if ( !asthma::make_reg(str, op, &str) )
          goto disp_end;
        type = OPT_PHRASE;
        op.reg2 = op.reg;
        op.reg = oreg;
      }
      if ( !numval )
      {
        if ( *str == '-' )
        {
          type = OPT_POSTDEC;
        }
        else if ( *str == '+' )
        {
          type = OPT_POSTINC;
        }
      }
      str = clparen + 1;

      ret = true;
      break;
    }
    case PROC_DEREF_BRACK_NUM_COM_REG_BRACK:
      // Unimplemented
      break;
    case PROC_DEREF_BRACK_REG_COM_NUM_BRACK:
      // Unimplemented
      break;
  }
disp_end:
  if ( upd && ret ) *upd = str;
  if ( ret )
    op.type = type;
  return ret;
}

bool asthma::make_str(char *str, op_t &op, char **upd)
{
  if ( *str != '"' )
    return false;
  str++;
  size_t i;
  size_t len = strlen(str);
  op.reg = 0;
  for ( i=0; i < len; i++ )
  {
    char c = str[i];
    if ( c == '\\' )
    {
      switch ( str[++i] )
      {
        case 'n': c = '\n'; break;
        case '"': c = '"'; break;
        case '\'': c = '\''; break;
        case 't': c = '\t'; break;
      }
    }
    else if ( c == '"' )
    {
      len = 0;
      c = '\0';
    }
    op.str[i] = c;
  }
  op.str[i-1] = '\0';
  if ( upd ) *upd = str + i;
  op.type = OPT_STR;
  return true;
}

bool asthma::make_num(char *str, op_t &op, char **upd, bool forceupd)
{
  bool valid;
  intmax_t val;
  valid = asthma::expr_eval(str, val, &op, &str);
  op.reg = 0;
  op.value = val;
  if ( upd && (valid || forceupd) ) *upd = str;
  if ( !valid )
    return false;
  // op type is already set by the expression evaluator
  return true;
}


//------------------------------------------------------------------------------
bool asthma::add_symbol(char *str, intmax_t val)
{
  if ( (*str == '.' || *str == '$') && !asthma::valid_sym_char(*(str+1)) ) // EA symbol
  {
    asthma::error(asthma::file, asthma::line, asthma::line_orig, "Reserved symbol name.");
    return false;
  }
  if ( asthma::clean_sym(str) != uint32_t(-1) )
  {
    asthma::error(asthma::file, asthma::line, asthma::line_orig, "Invalid symbol name.");
    return false;
  }
  char symstr[MAX_SYMNAME_LEN];
  if ( *str == '.' )
    snprintf(symstr, MAX_SYMNAME_LEN, "%s%s", asthma::lastsymname, str);
  else
    snprintf(symstr, MAX_SYMNAME_LEN, "%s", str);
  if ( asthma::check_sym_exist(symstr, val) )
  {
    symbol_t *testsym;
    asthma::check_syms(symstr, &testsym);
    asthma::error(asthma::file, asthma::line, asthma::line_orig, "Symbol already exists %d %d.", val, testsym->val);
    return false;
  }
  if ( *str != '.' )
    strncpy(asthma::lastsymname, symstr, MAX_SYMNAME_LEN);
  asthma::syms.push_back(symbol_t(symstr, val));
  return true;
}

bool asthma::check_syms(char *str, symbol_t **sym, char **upd)
{
  std::vector<symbol_t>::iterator p;
  char ch;
  if ( sym ) *sym = NULL;
  if ( *str == '\0' )
    return false;
  if ( (*str == '.' || *str == '$') && !asthma::valid_sym_char(*(str+1)) ) // EA symbol
  {
    asthma::ea_sym.set_val(asthma::ea);
    if ( sym ) *sym = &asthma::ea_sym;
    if ( upd ) *upd = str + 1;
    return true;
  }
  char symline[MAX_SYMNAME_LEN];
  if ( *str == '.' )
    snprintf(symline, MAX_SYMNAME_LEN, "%s%s", asthma::lastsymname, str);
  else
    snprintf(symline, MAX_SYMNAME_LEN, "%s", str);
  asthma::clean_sym(symline, ch);
  for ( p=asthma::syms.begin(); p != asthma::syms.end(); p++ )
  {
    if ( strncmp((*p).name, symline, MAX_SYMNAME_LEN) == 0 )
    {
      if ( (*p).ext ) // If it's an external, it is assembled as EA.
        (*p).val = asthma::ea;
      if ( sym ) *sym = &(*p);
      if ( upd ) *upd = str + strlen(str);
      return true;
    }
  }
  if ( asthma::pass != LAST_PASS )
  {
    // TODO: Relocation crap
    symbol_t newnullsym(symline, asthma::ea);
    newnullsym.valid = newnullsym.val_valid = false;
    asthma::nullsyms.push_back(newnullsym);
    if ( sym ) *sym = &(*asthma::nullsyms.rbegin());
    if ( upd ) *upd = str + strlen(str);
    return true;
  }
  return false;
}

bool asthma::check_sym_exist(char *str, intmax_t val)
{
  std::vector<symbol_t>::iterator p;
  char ch;
  if ( *str == '\0' )
    return false;
  if ( (*str == '.' || *str == '$') && !asthma::valid_sym_char(*(str+1)) ) // EA symbol
    return true;
  char symline[MAX_SYMNAME_LEN];
  if ( *str == '.' )
    snprintf(symline, MAX_SYMNAME_LEN, "%s%s", asthma::lastsymname, str);
  else
    snprintf(symline, MAX_SYMNAME_LEN, "%s", str);
  asthma::clean_sym(symline, ch);
  for ( p=asthma::syms.begin(); p != asthma::syms.end(); p++ )
  {
    if ( (*p).val == val || // Doesn't matter if the symbol is the same value
         !(*p).valid ) // Doesn't matter if the symbol is invalid
      continue;
    if ( strncmp((*p).name, symline, MAX_SYMNAME_LEN) == 0 )
    {
      return true;
    }
  }
  return false;
}

bool asthma::valid_sym_char(char ch)
{
  if ( isalnum(ch) ||
       (ch == '.') ||
       (ch == '_'))
    return true;
  return false;
}

uint32_t asthma::clean_sym(char *str, char &ch)
{
  uint32_t i;
  uint32_t len = strlen(str);
  ch = *str;
  if ( isdigit(*str) )
    return 0;
  for ( i=0; i < len; i++ )
  {
    if ( !asthma::valid_sym_char(str[i]) )
    {
      ch = str[i];
      str[i] = 0;
      return i;
    }
  }
  return -1;
}

uint32_t asthma::clean_sym(char *str)
{
  char trash;
  return asthma::clean_sym(str, trash);
}
