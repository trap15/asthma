DEBUG    ?= 0

PREFIX   ?= /usr/local

OBJECTS   = src/asthma.o src/srcread.o src/warn_err.o src/getopt.o \
            src/parser.o src/expr.o \
            src/pseudo.o \
            src/proc.o \
              src/procs/v810.o \
              src/procs/i4004.o \
              src/procs/ppc.o \
              src/procs/reco.o \
              src/procs/pgmmus.o \
              src/procs/mb8840.o \
            src/binary.o \
              src/fmts/srec.o \
              src/fmts/aout.o

OUTPUT    = asthma
LIBS     += -lm
LDFLAGS  += $(LIBS)
CFLAGS   += -Wall -Isrc

ifeq ($(DEBUG),1)
CFLAGS   += -g -O0 -DDEBUG=1
LDFLAGS  += -g
else
CFLAGS   += -O3
endif

CXXFLAGS  = -Wno-write-strings -fno-exceptions $(CFLAGS)

.PHONY: all clean install

all: $(OUTPUT)

src/asthma.cpp:   src/asthma.hpp src/parser.hpp src/warn_err.hpp src/proc.hpp \
		    src/binary.hpp src/getopt.hpp \
		    src/proclist.hpp src/fmtlist.hpp \
		    src/procs/procs.hpp
src/binary.cpp:   src/asthma.hpp src/parser.hpp src/proc.hpp src/binary.hpp
src/expr.cpp:     src/asthma.hpp src/parser.hpp src/proc.hpp src/expr.hpp
src/getopt.cpp:   src/asthma.hpp src/getopt.hpp
src/parser.cpp:   src/asthma.hpp src/parser.hpp src/warn_err.hpp src/proc.hpp \
                    src/srcread.hpp src/expr.hpp
src/proc.cpp:     src/asthma.hpp src/proc.hpp src/parser.hpp
src/pseudo.cpp:   src/asthma.hpp src/parser.hpp src/warn_err.hpp src/proc.hpp \
                    src/srcread.hpp src/proclist.hpp
src/srcread.cpp:  src/asthma.hpp src/srcread.hpp
src/warn_err.cpp: src/asthma.hpp src/warn_err.hpp

src/procs/v810.cpp:   src/asthma.hpp src/parser.hpp src/proc.hpp
src/procs/i4004.cpp:  src/asthma.hpp src/parser.hpp src/proc.hpp
src/procs/ppc.cpp:    src/asthma.hpp src/parser.hpp src/proc.hpp
src/procs/reco.cpp:   src/asthma.hpp src/parser.hpp src/proc.hpp
src/procs/pgmmus.cpp: src/asthma.hpp src/parser.hpp src/proc.hpp
src/procs/mb8840.cpp: src/asthma.hpp src/parser.hpp src/proc.hpp

src/fmts/srec.cpp: src/asthma.hpp src/parser.hpp src/warn_err.hpp src/proc.hpp \
                     src/binary.hpp src/getopt.hpp
src/fmts/aout.cpp: src/asthma.hpp src/parser.hpp src/warn_err.hpp src/proc.hpp \
                     src/binary.hpp src/getopt.hpp

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<
%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	$(CXX) $(LDFLAGS) -o $(OUTPUT) $(OBJECTS)
clean:
	rm -f $(OUTPUT) $(OBJECTS)
release: clean

install: $(OUTPUT)
	install -m 0755 $(OUTPUT) $(PREFIX)/bin
