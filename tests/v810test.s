	.cpu	V810

	.global	start

start:
	sch0bsu
	sch0bsd
	sch1bsu
	sch1bsd
	orbsu
	andbsu
	xorbsu
	movbsu
	ornbsu
	andnbsu
	xornbsu
	notbsu

	mov	lp, r14
	mov	-$10, r10
	shr	lp, r14
	shr	@17, r10
	sar	lp, r14
	sar	%0101, r10
	shl	lp, r14
	shl	0b1010, r10
	cmp	lp, r14
	cmp	017, r10
	add	lp, r14
	add	1, r10

	sub	r10, r14
	mul	r10, r14
	div	r10, r14
	mulu	r10, r14
	divu	r10, r14
	or	r10, r14
	and	r10, r14
	xor	r10, r14
	not	r10, r14

	setf	nz, r6

	ld.w	16+24[sp], r14
	ld.h	16+22[sp], r10
	ld.b	[sp], lp
.crap:
	in.w	0x600[r0], r14
	in.h	0x600[r0], r10
	in.b	0x600[r0], lp

	st.w	r14, 16+24[sp]
	st.h	r10, 16+22[sp]
	st.b	lp, [sp]
	out.w	r14, 0x600[r0]
	out.h	r10, 0x600[r0]
	out.b	lp, 0x600[r0]

	jal	barf
	jr	.crap
	jmp	[lp]

	trap	15

	nop

barf:
	bv	h'9c
	bc	H'9c
	bz	barf
	be	b'10101010
	bnh	q'76
.crap:
	bn	d'50
	br	0x9c
	blt	.crap
	ble	0x9c
	bnv	$+4
	bnc	0x9c
	bnz	0x9c
	bne	0x9c
	bh	0x9c
	bp	0x9c
	bge	0x9c

	movea	0x4F00, r10, r14
	addi	0x40F0, r10, r14
	ori	0x400F, r10, r14
	andi	0x4500, r10, r14
	xori	0x4050, r10, r14
	movhi	0x4005, r10, r14

	stsr	eipc, r5
	ldsr	r5, eipc
