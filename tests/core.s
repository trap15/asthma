	.cpu	NONE
	.org	0x20

.rept 4
	.byte	9
	.dc.b	8
	.db	7
.end

	.dc.h	0xBEEF, 0xFEED

.if 0
	.dc.h	0xDEAD
.elseif 1
	.dc.h	0xBABE
.elseif 1
	.dc.h	0xAAAA
.end

.if 1
.if 1
	.dc.h	0xF00D
.elseif 0
	.dc.h	0x0BAD
.end
.if 0
	.dc.h	0xBABE
.elseif 0
	.dc.h	0xCAFE
.else
	.dc.h	0xA5A5
.else
	.dc.h	0xBADF
.end
.end

	.space	1
	.space	4, 0x55
	.ds.b	4, 0xAA
	.endian little
	.ds.h	2, 0xDEAD
	.ds.w	1, 0xDEADBEEF

	.align	4

	.set	foo, 0xFEED

	.dc.h	foo

	.set	foo, 0xB00B

	.dc.h	foo

	.align	4

	.incbin	"core.s"

	.ascii	"barkbark", "meow"
	.asciz	"moocow", "beelzebub"

	.cpu	V810

	.extern	cowduck

.rept 4
	halt
.rept 2
	reti
.end
.end

	mov	-$10, r10
	shr	@17, r10
	sar	%0101, r10
	shl	0b1010, r10
	cmp	017, r10
	bv	h'9c
	bc	H'9c
	bz	foo
	be	b'10101010
	bnh	q'76
	bn	d'50
	bnv	$+4
	bne	cowduck
