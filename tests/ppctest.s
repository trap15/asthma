	.cpu	PPC

	.org	0

start:
	add	r5, r6, r7
	add.	r5, r6, r7
	addo	r5, r6, r7
	addo.	r5, r6, r7
	addc	r5, r6, r7
	addc.	r5, r6, r7
	addco	r5, r6, r7
	addco.	r5, r6, r7
	adde	r5, r6, r7
	adde.	r5, r6, r7
	addeo	r5, r6, r7
	addeo.	r5, r6, r7
	divw	r5, r6, r7
	divw.	r5, r6, r7
	divwo	r5, r6, r7
	divwo.	r5, r6, r7
	divwu	r5, r6, r7
	divwu.	r5, r6, r7
	divwuo	r5, r6, r7
	divwuo.	r5, r6, r7
	eciwx	r5, r6, r7
	ecowx	r5, r6, r7

moocow:
	addi	r5, r6, -0x7000
	addic	r5, r6, -0x8000
	addic.	r5, r6, 0x7FFF
	addis	r5, r6, 0x10

baz:
	addme	r5, r6
	addme.	r5, r6
	addmeo	r5, r6
	addmeo.	r5, r6
	addze	r5, r6
	addze.	r5, r6
	addzeo	r5, r6
	addzeo.	r5, r6

bar:
	and	r5, r6, r7
	and.	r5, r6, r7
	andc	r5, r6, r7
	andc.	r5, r6, r7
	eqv	r5, r6, r7
	eqv.	r5, r6, r7

foo:
	andi.	r5, r6, 0x8FFF
	andis.	r5, r6, 0xFFFF

	b	start
	ba	bar
	bl	baz
	bla	moocow

	bc	0x3, 0x8, bar
	bca	0x5, 0x8, baz
	bcl	0x8, 0x8, moocow
	bcla	0xA, 0x8, foo

	bcctr	0x8, 0x9
	bcctrl	0x8, 0x9
	bclr	0x8, 0x9
	bclrl	0x8, 0x9

	cmp	cr4, 0, r5, r6
	cmpl	cr4, 0, r5, r6

	cmpi	cr4, 0, r5, -0x8000
	cmpli	cr4, 0, r5, 0x9FFF

	cntlzw	r5, r6
	cntlzw.	r5, r6
	extsb	r5, r6
	extsb.	r5, r6
	extsh	r5, r6
	extsh.	r5, r6

	crand	0x1, 0x2, 0x3
	crandc	0x1, 0x2, 0x3
	creqv	0x1, 0x2, 0x3
	crnand	0x1, 0x2, 0x3
	crnor	0x1, 0x2, 0x3
	cror	0x1, 0x2, 0x3
	crorc	0x1, 0x2, 0x3
	crxor	0x1, 0x2, 0x3

	dcba	r5, r6
	dcbf	r5, r6
	dcbi	r5, r6
	dcbst	r5, r6
	dcbt	r5, r6
	dcbtst	r5, r6
	dcbz	r5, r6
	icbi	r5, r6

	eieio
	isync

	fabs	fr5, fr6
	fabs.	fr5, fr6
	fctiw	fr5, fr6
	fctiw.	fr5, fr6
	fctiwz	fr5, fr6
	fctiwz.	fr5, fr6
	fmr	fr5, fr6
	fmr.	fr5, fr6
	fnabs	fr5, fr6
	fnabs.	fr5, fr6
	fneg	fr5, fr6
	fneg.	fr5, fr6
	fres	fr5, fr6
	fres.	fr5, fr6
	frsp	fr5, fr6
	frsp.	fr5, fr6
	frsqrte	fr5, fr6
	frsqrte. fr5, fr6
	fsqrt	fr5, fr6
	fsqrt.	fr5, fr6
	fsqrts	fr5, fr6
	fsqrts.	fr5, fr6

	fadd	fr5, fr6, fr7
	fadd.	fr5, fr6, fr7
	fadds	fr5, fr6, fr7
	fadds.	fr5, fr6, fr7
	fdiv	fr5, fr6, fr7
	fdiv.	fr5, fr6, fr7
	fdivs	fr5, fr6, fr7
	fdivs.	fr5, fr6, fr7
	fsub	fr5, fr6, fr7
	fsub.	fr5, fr6, fr7
	fsubs	fr5, fr6, fr7
	fsubs.	fr5, fr6, fr7

	fcmpo	cr5, fr5, fr6
	fcmpu	cr5, fr5, fr6

	fmadd	fr5, fr6, fr7, fr8
	fmadd.	fr5, fr6, fr7, fr8
	fmadds	fr5, fr6, fr7, fr8
	fmadds.	fr5, fr6, fr7, fr8
	fmsub	fr5, fr6, fr7, fr8
	fmsub.	fr5, fr6, fr7, fr8
	fmsubs	fr5, fr6, fr7, fr8
	fmsubs.	fr5, fr6, fr7, fr8
	fnmadd	fr5, fr6, fr7, fr8
	fnmadd.	fr5, fr6, fr7, fr8
	fnmadds	fr5, fr6, fr7, fr8
	fnmadds. fr5, fr6, fr7, fr8
	fnmsub	fr5, fr6, fr7, fr8
	fnmsub.	fr5, fr6, fr7, fr8
	fnmsubs	fr5, fr6, fr7, fr8
	fnmsubs. fr5, fr6, fr7, fr8
	fsel	fr5, fr6, fr7, fr8
	fsel.	fr5, fr6, fr7, fr8

	fmul	fr5, fr6, fr7
	fmul.	fr5, fr6, fr7
	fmuls	fr5, fr6, fr7
	fmuls.	fr5, fr6, fr7

	lbz	r5, 0x07FC(r6)
	lbzu	r5, -0x8000(r6)
	lbzux	r5, r6, r7
	lbzx	r5, r6, r7
	lfd	fr5, 0x07FC(r6)
	lfdu	fr5, -0x8000(r6)
	lfdux	fr5, r6, r7
	lfdx	fr5, r6, r7
	lfs	fr5, 0x07FC(r6)
	lfsu	fr5, -0x8000(r6)
	lfsux	fr5, r6, r7
	lfsx	fr5, r6, r7
	lha	r5, 0x07FC(r6)
	lhau	r5, -0x8000(r6)
	lhaux	r5, r6, r7
	lhax	r5, r6, r7
	lhbrx	r5, r6, r7
	lhz	r5, 0x07FC(r6)
	lhzu	r5, -0x8000(r6)
	lhzux	r5, r6, r7
	lhzx	r5, r6, r7
	lmw	r5, -0x8000(r6)
	lswi	r5, r6, 0x12
	lswx	r5, r6, r7
	lwarx	r5, r6, r7
	lwbrx	r5, r6, r7
	lwz	r5, 0x07FC(r6)
	lwzu	r5, -0x8000(r6)
	lwzux	r5, r6, r7
	lwzx	r5, r6, r7

	mcrf	cr4, cr5
	mcrfs	cr4, cr5
	mcrxr	cr4
	mfcr	r6
	mffs	fr5
	mffs.	fr5
	mfmsr	r5
	mfspr	r5, xer
	mfsr	r5, 8
	mfsrin	r5, r6
	mftb	r5, tbl
	mtcrf	0x7F, r5
	mtfsb0	0x10
	mtfsb0.	0x10
	mtfsb1	0x10
	mtfsb1.	0x10
	mtfsf	0xFF, fr1
	mtfsf.	0xFF, fr1
	mtfsfi	cr3, 0xF
	mtfsfi.	cr3, 0xF

	mtmsr	r18
	mtspr	xer, r18
	mtsr	8, r18
	mtsrin	r18, r19

	mulhw	r15, r16, r17
	mulhw.	r15, r16, r17
	mulhwu	r15, r16, r17
	mulhwu.	r15, r16, r17
	mulli	r15, r16, -0x4F4F
	mullw	r15, r16, r17
	mullw.	r15, r16, r17
	mullwo	r15, r16, r17
	mullwo.	r15, r16, r17

	nand	r15, r16, r17
	nand.	r15, r16, r17
	neg	r15, r16
	neg.	r15, r16
	nego	r15, r16
	nego.	r15, r16
	nor	r15, r16, r17
	nor.	r15, r16, r17
	or	r15, r16, r17
	or.	r15, r16, r17
	orc	r15, r16, r17
	orc.	r15, r16, r17
	ori	r15, r16, 0x8FFF
	oris	r15, r16, 0x8FFF

	rfi

	rlwimi	r15, r16, 0x10, 0x14, 0x1F
	rlwimi.	r15, r16, 0x10, 0x14, 0x1F
	rlwinm	r15, r16, 0x10, 0x14, 0x1F
	rlwinm.	r15, r16, 0x10, 0x14, 0x1F
	rlwnm	r15, r16, 0x10, 0x14, 0x1F
	rlwnm.	r15, r16, 0x10, 0x14, 0x1F

	sc

	slw	r15, r16, r17
	slw.	r15, r16, r17
	sraw	r15, r16, r17
	sraw.	r15, r16, r17
	srawi	r15, r16, 17
	srawi.	r15, r16, 17
	srw	r15, r16, r17
	srw.	r15, r16, r17

	stb	r15, 0x7FFF(r0)
	stbu	r15, 0x7000(r16)
	stbux	r15, r16, r17
	stbx	r15, r16, r17
	stfd	fr15, -0x8000(r0)
	stfdu	fr15, -0x7FFF(r16)
	stfdux	fr15, r16, r17
	stfdx	fr15, r16, r17
	stfiwx	fr15, r16, r17
	stfs	fr15, -0x8000(r0)
	stfsu	fr15, -0x7FFF(r16)
	stfsux	fr15, r16, r17
	stfsx	fr15, r16, r17
	sth	r15, 0x7FFF(r0)
	sthbrx	r15, r16, r17
	sthu	r15, 0x7000(r16)
	sthux	r15, r16, r17
	sthx	r15, r16, r17
	stmw	r15, 0x7000(r16)
	stw	r15, 0x7FFF(r0)
	stwbrx	r15, r16, r17
	stwcx.	r15, r16, r17
	stwu	r15, 0x7000(r16)
	stwux	r15, r16, r17
	stwx	r15, r16, r17

	subf	r15, r16, r17
	subf.	r15, r16, r17
	subfo	r15, r16, r17
	subfo.	r15, r16, r17
	subfc	r15, r16, r17
	subfc.	r15, r16, r17
	subfco	r15, r16, r17
	subfco.	r15, r16, r17
	subfe	r15, r16, r17
	subfe.	r15, r16, r17
	subfeo	r15, r16, r17
	subfeo.	r15, r16, r17
	subfic	r15, r16, -0x7FFF
	subfme	r15, r16
	subfme.	r15, r16
	subfmeo	r15, r16
	subfmeo. r15, r16
	subfze	r15, r16
	subfze.	r15, r16
	subfzeo	r15, r16
	subfzeo. r15, r16

	sync
	tlbia
	tlbie	r15
	tlbsync

	xor	r15, r16, r17
	xor.	r15, r16, r17
	xori	r15, r16, 0x9FFF
	xoris	r15, r16, 0x9FFF
