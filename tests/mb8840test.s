	.cpu	MB8840

	.org	0

start:
	tath
	tatl
	tas
	tay
	tsa
	ttha
	ttla
	tya
	xx

	l
	ls
	st
	stdc
	stic
	sts
	x
	xd	2
	xyd	6

	cla
	li	14
	lxi	4
	lyi	12

	adc
	ai	9
	and
	c
	ci	10
	cyi	11
	daa
	das
	dca
	dcm
	dcy
	eor
	ica
	icm
	icy
	neg
	or
	rol
	ror
	sbc

	rbit	3
	sbit	2
	tba	1
	tbit	0

	en	254
	dis	255

	in
	ink
	out
	outp
	rstd	3
	rstr
	setd	2
	setr
	tstd	1
	tstr

short_addr:
	call	long_addr
	jmp	short_addr
	jpa	0
	jpl	long_addr

	rti
	rts

	rstc
	setc
	tstc
	tsti
	tsts
	tstv
	tstz

	nop

	.org	0x200
long_addr:





