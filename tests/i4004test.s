	.cpu	I4004

	.org	0

start:
	nop
	jcn	0, .
	jcn	1, .
	jcn	2, .
	jcn	3, .
	jcn	4, .
	jcn	5, .
	jcn	6, .
	jcn	7, .
	jcn	8, .
	jcn	9, .
	jcn	10, .
	jcn	11, .
	jcn	12, .
	jcn	13, .
	jcn	14, .
	jcn	15, .
	fim	p0, $00
	src	p0
	fin	p1
	jin	p1
	jun	farjmp
	jms	farsub
	inc	r0
	isz	r0, shortjmp
	add	r1
	sub	r2
	ld	r3
shortjmp:
	xch	r4
	bbl	$F
	ldm	$9
	wrm
	wmp
	wrr
	wr0
	wr1
	wr2
	wr3
	sbm
	rdm
	rdr
	adm

	.org	0x200
farjmp:
	rd0
	rd1
	rd2
	rd3
	clb
	clc
	iac
	cmc
farsub:
	cma
	ral
	rar
	tcc
	dac
	tcs
	stc
	daa
	kbp
	dcl
